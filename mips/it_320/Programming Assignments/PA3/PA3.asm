# Dalton Cothron
# IT 320 Programming Assignment 3
# I certify that this program is entirely my own creation.
# I have not shared this code with any other person
	
	.data

inputPrompt: .asciiz "How many rows: \n"
secondPrompt: .asciiz "How many columns \n"
repeatPrompt: .asciiz "\nEnter an array element (in row order): "   #Prompts
failRow: .asciiz "The row input must be between 2 and 6!"
failColumn: .asciiz "The column input must be between 2 and 6!"
rowPrompt: .asciiz "The row sums are: "

fone : .float 1.0
fzero: .float 0.0

	.align 2
arr:	.space 150
arrayAnswers: .space 150
	.text
	
main:
	li $v0, 4
	la $a0, inputPrompt
	syscall
	li $v0, 5
	syscall
	move $s0, $v0 # Store rows in $s0
	li $s7, 0 # store row constant as $s6
	
	bgt $s0, 6, jRowFail
	blt $s0, 2, jRowFail # if we fail on rows
	
	li $v0, 4
	la $a0, seconPrompt
	syscall
	li $v0, 5
	syscall
	move $s1, $v0 # Store columns in $s1
	li $s6, 0 # Store column constant as $s6
	bgt $s1, 6, jColumnFail
	blt $s1, 2, jColumnFail # if we fail on columns
	la $t7, arr

repeatInput:
	li $v0, 4
	la $a0, repeatPrompt
	syscall
	j rowBegin
	
rowBegin:
	li $v0, 6
	syscall
	s.s $f0, 0($t7)
	addi $t7, $t7, 4 #increment by 4
	addi $s6, $s6, 1 #increment column
	j nextCol
	
nextCol:
	li $v0, 4
	la $a0, repeatPrompt
	syscall
	li $v0, 6
	syscall
	s.s $f0, 0($t7)
	addi $t7, $t7, 4 #increment by 4
	addi $s6, $s6, 1 # incremenet column counter
	beq $s1, $s6, nextRow
	j nextCol
nextRow:
	li $s6, 0
	addi $s7, $s7, 1 #add next row
	li $v0, 4
	la $a0, repeatPrompt
	syscall
	li $v0, 6
	syscall
	addi $s6, $s6, 1
	s.s $f0, 0($t7)
	addi $t7, $t7, 4
	beq $s0, $s7, computate
	j nextCol
	
jRowFail:
	li $v0, 4
	la $a0, failRow
	syscall
	j end # quit if rows fail
	
jColumnFail:
	li $v0, 4
	la $a0, failColumn
	syscall
	j end # quit if columns fail

computate:
	li $s6, 0
	li $s7, 0
	la $t6, arrayAnswers

addRows:
	la $t0, fzero
	li $t1, 0
	li $t2, 0($t7)
	addi $t0, $t0, $t2
	s.s $t0, 0($t6)
	addi $t6, $t6, 4
	addi $t7, $t7, 4
	mul $t7, $t7, $s0
	addi $s7, $s7, 1
	beq $s7, $s0, end
	j addRows
	
end:
	li $v0, 4
	li $t1, rowPrompt
	syscall
	li $v0, 2
	la $f12, 0($t6)
	syscall
	
	li $v0, 10
	syscall #return to system
	