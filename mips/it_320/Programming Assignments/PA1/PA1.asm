# Dalton Cothron
# IT 320 Lab 2
#I certify that this program is entiely my own creation.
# I have not shared this code with any other person

	.data
buffer: .space 80 #Create space for string input
	
s1: .asciiz "Enter a string: "

length: .asciiz "\nYour string contains "

lengthPart: .asciiz " characters.\n"

header: .asciiz "There are " #Used to print the first parts of strings

upper: .asciiz " upper case characters.\n"

lower: .asciiz " lower case characters.\n"

digits: .asciiz " digits.\n"

spaces: .asciiz " spaces.\n"

other: .asciiz " other characters.\n"

	.text
	
main:	
	li $v0, 4	#system call code for print_str
	la $a0, s1	#address of the string to print
	syscall		#print the first prompt
	
	li $v0, 8 #code for syscall read_string
	la $a0, buffer #Tell syscall where the buffer is
	li $a1, 90 #Tell syscall how big the buffer is
	syscall
	
	la $t0, buffer # place address of buffer in $t0
loop:	lb $t1, 0($t0) # read byte located in $t0
	addi $t0, $t0, 1 # Increase address in %t0

	li $t2, 0x0a
	beq $t1, $t2, end 
	li $t2, 0x00
	beq $t1, $t2, end #branch if we need to exit
	addi $s7, $s7, 1 #count is in $s7
	
	sgeu $t5, $t1, 91  
	slti $t4, $t1, 122 
	beq $t4, $t5, lowercount # If it is a lowercase
	
	sgeu $t5, $t1, 65
	slti $t4, $t1, 90
	beq, $t4, $t5, highercount #if it is uppercase
	
	sgeu $t5, $t1, 48
	slti $t4, $t1, 57
	beq $t4, $t5, digitcount #if it is a digit
	
	seq $t5, $t1, 32
	li $t4, 1
	beq $t4, $t5, spacecount
	
	addi $s6, $s6, 1 #If we hit this, it's "other"
	j loop	

lowercount:
	addi $s1, $s1, 1 # Store lowercase count in $s1
	j loop
highercount:
	addi $s2, $s2, 1 #store uppercase count in $s2
	j loop
digitcount:
	addi $s3, $s3, 1 #Store digit count in  $s3
	j loop
spacecount:
	addi $s4, $s4, 1 #Store spacecount in $s4
	j loop
end:
	li $v0, 4	
	la $a0, length
	syscall		
	li $v0, 1
	move $a0, $s7 #length is in $s7
	syscall 
	li $v0, 4
	la $a0, lengthPart
	syscall #Print remaining length text
	
	li $v0, 4
	la $a0, header
	syscall
	li $v0, 1
	move $a0, $s2 #uppcase is in $s2
	syscall
	li $v0, 4
	la $a0, upper
	syscall #print remaining section for uppercase
	
	li $v0, 4
	la $a0, header
	syscall
	li $v0, 1
	move $a0, $s1 # lowercase is in $s0
	syscall
	li $v0, 4
	la $a0, lower
	syscall #print remaining lowercase section
	
	li $v0, 4
	la $a0, header
	syscall
	li $v0, 1
	move $a0, $s3 #digits is in $s3
	syscall
	li $v0, 4
	la $a0, digits
	syscall #print remaining digits section
	
	li $v0, 4
	la $a0, header
	syscall
	li $v0, 1
	move $a0, $s4 #space is in $s4
	syscall
	li $v0, 4
	la $a0, spaces
	syscall #print remaining spaces section
	
	li $v0, 4
	la $a0, header
	syscall
	li $v0, 1
	move $a0, $s6 #other count is in $s6
	syscall
	li $v0, 4
	la $a0, other
	syscall #print other section
	
	
	li $v0, 10	#return to system
	syscall