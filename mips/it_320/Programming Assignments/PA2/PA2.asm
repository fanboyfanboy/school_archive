# Dalton Cothron
# IT 320 Programming Assignment 2
# I certify that this program is entirely my own creation.
# I have not shared this code with any other person

	.data
buffer: .space 80 # Create space for string input
storeBuffer: .space 80 #Create a space to save alphabetic characters

inputPrompt: .asciiz "Enter a string: "
processedPrompt: .asciiz "\nThe processed string is: \n"

resultNo: .asciiz "\nThis string is not a palindrone."
resultYes: .asciiz "\nThis string is a palindrome." 
	.text 
	
main:
	li $v0, 4 
	la $a0, inputPrompt
	syscall # Prompt for input
	
	li $v0, 8 
	la $a0, buffer
	li $a1, 80
	syscall # Accept input and buffer
	li $t0, 0
	la $a1, storeBuffer
	
loop:	
	lb $t1, 0($a0) # read byte located in $t0 and store in $s5 *USE THIS TO PRESERVE ACROSS FUNCTION CALLS TO STORE*
	li $t2, 0x0a
	beq $t1, $t2, end 
	li $t2, 0x00
	beq $t1, $t2, end #branch if we need to exit
	
	sgeu $t2, $t1, 65
	slti $t3, $t1, 90
	beq $t2, $t3, upper #If it's is uppercase
	
	sgeu $t2, $t1, 91
	slti $t3, $t1, 122
	beq $t2, $t3, lower #If it's NOT lowercase, $t5 = 0
	
	j loop
upper:
	addi $t1, $t1, 32 #convert to lowercase
	j store
lower:
	j store
store:
	addi $t0, $t0, 1
	
	sb   $t1, 0($a1)
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	
	j loop
palindroneTest:
	slti $t1, $a1, 2
	bne $t1, $zero, outputYes #base case
	
	lb $t1, 0($a1) #Load left inex
	addi $t2, $t0, -1
	add $t2, $t2, $a1
	lb $t2, 0($t2)
	bne $t1, $t2, outputNo
	
	addi $a1, $a1, -2
	addi $a0, $a0, 1
	j palindroneTest

end:
	li $v0, 4
	la $a0, processedPrompt
	syscall # Output first prompt
	
	li $v0, 4
	la $a0, storeBuffer
	li $a1, 80 #Tell syscall how big the buffer is
	syscall #output processed word
	
	la $a1, storeBuffer
	li $a2, 0 #For left index
	j palindroneTest
	
outputNo:
	li $v0, 4
	la $a0, resultYes
	syscall #output for no of palindrone prompt
	j finished
outputYes:
	li $v0, 4
	la $a0, resultNo
	syscall #ouput for yes
	j finished

finished:	
	li $v0, 10
	syscall #return to thte system	