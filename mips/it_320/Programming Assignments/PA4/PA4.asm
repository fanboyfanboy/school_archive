# Dalton Cothron
# IT 320 Programming Assignment 4
# I certify that this program is entirely my own creation 
# ***** NOTE TO DR. TIETZ. THIS PA4 IS AN UPDATE [VERSION O.2] OF PA2 THAT WAS SUBMITTED EARLIER. *******
# I have not shared this code with any other person

	.data
buffer: .word 80 # Create space for string input
storeBuffer: .word 80 #Create a space to save alphabetic characters

inputPrompt: .asciiz "Enter a string: "
processedPrompt: .asciiz "\nThe processed string is: \n"

resultNo: .asciiz "\nThis string is not a palindrone."
resultYes: .asciiz "\nThis string is a palindrome." 
	.text 
	
main:
	li $v0, 4 
	la $a0, inputPrompt
	syscall # Prompt for input
	
	li $v0, 8 
	la $a0, buffer
	li $a1, 80
	syscall # Accept input and buffer
	
	la $t0, buffer # Place address of buffer in $t0
	la $s4, storeBuffer #Place our 2nd buffer in $a2
	b loop
loop:	
	lb $t1, 0($t0) # read byte located in $t0 and store in $s5 *USE THIS TO PRESERVE ACROSS FUNCTION CALLS TO STORE*
	addi $t0, $t0, 1 # Increase Address in $t0
	
	li $t2, 0x0a
	beq $t1, $t2, newLine # Will just loop back with $t0 incrememnted
	li $t2, 0x00
	beq $t1, $t2, nullValue #branch if we hit the null character to exit
	
	sgeu $t2, $t1, 65
	slti $t3, $t1, 90
	beq $t2, $t3, upper #If it's is uppercase
	
	sgeu $t2, $t1, 91
	slti $t3, $t1, 122
	beq $t2, $t3, store # Store lowercase, just jump to store
	
	j loop
	
store:
	sb   $t1, 0($s4) # Store the byte in the counter $t4
	addi $s4, $s4, 1 #Increase counter at $t4
	addi $a1, $a1, 1
	j loop # Continue looping
	
newLine:
	j loop # We skipped the newline here but increased counter in the loop

upper:
	addi $t1, $t1, 32 #convert to lowercase
	j store # Now go to store
nullValue:
	sb $t1, 0($t4) #Store the null character
	addi $t4, $t4, 1 #Increment the counter
	b preparePalindroneTest


# *******PALINDRONE TEST SECTION *******
preparePalindroneTest:
	li $a0, 80 #Load our buffer w/ string
	la $a1, storeBuffer
	jal palindroneTest #Do our test
	add $t5, $v0, $zero # Store result
	jal print
	addi $v0, $zero, 10
	syscall #Exit
	
palindroneTest:
	slti $t0, $a0, 2 
	bne $t0, $zero, isPalindrone # Check base case
	
	
	lb $t0, 0($a1) #Load left inex
	addi $t1, $a0, -1
	add $t1, $t1, $a1 # right side
	lb $t1, 0($t1)
	bne $t0, $t1, isNotPalindrone
	
	addi $a0, $a0, -2
	addi $a1, $a1, 1
	j palindroneTest # adjust pointers & recurse

isNotPalindrone:
	addi $v0, $zero, 1
	jr $ra
		
isPalindrone:
	addi $v0, $zero, 0
	jr $ra

print:
	# PRINT SECTION
	li $v0, 4
	la $a0, processedPrompt
	syscall # Output first prompt
	
	li $v0, 4
	la $a0, storeBuffer
	li $a1, 80 #Tell syscall how big the buffer is
	syscall #output processed word
	
	bne $t5, $zero, testPassed #If it is a palindrone
	li $v0, 4
	la $a0, resultNo
	syscall
testPassed:
	la $a0, resultYes
	syscall #output for no of palindrone prompt
	jr $ra