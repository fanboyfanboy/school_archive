# Dalton Cothron
# Lab 03
# I certify that this program is entirely my own creation
# (except for the portions provied by the professor).
# I have not shared this code with any other person.
#
# Java code for the recursive Factorial algorithm:
# 
# public class Factorial{
#		public static void main(String args[]) throws IOException, NumberFormatExcpetion{
#		
#		int requestedFactorial;
#		int answer;
# 		try{
#			requestedFactorial = Integer.parseInt(System.scanner.in("Enter a factorial number: "));
#			answer = fact(requestedFactorial);
#		}catch(IOException io | NumberFormatException nfe){
#			System.out.println("Not an integer!");
#			return;
#		}
#		System.out.println("Factorial " + factorial + " is " + answer);
#
#		return 0;
#	}
#
#		public int fact(int n){
#			int number;
#			if(n == 0)
#				return 1;
#			
#			number = fact(n-1) * n;
# 			return number;
#	}
#}


        .data
p1:     .asciiz "Which Factorial number do you want? "
p2:     .asciiz "Factorial #"
p3:     .asciiz " is "
nl:     .asciiz "\n\n"

        .text

main:   # Build the stack frame
        addiu   $sp,$sp,-4      # main's stack frame is 4 bytes long
        sw      $ra,0($sp)      # Save return address
        
        # Perform input
        la      $a0, p1         # address of string to print
        li      $v0, 4
        syscall                 # print the first prompt

        li      $v0, 5          # system call code for read_int
        syscall                 # read the integer
        move    $s0, $v0        # and store "i" to $s0
        
        # Set up and call fib
        move    $a0, $s0
        jal fact
        move    $s1, $v0        # save result of fact calc in $s1

        # Perform output
        li      $v0, 4          # system call code for print_str
        la      $a0, p2         # address of string to print
        syscall                 # print the answer part 1

        li      $v0, 1          # system call code for print_int
        move    $a0, $s0        # integer to print (c)
        syscall                 # print it

        li      $v0, 4          # system call code for print_str
        la      $a0, p3         # address of string to print
        syscall                 # print "is"

        li      $v0, 1          # system call code for print_int
        move    $a0, $s1        # integer to print (a)
        syscall                 # print it

        li      $v0, 4          # system call code for print_str
        la      $a0, nl         # address of string to print
        syscall                 # print some newlines
        
        addiu   $sp,$sp,4       # Tear down stack frame
        lw      $ra,0($sp)      

        li      $v0, 10         # exit program
        syscall
        
fact:    subu    $sp,$sp,12      # fact's stack frame is 12 bytes long
        sw      $ra,0($sp)      # Save return address
        sw      $a0,4($sp)      # Save n
                                # Offset of 8 is for local temporary. Not initialized here.
        
        bnez     $a0,fact2      # test for and jump around base case
        li      $v0,1           # base case
        j       done
        
fact2:   addi    $a0,$a0,-1
        jal     fact
        sw      $v0,8($sp)      # save first half of calculation into temp in stack
        
        lw      $t1,8($sp)      # retrieve the value of the first half
        lw 		 $t2,4($sp)		  # retrieve n
        mul		 $v0, $t1, $t2   # multiply times n 
                
        
done:   lw      $a0,4($sp)      # tear down stack frame and return
        lw      $ra,0($sp)
        addiu   $sp,$sp,12
        jr      $ra
