# Dalton Cothron
# IT 320 Lab 2
#I certify that this program is entiely my own creation.
# I started with the code from Lab 1 and modified as needed.
# I have not shared this code with any other person

	.data
# Note space at the end of each string!  Spacing on-screen is better this way.
p1:
	.asciiz "What is integer \"a\"? "
p2:
	.asciiz "What is integer \"b\"? "
ans1:
	.asciiz "\nc is "
ans2:
	.asciiz "\nd is "
ans3:
	.asciiz "\ne is "

	.text

# The next two lines are required only if running with spim
	.globl main
main:
	li $v0, 4	#system call code for print_str
	la $a0, p1	#address of the string to print
	syscall		#print the first prompt
	
	li $v0, 5	#system call code for read_int
	syscall		#read the first integer
	move $s3, $v0	# 'a' is i in register $s3
	
	li $v0, 4	 #system call code for print_str
	la $a0, p2	#address of string to print
	syscall		#print the second prompt
	
	li $v0, 5	#system call for read_int
	syscall 	# read the second integer
	move $s4, $v0	# 'b' is in $s4
	
	# ******************** 'C' section *******************
	li $a1, 21	#load and store 21
	move $t4, $a1
	mul $t5, $t4, $s3
	sub $s0, $t5, $s4	#Do our math for 'C' and store in $t4 for later $s0 is for 'C'
	
	# ****************** 'D' section ************************************
	
	bgez $s3, Else
	li $a2, 9
	move $t7, $a2
	sub $s1, $s4, $t7  #Else subtraction statement for 'D'
	j Exit
Else:
	sub $a1, $zero, 4
	move $t6, $a1 #-4 is in $t6
	li $a1, 11
	move $t7, $a1 #11 is in $7
	
	mul $t1, $t6, $s4
	add $t2, $t1, $s3
	add $s1, $t2, $t7 #Do our math, 'D' is stored in $t5
Exit:

	#*************************** 'E' section *****************************
	beq $s3, $s4, EElse
	sub $t1, $s3, 2
	add $t2, $s4, 25
	mul $s2, $t1, $t2 #D oour math for else statement for #
	j EExit
EElse:
	move $s2, $s4 #Set our E if a == b
EExit:
	
	#******************** Log section *************************
	
	li $v0, 4	#system call for print string for C
	la $a0, ans1	# address of the string to print
	syscall		#print the answer of identifying string
	
	li $v0, 1	 #system call print C answer int
	move $a0, $s0	# 'C' is in $s0
	syscall 	#print the int
	
	li $v0, 4	#system call code for print string for D
	la $a0, ans2	#address of the string to print
	syscall	# print the answer identifying string
	
	li $v0, 1	#system call code for print D answer int
	move $a0, $s1	# 'D' i s in $s1
	syscall		#print it
	
	li $v0, 4 #System call to print E answer string
	la $a0, ans3
	syscall
	
	li $v0, 1 #System call to print E answer int
	move $a0, $s2 #E is in $s2
	syscall 
	
	li $v0, 10	#return to system
	syscall
