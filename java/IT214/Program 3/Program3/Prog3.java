//Dalton Cothron
//Program 3

import java.util.Random;
import java.lang.Math;

public class Prog3 {
    public static void main(String[] args) {

        Random randomGenerator = new Random();
        int maxLength=0;
        int maxZeroes=0;
        double average = 0;
        double zeroAverage = 0;

        for(int trials=1; trials<=50; trials++){  // repeat the simulation

            int longest=0;
            int lineZeroes=0;
            int lineOne = 0;
            int lineTwo = 0;
            int lineThree = 0;
            double shiftAverage = 0;

            for (int minutes=1; minutes<=720; minutes++) {  // each 12-hour simulation
                int addCustomer = randomGenerator.nextInt(2); //Generate a 0 or 1 to add a customer
                if(addCustomer == 1)
                {
                    //Determine which line to add a customer to.
                    if(lineOne < lineTwo){
                        if(lineOne < lineThree){
                            lineOne++; }
                        else{ lineThree++; }
                    }
                    else{
                        if(lineTwo < lineThree){
                            lineTwo++; }
                        else{ lineThree++; }
                    }
                }
                //Check to see if a customer checks out of the line.
                if(lineOne > 0){
                    int decreaseOne = randomGenerator.nextInt(5);
                    if(decreaseOne == 4){lineOne--;}
                }
                if(lineTwo > 0){
                    int decreaseTwo = randomGenerator.nextInt(5);
                    if(decreaseTwo == 4){lineTwo--;}
                }
                if(lineThree > 0){
                    int decreaseThree = randomGenerator.nextInt(5);
                    if(decreaseThree == 4){lineThree--;}
                }
            // record max length of any line
            if (Math.max(lineOne, Math.max(lineTwo,lineThree))>longest){
                longest=Math.max(lineOne, Math.max(lineTwo,lineThree));
            }
                //Add zeroes to line counter
                if(lineOne == 0){ lineZeroes++; }
                if(lineTwo == 0){ lineZeroes++; }
                if(lineThree == 0){ lineZeroes++; }

             //get the average for each minute
             //Add to 12 hour average
             double minAverage = (lineOne + lineTwo + lineThree)/3;
             shiftAverage += minAverage;

        } // end of for minutes

             if (longest>maxLength){
                  maxLength=longest; } //Set max longest line recorded
             average += shiftAverage/720; //Add that 12 hour sim average to overall total
             zeroAverage += lineZeroes/3; //Add that run zero totals to overall

             if (lineZeroes>maxZeroes){
                maxZeroes=lineZeroes;
            }

        } // end of for trials

        average = average / 50;
        zeroAverage = zeroAverage / 50;
        System.out.printf("The average line length for 50 runs is: %.2f",average); //Print the average line length
        System.out.println();

        System.out.printf("The average number of empty lines is: %.2f",zeroAverage); //Print overall empty line average
        System.out.println();

        System.out.println("The longest line overall is: " +maxLength); //Print the max length overall
        System.out.println();

    } // end of main

} // end of Prog3
