import java.util.Scanner;
import java.text.DecimalFormat;


public class MethodPractice
{
	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");

		double length;
		double side1, side2, side3;
		double side,angle;

		// Square test
		System.out.print("What is the side length of a square? ");
		length = keyboard.nextDouble();

		System.out.println("The area of the square is "+df.format(area(length,'S')));

		// Circle test
		System.out.print("What is the radius of a circle? ");
		length = keyboard.nextDouble();

		System.out.println("The area of the circle is "+df.format(area(length,'C')));

		// Rectangle test
		System.out.print("What is the length of one side of a rectangle? ");
		side1 = keyboard.nextDouble();
		System.out.print("What is the length of the adjacent side of the rectangle? ");
		side2 = keyboard.nextDouble();

		System.out.println("The area of the rectangle is "+df.format(area(side1, side2, 'R')));

		// Rhombus test
		System.out.print("What is the length of a side of the rhombus? ");
		side = keyboard.nextDouble();
		System.out.print("What is the angle (in degrees) of any interior angle of the rhombus? ");
		angle = keyboard.nextDouble();

		System.out.println("The area of the rhombus is "+df.format(area(side, angle, 'H')));

		// Triangle test
		System.out.print("Enter the sides lengths of the triangle, with a space between each: ");
		side1 = keyboard.nextDouble();
		side2 = keyboard.nextDouble();
		side3 = keyboard.nextDouble();

		System.out.println("The area of the triangle is "+df.format(area(side1,side2,side3)));

	} // end main

	     // two doubles and a character
    public static double area(double one, double two,char shape) {

        if (shape=='R')  // Rectangle
            return one*two;

        else if (shape=='H')  {  // Rhombus
            double radians=Math.toRadians(two);

            return one*one*Math.sin(radians);
            } // end Rhombus

            else
                return -1;
    }

    // one double and a character
    public static double area(double data, char shape) {

        if (shape=='S')  // Square
            return data*data;

        else if (shape=='C')  // Circle
                return Math.PI*data*data;

            else
                return -1;
    }

    // three doubles
    public static double area(double side1, double side2, double side3) {

        // use Heron's Law to calculate the area of a triangle if given the lengths of the three sides

        double semiP = (side1 + side2 + side3) / 2;

        return Math.sqrt(semiP * ((semiP - side1)*(semiP - side2)*(semiP - side3)));

    }



}  //end MethodPractice
