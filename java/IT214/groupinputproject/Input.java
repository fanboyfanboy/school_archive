

import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Travis, Dalton, Supreme, Ranendra, Tyler (Group 1)
 */
public class Input {

    /**
     * Displays a dialog with listbox for month
     *
     * @return 3-character month code
     * @throws InputCancelException
     */
    public static String month() throws InputCancelException {
        final String[] MONTH_NAME = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        //Frame for month
        JFrame month = new JFrame("Input Dialog with month");

        String m = (String) JOptionPane.showInputDialog(month,
                "What month were you born?",
                "Month",
                JOptionPane.QUESTION_MESSAGE,
                null,
                MONTH_NAME,
                MONTH_NAME[0]);

        // month will be null if the user clicks Cancel
        if (m == null) {
            throw new InputCancelException("Cancelled.");
        } else {
            return m;
        }
    }

    /**
     * Displays a dialog with listbox for day of month
     *
     * @return Day of month
     * @throws InputCancelException
     */
    public static String dayOfMonth() throws InputCancelException {
        //Frame for day
        final int DAY_LENGTH = 31;
        String[] daylist = new String[DAY_LENGTH];

        for (int i = 0; i < DAY_LENGTH; i++) {
            daylist[i] = Integer.toString(i + 1); //convert integer to string
        }

        JFrame day = new JFrame("Input Dialog with day");
        String d = (String) JOptionPane.showInputDialog(day,
                "list of day scroll down for more",
                "Which day you were born",
                JOptionPane.QUESTION_MESSAGE,
                null,
                daylist,
                daylist[0]);
        // day will be null if the user clicks Cancel

        if (d == null) {
            throw new InputCancelException("Cancelled.");
        } else {
            return d;
        }
    }

    /**
     * Displays a dialog with listbox for year
     *
     * @return Birth year
     * @throws InputCancelException
     */
    public static String birthYear() throws InputCancelException {
        //Frame for year
        final int YEAR_LENGTH = 115; //assume life of human

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        String[] yearlist = new String[YEAR_LENGTH];

        for (int i = 0; i < YEAR_LENGTH; i++) {
            yearlist[i] = Integer.toString(currentYear - i); ////convert integer to string
        }

        JFrame year = new JFrame("Input Dialog with year");
        String y = (String) JOptionPane.showInputDialog(year,
                "list of year scroll down",
                "Which year you were born",
                JOptionPane.QUESTION_MESSAGE,
                null,
                yearlist,
                yearlist[0]);

        // year will be null if the user clicks Cancel
        if (y == null) {
            throw new InputCancelException("Cancelled.");
        } else {
            return y;
        }

    }

    /**
     * Displays text input box
     *
     * @return name
     * @throws InputCancelException
     */
    public static String name() throws InputCancelException {

        boolean valid = false;
        String name = "";

        String question = "Please enter your name";
        while (!valid) {
            JFrame frame = new JFrame("InputDialog Example #1");

            // prompt the user to enter their name
            name = JOptionPane.showInputDialog(frame, question);


            if (name == null) {
                throw new InputCancelException("Cancelled.");
            }
            name = stripWhiteSpace(name);

            int validState = Valid.name(name);
            if (validState == Valid.NAME_VALID) {
                valid = true;
            } else if ((validState == Valid.NAME_CONFIRMCORRECT) && (getYesNo("Is '" + name + "' correct?"))) {
                valid = true;
            } else {
                //validState of 2 is invalid -> valid stays false
                question = "Please re-input your name";
            }
        }
        return name;
    }

    public static String streetAddress() throws InputCancelException {

        String streetaddress = "";

        boolean continuelooping = true;

        String prompt = "What is your street address?\nAddress must contain space.";
        while (continuelooping) {
            JFrame frame = new JFrame("Address");
            streetaddress = JOptionPane.showInputDialog(prompt);
            if (streetaddress == null) {
                throw new InputCancelException("Cancelled.");
            }
            int space = streetaddress.indexOf(" ");

            if (space == -1) {

                prompt = "Address must contain space";


            }

            try {

                String number = streetaddress.substring(0, space);

                int num = Integer.parseInt(number);

            } catch (NumberFormatException|StringIndexOutOfBoundsException e) {

                prompt = "First part of the address must be numbers";

            }

            String address = streetaddress.substring(space + 1);

            if (address.trim().equals(""))// trim removes spaces
            {

                prompt = "There sould be second part of the address after the numbers";
            }

            //if valid, coninuelooping = negated (false)
            continuelooping = !Valid.streetAddress(streetaddress);

        }

        return streetaddress;
    }

    public static String state() throws InputCancelException {
        String abc = "";
        final String[] state = {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};

        JFrame frame1 = new JFrame("States");
        abc = (String) JOptionPane.showInputDialog(frame1,
                "What is your State?",
                "State",
                JOptionPane.QUESTION_MESSAGE,
                null,
                state,
                state[0]);
        if (abc == null) {
            throw new InputCancelException("Cancelled.");
        } else {
            return abc;
        }
    }

    public static String zipcode() throws InputCancelException {
        String zip = "";
        boolean valid = false;

        String question = "What is your zipcode?";

        while (!valid) {

            JFrame frame = new JFrame("Phone");

            // prompt the user to enter their name
            zip = JOptionPane.showInputDialog(frame, question);

            //change the question for next time
            question = "Your zipcode was invalid. Please try again:";

            //check if they clicked cancel
            if (zip == null) {
                throw new InputCancelException("Cancelled.");
            }

            if (Valid.zipcode(zip)) {
                valid = true;
            }

        }
        return zip;

    }

    /**
     * Asks a yes/no question
     *
     * @param inputQuestion Question to ask (empty string for default)
     * @return true/false for yes/no
     * @throws InputCancelException
     */
    public static boolean getYesNo(String inputQuestion) throws InputCancelException {
        if (inputQuestion.equals("")) {
            inputQuestion = "Continue?";
        }
        int response = JOptionPane.showConfirmDialog(null, inputQuestion, "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.NO_OPTION) {
            return false;
        } else if (response == JOptionPane.YES_OPTION) {
            return true;
        } else if (response == JOptionPane.CLOSED_OPTION) {
            throw new InputCancelException("Cancelled.");
        } //should never reach here?
        else {
            return false;
        }
    }

    public static String phone() throws InputCancelException {

        String number = "";
        boolean valid = false;

        String question = "What's your phone number?";

        while (!valid) {

            JFrame frame = new JFrame("Phone");

            // prompt the user to enter their name
            number = JOptionPane.showInputDialog(frame, question);

            //change the question for next time
            question = "Your phone number was invalid. Please try again:";

            //check if they clicked cancel
            if (number == null) {
                throw new InputCancelException("Cancelled.");
            }

            //remove anything not a digit from the input
            number = number.replaceAll("\\D", "");

            if (Valid.phoneNumber(number)) {
                valid = true;
            }

        }
        return number;
    }

    /**
     * Strips leading and trailing whitespace, and turns consecutive whitespace
     * into a single space
     *
     * @param string The string to strip
     * @return String with extra whitespace removed
     */
    public static String stripWhiteSpace(String string) {
        //removes leading and trailing whitespaces
        string = string.replaceAll("(?:^\\s+|\\s+$)", "");

        //consecutive whitespaces are turned into a single space
        return string.replaceAll("\\s+", " ");
    }

}
