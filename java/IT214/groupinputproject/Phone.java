/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class Phone {

    public static final int HOME = 1;
    public static final int WORK = 2;
    public static final int FAX = 3;
    public static final int MOBILE = 4;
    public static final int OTHER = 0;

    private String number = "";

    private int type = HOME;

    /**
     *
     * @param type HOME, WORK, FAX, MOBILE, OTHER
     * @param number phone number
     * @throws InvalidNumberException phone number was bad
     * @throws InvalidNumberTypeException type was wrong
     */
    public Phone(int type, String number) throws InvalidNumberException, InvalidNumberTypeException {
        this.number = number;
        this.type = type;
    }

    public Phone() {
        this.number = "";
        this.type = OTHER;
    }

    public void setType(int type) throws InvalidNumberTypeException {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }

    public void setNumber(String number) throws InvalidNumberException {
        if (!isValidNumber(number)) {
            throw new InvalidNumberException("The number given was not valid.");
        } else {
            this.number = number;
        }
    }

    public String getNumber() {
        return this.number;
    }

    public String getNumberFormatted() {
        //dd-ddd
        if (this.number.matches("^\\d{5}$")) {
            return this.number.replaceAll("^(\\d{2})(\\d{3})$", "$1-$2");
        } //(ddd) ddd-dddd
        else if (this.number.matches("\\d{10}$")) {
            return this.number.replaceAll("(\\d{3})(\\d{3})+(\\d{4})$", "($1) $2-$3");
        } else {
            //return this.number.replaceAll("(\\d)", "$1-");
            String suffix = "-" + this.number.substring(this.number.length() - 5);
            String num = number.substring(0, this.number.length() - 6);
            return num.replaceAll("(\\d{3})(?=(?:\\d{3})*$)", "-$1") + suffix;
            //num = num.replaceFirst("\\d{4}$", "-$1");
        }

    }

    private String test(String foo) {
        return "+1 " + foo;
    }

    public static String extractNumber() throws NumberNotFoundException {
        return "";
    }

    public static boolean isValidNumber(String number) {
        return number.matches("^(?:\\d{10,11})$");

    }

}
