/*
 Group 1 Input project

 Travis Britz
 Dalton Cothron
 Tyler Phillips
 Supreme Shrestha
 Ranendra Lakha
 */

/**
 *
 * @author Travis, Dalton, Supreme, Ranendra, Tyler (Group 1)
 */
public class GroupInputProjectMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //initialize variables
        String birthmonth = "";
        String birthday = "";
        String birthyear = "";

        String name = "";

        String phone = "";
        try {
            Phone p = new Phone(Phone.FAX, "5077663858");
            Phone q = new Phone(Phone.FAX, "15077663858");
            System.out.println(q.getNumberFormatted());
            System.out.println(p.getNumberFormatted());
        } catch (InvalidNumberException | InvalidNumberTypeException e) {
            System.out.println(e.getMessage());
        }

        String zip = "";
        String state = "";
        String address = "";

        try { //attempt to get input values
            birthmonth = Input.month();
            birthday = Input.dayOfMonth();
            birthyear = Input.birthYear();

            name = Input.name();

            phone = Input.phone();

            zip = Input.zipcode();
            state = Input.state();
            address = Input.streetAddress();

        } catch (InputCancelException e) { //exit gracefully on cancel
            System.out.println(e.getMessage());
            System.exit(0);

        }

        System.out.printf("Date of birth is  %s / %s / %s\r\n", birthmonth, birthday, birthyear);
        System.out.println("Name: " + name);
        System.out.println("Phone: " + phone);
        System.out.println("Address: " + address + ", " + state + ", " + zip);
        System.exit(0);

    }

}
