/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.regex.*;

/**
 *
 * @author Travis
 */
public class Valid {

    public static final int NAME_INVALID = 2;
    public static final int NAME_VALID = 0;
    public static final int NAME_CONFIRMCORRECT = 1;

    /**
     * Checks if a string is likely a name
     *
     * @param name String to check
     * @return NAME_INVALID, NAME_VALID, NAME_CONFIRMCORRECT
     */
    public static int name(String name) {

        if (name.length() < 2) {
            return NAME_INVALID;
        }

        double namePercentage = percentAlpha(name);

        if (namePercentage == 1.0) {
            return NAME_VALID;
        } else if (namePercentage > 0.8) {
            return NAME_CONFIRMCORRECT;
        } else {
            return NAME_INVALID;
        }
    }

    public static boolean phoneNumber(String number) {
        //5 digits: shortcode dd-ddd
        //7 digits: ddd-dddd
        //10 digits: (ddd)-ddd-dddd
        //11 digits: 1-(ddd)-ddd-dddd
        //others?
        return number.matches("^(?:\\d{5}|\\d{7}|\\d{10,11})$");

    }

    public static boolean streetAddress(String streetaddress) {
        int space = streetaddress.indexOf(" ");

        if (space == -1) {
            return false;

        }

        try {
            String number = streetaddress.substring(0, space);
            int num = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        String address = streetaddress.substring(space + 1);

        if (address.trim().equals(""))// trim removes spaces
        {
            return false;
        }
        return true;
    }

    public static boolean zipcode(String zipcode) {

        return zipcode.matches("^\\d{5}$");

    }

    /**
     * Checks the percent of letters in a string Letters are anything in the
     * "letter" category of Unicode
     *
     * @param s String to check
     * @return double between 0 and 1 (percent of characters that are letters)
     */
    public static double percentAlpha(String s) {
        //number of letters divided by number of non-whitespace characters
        return (double) countMatches(s, "\\p{L}") / countMatches(s, "\\S");

    }

    /**
     * Counts the number of occurrences of an expression in a string
     *
     * @param s The string to check
     * @param regex The expression to look for
     * @return The number of matches found
     */
    public static int countMatches(String s, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        int count = 0;
        while (matcher.find()) { //loop until can't find more matches
            count++;
        }

        return count;
    }

}
