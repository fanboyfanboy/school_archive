/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-o2-18
*/

public class Address {

//=============================================================================
//Properties
//=============================================================================

    private String street;
    private String streetTwo;
    private String city;
    private String postalCode;
    private String state;

//=============================================================================
//Constructors
//=============================================================================

    //Constructor with private data
    public Address(String street, String streetTwo, String city, String state, String postalCode)
    {
        this.street = street;
        this.streetTwo = streetTwo;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;

    }

    //Empty constructor
    public Address()
    {
        street = "";
        streetTwo = "";
        city = "";
        state = "";
        postalCode = "";
    }

//=============================================================================
//Setters
//=============================================================================

    /**
    *Sets street.
    *
    *@param street String to set street.
    */
    public void setStreet(String street)
    {
        this.street = street;
    }

    /**
    *Sets second street/apt number/etc
    *
    *@param streetTwo String of second address.
    */
    public void setStreetTwo(String streetTwo)
    {
        this.streetTwo = streetTwo;
    }

    /**
    *Sets city.
    *
    *@param city String of city to be entered.
    */
    public void setCity(String city)
    {
        this.city = city;
    }

    /**
    *Sets state.
    *
    *@param state String of the state to be entered.
    */
    public void setState(String state)
    {
        this.state = state;
    }

    /**
    *Sets zip code.
    *
    *@param postalCode String of zip code.
    */
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

//=============================================================================
//Getters
//=============================================================================

    /**
    *Gets steet.
    *
    *@return String of the street address.
    */
    public String getStreet()
    {
        return this.street;
    }

    /**
    *Gets second street.
    *
    *@return String of the second streeet addres.
    */
    public String getStreetTwo()
    {
        return this.streetTwo;
    }

    /**
    *Gets city
    *
    *@return String of the city.
    */
    public String getCity()
    {
        return this.city;
    }

    /**
    * Gets state.
    *
    *@return String of the state.
    */
    public String getState()
    {
        return this.state;
    }

    /**
    * Gets zip code.
    *
    *@return String of the zip code.
    */
    public String getPostalCode()
    {
        return this.postalCode;
    }

//=============================================================================
//Public Methods
//=============================================================================

    /**
    * Check which zip code is smaller.
    *
    *@param first The first object.
    *@param second The second object.
    *@return    True if first object is before second object.
    */
    public static boolean comesBefore(Address first, Address second)
    {
        int zipOne = Integer.parseInt(first.getPostalCode());
        int zipTwo = Integer.parseInt(second.getPostalCode());
        if (zipOne < zipTwo)
        {
            return true;
        }
        else{ return false; }
    }

} //end class
