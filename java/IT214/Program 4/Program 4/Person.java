/**
 *@author Dalton Cothron
 *@version 1.0
 *@since 2015-02-18
 */

public class Person {

//=============================================================================
//Properties
//=============================================================================

    private String firstName;
    private String lastName;

//=============================================================================
//Constructors
//=============================================================================

    public Person(String first, String last)
    {
        firstName = first;
        lastName = last;
    }

    public Person()
    {
        firstName = "";
        lastName = "";

    }

//=============================================================================
//Setters
//=============================================================================

    /**
    *Sets first name.
    *
    *@param first First name.
    */
    public void setFirst(String first)
    {
        this.firstName = first;
    }

    /**
    * Sets last name.
    *
    *@param last Last name.
    */
    public void setLast(String last)
    {
        this.lastName = last;
    }

//=============================================================================
//Getters
//=============================================================================

    /**
    *Gets first name.
    *
    *@return First name as a string.
    */
    //get first name
    public String getFirst()
    {
        return this.firstName;
    }

    /**
    *Gets last name.
    *
    *@return Last name as a string.
    */
    public String getLast()
    {
        return this.lastName;
    }

//=============================================================================
//Public Methods
//=============================================================================

    /**
    *Checks which object is first.
    *Sorts alphabetically by last name.
    *
    *@param first The first object.
    *@param second The second object.
    *@return  True if first object is before second.
    */
    public static boolean comesBefore(Person first, Person second)
    {
        if(first.getLast().compareTo(second.getLast()) < 0)
        {
            return true;
        }
        else{return false;}

    }

}//end class
