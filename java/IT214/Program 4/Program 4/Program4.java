/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-02-18
*/

//=========================================================================
//Import Statements
//=========================================================================

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;


public class Program4 {

    public static void main(String[] args) throws FileNotFoundException{

//Setup
//=========================================================================

        boolean notEmptySecond = true; //true if second address label is not empty

        File inputFile = new File ("p4input.csv");  //csv input file

        Scanner inFile = new Scanner(inputFile); //Prepare to read contents from file.

        PrintWriter mailingOutput = new PrintWriter("1vf4049nv.txt"); //mailing label output file.
        PrintWriter directoryOutput = new PrintWriter("2vf4049nv.txt"); //directory output file.

        String COMMA = ",";  //Set deliminator

        ArrayList<Address> address = new ArrayList<Address>(); //create array for address object
        ArrayList<Person> person = new ArrayList<Person>(); //create array for person objects

//=========================================================================
//Processing
//=========================================================================

        //Read contents of file, create new objects, add to array.
        while(inFile.hasNextLine())
        {
            String fileLine = inFile.nextLine();

            String[] line = fileLine.split(COMMA);

            person.add(new Person(line[0],line[1]));
            address.add(new Address(line[2], line[3],line[4], line[5], line[6]));
        }

//=========================================================================
//Mailing Label
//=========================================================================

        //Sort mailing labels by zipcode
        for(int i = 1; i < address.size(); i++)
        {
            if(Address.comesBefore(address.get(i), address.get(i-1)))
            {
                Collections.swap(address, address.indexOf(address.get(i)), address.indexOf(address.get(i-1)));
                Collections.swap(person, person.indexOf(person.get(i)), person.indexOf(person.get(i-1)));
            }
        }

        //For loop to print out mailing list
        for(int i = 0; i < address.size(); i++)
        {

            notEmptySecond = true; //reset to true before checking again

            //get all the info in temporary strings
            String tempFirst = person.get(i).getFirst();
            String tempLast = person.get(i).getLast();
            String tempStreetOne = address.get(i).getStreet();
            String tempStreetTwo = address.get(i).getStreetTwo();
            String tempZip = address.get(i).getPostalCode();
            String tempCity = address.get(i).getCity();
            String tempState = address.get(i).getState();

            //Check for 2nd street, apartment number, etc.

            if(tempStreetTwo.equals(""))
            {
                notEmptySecond = false;
            }

            //Print out information
            mailingOutput.println(tempFirst+" "+tempLast);
            mailingOutput.println(tempStreetOne);

            //Check for further address info.
            if(notEmptySecond)
            {
                mailingOutput.println(tempStreetTwo);
            }

            mailingOutput.println(tempCity+", "+tempState+" "+tempZip);
            mailingOutput.println();

            //Print extra line if no 2nd steet/apartment number etc
            if(!notEmptySecond)
            {
                mailingOutput.println();
            }

        } //End mailing output.

        //Sort directory by Name
        for(int i = 1; i < person.size(); i++)
        {
            if(Person.comesBefore(person.get(i), person.get(i-1)))
            {
                Collections.swap(person, person.indexOf(person.get(i)), person.indexOf(person.get(i-1)));
                Collections.swap(address, address.indexOf(address.get(i)), address.indexOf(address.get(i-1)));
            }
        }

//=========================================================================
//Directory
//=========================================================================

        //Sort directory by Name
        for(int i = 1; i < person.size(); i++)
        {
            if(Person.comesBefore(person.get(i), person.get(i-1)))
            {
                Collections.swap(person, person.indexOf(person.get(i)), person.indexOf(person.get(i-1)));
                Collections.swap(address, address.indexOf(address.get(i)), address.indexOf(address.get(i-1)));
            }
        }

        //For loop to print out directory label
        for(int i = 0; i < person.size(); i++)
        {
            notEmptySecond = true; //reset to true before checking again

            //Temporary strings for printing.
            String tempFirst = person.get(i).getFirst();
            String tempLast = person.get(i).getLast();
            String tempStreetOne = address.get(i).getStreet();
            String tempStreetTwo = address.get(i).getStreetTwo();
            String tempZip = address.get(i).getPostalCode();
            String tempCity = address.get(i).getCity();
            String tempState = address.get(i).getState();

            //Check for 2nd street, apartmentnumber, etc.
            if(tempStreetTwo.equals(""))
            {
                notEmptySecond = false;
            }

            //Print out directory label
            directoryOutput.print(tempFirst + " " + tempLast+", ");
            directoryOutput.print(tempStreetOne+", ");

            if(notEmptySecond)
            {
                directoryOutput.print(tempStreetTwo+", ");
            }
            directoryOutput.print(tempCity+", "+tempState+" ");
            directoryOutput.println(tempZip);
        }

//=========================================================================
//Cleanup
//=========================================================================

        //Always remember to close files
        inFile.close();
        mailingOutput.close();
        directoryOutput.close();

    } //End main

} //End class
