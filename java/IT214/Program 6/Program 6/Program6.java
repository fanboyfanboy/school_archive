/**
*Program 6
*Dalton Cothron
*/

//
//Import statements
//

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;


public class Program6 {

    public static void main(String[] args) throws FileNotFoundException{

//
//Setup
//

        File inputFile = new File("p6input.csv"); //Input file

        Scanner inFile = new Scanner(inputFile);  //Scan input file

        PrintWriter outFile = new PrintWriter("vf4049nv.txt"); //Output file

        String COMMA = ","; //deliminator

        //Innitialize master array list of 1000 spots
        ArrayList<ArrayList<Students>> master = new ArrayList<>();
        for(int i = 0; i < 1000; i++){
            master.add(new ArrayList<Students>());
        }

        //Innitialize array list for longest list
        ArrayList<Students> longestList = new ArrayList<Students>();

        //Innitialize array list for empty spots
        ArrayList<Integer> empty = new ArrayList<Integer>();
        int totalEmpty = 0;

        //Innitialize top student array list
        ArrayList<TopStudent> topStudent = new ArrayList<TopStudent>();
//
//Processing
//

        while(inFile.hasNextLine()){ //scan file and add objects

            String fileLine = inFile.nextLine();

            String[] line = fileLine.split(COMMA);

            //Get temp gpa and credits for topstudent
            Double tempGPA = Double.parseDouble(line[3]);
            int tempCredits = Integer.parseInt(line[4]);

            //Create new student
            Students student = new Students(line[0],line[1],line[2],line[3],line[4]);

            //Add people to top student list
            if(tempGPA >= 3.95 && tempCredits >= 100){
            topStudent.add(new TopStudent(line[1],line[2],line[3],line[4]));
        }

            //ending tech ID temp string
            String tempTech = line[0].substring(line[0].length() - 3);

            int tech = Integer.parseInt(tempTech);

            //Add student
            master.get(tech).add(student);
        }

        //find longest list of people
        int longList = 0;
        int largestNumber = 0;
        for(int i = 0; i < master.size(); i++){
            if(master.get(i).size() > largestNumber){
                largestNumber = master.get(i).size();
                longList = i;
            }
        }

        //Print longest list out of master
        longestList = master.get(longList);
        outFile.println("First Longest = "+longList);
        for(Students person : longestList){
            outFile.println(person);
        }

        //Find empty lists, and add them.
        for(int i = 0; i < master.size(); i++){
            if(master.get(i).size() == 0){
                empty.add(i);
                totalEmpty++;
            }
        }

        //Print out the first 10 empty lists
        outFile.println();
        outFile.print("There are "+totalEmpty+" empty lists. ");
        outFile.print("The first ten empty lists are: ");
        int count = 0;
        for(Integer each : empty){
            if(count == 10){
                outFile.print(".");
                break;
            }

        //Spacing for print file and print out empty list numbers
        if(count == 0){} else{outFile.print(", ");}
        outFile.print(empty.get(count));
        count++;
        }
        outFile.println();
        outFile.println();

        //Sort top student array list
        Collections.sort(topStudent, new Comparator<TopStudent>(){
            public int compare(TopStudent s1, TopStudent s2){
                return s1.getLast().compareTo(s2.getLast());
            }
        });

        //Print out top student list
        for(TopStudent student : topStudent){
            outFile.printf(student.getLast().concat(", "+student.getFirst()));
            outFile.printf("%5s", student.getGPA());
            outFile.println();
        }

        //Close files
        inFile.close();
        outFile.close();
    } //End main method
}//End class
