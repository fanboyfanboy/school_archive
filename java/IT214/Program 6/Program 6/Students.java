

public class Students {

//
//Properties
//
    private String techID;
    private String firstName;
    private String lastName;
    private String gpa;
    private String credits;

//
//Constructors
//

    //Empty constructor
    public Students(){}

    //Constructor with input data
    public Students(String techID, String first, String last, String gpa, String credits){
        this.techID = techID;
        this.firstName = first;
        this.lastName = last;
        this.gpa = gpa;
        this.credits = credits;
    }

//
//Setters
//
    //Set tech id
    //@param tech id
    public void setTechID(String tech){
        this.techID = tech;
    }

    //Set first name
    //@param the first name
    public void setFirst(String first){
        this.firstName = first;
    }

    //Set last name
    //@param last name
    public void setLast(String last){
        this.lastName = last;
    }

    //Set gpa
    //@param the gpa to be set
    public void setGPA(String gpa){
        this.gpa = gpa;
    }

    //Set credits
    //@param the credit to be set
    public void setCredits(String credits){
        this.credits = credits;
    }

//
//Getters
//

    //Get tech ID
    //return string of the tech ID
    public String getTechID(){
        return this.techID;
    }

    //Get first name
    //return first name as a string
    public String getFirst(){
        return this.firstName;
    }

    //Get last name
    //return string of the last name
    public String getLast(){
        return this.lastName;
    }

    //Get gpa
    //return the String of the GPA
    public String getGPA(){
        return this.gpa;
    }

    //Get credits
    //return string of the credits
    public String getCredits(){
        return this.credits;
    }

    //Fixes the output string
    //return the regular string
    public String toString(){
        String output = techID + "\t" + firstName + " " + lastName;
        return output;
    }
} //End class
