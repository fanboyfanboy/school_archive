//Top Student Class
//Program 6
//By Dalton Cothron

public class TopStudent {

//
//Private variables
//
    private String firstName;
    private String lastName;
    private String gpa;
    private String credits;


    public TopStudent(){} //Empty constructor

    //Constructor with private data
    public TopStudent(String first, String last, String gpa, String credits){
        this.firstName = first;
        this.lastName = last;
        this.gpa = gpa;
        this.credits = credits;
    }
//
//Setters
//
    //Set first name
    //@param String of the first name
    public void setFirst(String first){
        this.firstName = first;
    }

    //Set last name
    //@param last name as a string
    public void setLast(String last){
        this.lastName = last;
    }

    //Set gpa
    //@param GPA as a string
    public void setGPA(String gpa){
        this.gpa = gpa;
    }

    //Set credits
    //@param String as credits
    public void setCredits(String credits){
        this.credits = credits;
    }

//
//Getters
//

    //Get first
    //return string of the first name
    public String getFirst(){
        return this.firstName;
    }

    //Get last
    //return string of the last name
    public String getLast(){
        return this.lastName;
    }

    //Get gpa
    //return String of the gpa
    public String getGPA(){
        return this.gpa;
    }

    //Get credits
    //return string of the gpa
    public String getCredits(){
        return this.credits;
    }

}//End class
