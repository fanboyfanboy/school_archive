// Put your name here, as the first line in the file
// GO TO LINE 22 AND CHANGE THE FILE NAME
// TO **YOUR** STARID

import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class sample
{
	public static void main (String[] args) throws FileNotFoundException
	{

		File file1 = new File("TextOne.txt");  // text input file
		File file2 = new File("CSVTwo.csv"); // CSV input file

		Scanner inFile1 = new Scanner(file1);
		Scanner inFile2 = new Scanner(file2);

		PrintWriter outFile = new PrintWriter("tr3342dh.txt"); // output file with StarID

		String SPACE = " ";    //set the deliminator used to split the lines of the text input file
		String COMMA = ",";  //set the deliminator used to split the lines of the CSV input file


		while(inFile1.hasNextLine()) // go until there are no more lines in the file
		{
			String fileLine = inFile1.nextLine();              // get the next line of the input file
			String [] line = fileLine.split(SPACE);           // split it where there is a single space
			outFile.println("First item: "+line[0]+"\t");  // the split items are in the string array line
			outFile.println("Second item: "+line[1]+"\t");
			outFile.println("Third item: "+line[2]);

			double value1 = Double.parseDouble(line[1]);  // must convert the string to a double
			double value2 = Double.parseDouble(line[2]);
			double sum = value1 + value2;


			// format to two decimal places
			outFile.print(String.format( "%.2f", value1)+" + "+String.format( "%.2f", value2));
			outFile.println(" = "+String.format( "%.2f", sum));

			outFile.println();  // blank line between items
		}

		inFile1.close();  // always remember to close files

		outFile.println("==================================");
		outFile.println();

		while(inFile2.hasNextLine()) // go until there are no more lines in the file
		{
			String fileLine = inFile2.nextLine();
			String [] line = fileLine.split(COMMA);
			outFile.println("First item: "+line[0]+"\t");
			outFile.println("Second item: "+line[1]+"\t");
			outFile.println("Third item: "+line[2]);

			double value1 = Double.parseDouble(line[1]);
			double value2 = Double.parseDouble(line[2]);
			double sum = value1 + value2;

			outFile.print(String.format( "%.2f", value1)+" + "+String.format( "%.2f", value2));
			outFile.println(" = "+String.format( "%.2f", sum)+"\n"+"\n");

			outFile.println();
		}

		inFile2.close();  // always remember to close files
		outFile.close();  // always remember to close files
	}
}
