/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-03-15
*/

public class StudentEmployee extends Employee{

//=============================================================================
//Properties
//=============================================================================
    private double payRate;
    private int weeklyHours;

//=============================================================================
//Constructors
//=============================================================================

    //Empty constructor
    public StudentEmployee(){}

    //Constructor with private data
    public StudentEmployee(String first, String last, String street, String city, String state, String zipCode, String social, String department, Double pay, Integer hours){
        super(first, last, street, city, state, zipCode, social, department);
        this.payRate = pay;
        this.weeklyHours = hours;
    }


//=============================================================================
//Setters
//=============================================================================

    //Set pay
    public void setPay(Double pay){
        this.payRate = pay;
    }

    //Set hours
    public void setHours(Integer hours){
        this.weeklyHours = hours;
    }

//=============================================================================
//Getters
//=============================================================================

    //Get pay
    public Double getPay(){
        return this.payRate;
    }

    //Get hours
    public Integer getHours(){
        return this.weeklyHours;
    }

}//End class
