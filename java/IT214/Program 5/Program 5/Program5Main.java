/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-03-15
*/

//=========================================================================
//Import Statements
//=========================================================================

import java.util.ArrayList;
import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;


public class Program5Main {


    public static void main(String[] args) throws FileNotFoundException{

//=========================================================================
//Setup
//=========================================================================

        File inputFile = new File("p5input.csv"); //cvs input file

        Scanner inFile = new Scanner(inputFile); //open input file

        PrintWriter outputFile = new PrintWriter("vf4049nv.csv"); //StarID output file

        String COMMA = ","; //set deliminator

        ArrayList<Faculty> faculty = new ArrayList<Faculty>();  //Create array for faculty

        ArrayList<StudentEmployee> studentEmployee = new ArrayList<StudentEmployee>();
//=========================================================================
//Processing
//=========================================================================

        //read contents of file, create objects, add to specific array
        while(inFile.hasNextLine()){

            String fileLine = inFile.nextLine();

            String[] line = fileLine.split(COMMA);

            String firstName = line[0].substring(0,line[0].indexOf(" ")+1);
            String lastName = line[0].substring(line[0].lastIndexOf(" ")+1);

            double tempDouble = Double.parseDouble(line[7]);

            if(line.length == 8){ //test for faculty

                faculty.add(new Faculty(firstName,lastName,line[1],line[2],line[3],line[4],line[5],line[6],tempDouble));
            }

            else if(line.length == 9){ //test for student employee

                int tempInt = Integer.parseInt(line[8]);

                studentEmployee.add(new StudentEmployee(firstName,lastName,line[1],line[2],line[3],line[4],line[5],line[6],tempDouble,tempInt));
            }
        } //end read contents

//=========================================================================
//Sorting
//=========================================================================



        Collections.sort(faculty, new Comparator<Faculty>(){
            public int compare(Faculty f1, Faculty f2){
                return f1.getLast().compareTo(f2.getLast());
            }
        });

        Collections.sort(studentEmployee, new Comparator<StudentEmployee>(){
            public int compare(StudentEmployee s1, StudentEmployee s2){
                return s1.getLast().compareTo(s2.getLast());
            }
        });

//=========================================================================
//Write to File
//=========================================================================

        //Setup to print faculty
        outputFile.println(" FACULTY                   ");
        outputFile.println("------------------         ");

        //enhanced for loop to go over every faculty
        //Prints information to output csv file
        for(Faculty worker : faculty){

            double tempDouble = worker.getSalary() / 26;
            outputFile.printf("%.15s %.15s",worker.getLast(),worker.getFirst()+COMMA);
            outputFile.print(worker.getStreet()+COMMA+worker.getCity()+", "+worker.getState()+" "+worker.getZip()+COMMA);
            outputFile.println(worker.getSocial());

            outputFile.printf(COMMA+COMMA+worker.getDepartment()+COMMA+"$%.2f",tempDouble);

        }

        //Setup to print student employees
        outputFile.println("                          ");
        outputFile.println(" STUDENT EMPLOYEES        ");
        outputFile.println("--------------------------");

        //enhanced for loop to go over each student employee
        //Prints information to output csv file
        for (StudentEmployee student : studentEmployee){

            double owedPay = (student.getPay() * student.getHours()) * 2;

            outputFile.printf("%.15s %.15s            ",student.getLast(),student.getFirst());
            outputFile.print(COMMA+student.getStreet()+COMMA+student.getCity());
            outputFile.print(", "+student.getState()+" "+student.getZip());
            outputFile.println(COMMA+student.getZip());

            outputFile.println(COMMA+COMMA+student.getDepartment()+COMMA+owedPay);

        }

        //Set up to print all employees
        outputFile.println("                            ");
        outputFile.println(" ALL EMPLOYEES              ");
        outputFile.println(" -------------------------  ");

        for(StudentEmployee student : studentEmployee){

            outputFile.printf("%.15s %.15s",student.getLast(),student.getFirst()+COMMA);
            outputFile.println(student.getSocial());

        }

        for (Faculty worker : faculty){
            outputFile.printf("%.15s %.15s",worker.getLast(),worker.getFirst()+COMMA);
            outputFile.println(worker.getSocial());
        }



//=========================================================================
//Cleanup
//=========================================================================
    inFile.close();
    outputFile.close();
    } //End main

}//End class
