/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-03-15
*/

public class Employee extends Person {
//=============================================================================
//Properties
//=============================================================================

    private String ssn;
    private String deptName;

//=============================================================================
//Constructors
//=============================================================================

    //Empty constructor
    public Employee(){}

    //Cosntructor with private data
    public Employee(String first, String last, String street, String city, String state, String zipCode, String social, String department){

        super(first, last, street, city, state, zipCode);
        this.ssn = social;
        this.deptName = department;
    }

//=============================================================================
//Setters
//=============================================================================

    //set ssn
    public void setSocial(String social){
        this.ssn = social;
    }

    //set department
    public void setDepartment(String department){
        this.deptName = department;
    }

//=============================================================================
//Getters
//=============================================================================

    //Get ssn
    public String getSocial(){
        return this.ssn;
    }

    //Get department
    public String getDepartment(){
        return this.deptName;
    }

} //End class
