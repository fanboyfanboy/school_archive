/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-03-15
*/

public class Person {
//=============================================================================
//Properties
//=============================================================================

    private String firstName;
    private String lastName;
    private String street;
    private String city;
    private String state;
    private String zipCode;

//=============================================================================
//Constructors
//=============================================================================

    //Empty constructor
    public Person(){}

    //Constructor with private data
    public Person(String first, String last, String street, String city, String state, String zipCode)
    {
        this.firstName = first;
        this.lastName = last;
        this.street = street;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

//=============================================================================
//Setters
//=============================================================================

    //Set firstName
    public void setFirst(String first){
        this.firstName = first;
    }

    //Set lastName
    public void setLast(String last){
        this.lastName = last;
    }

    //set street
    public void setStreet(String street){
        this.street = street;
    }

    //set city
    public void setCity(String city){
        this.city = city;
    }

    //set state
    public void setState(String state){
        this.state = state;
    }

    //Set zipCode
    public void setZip(String zip){
        this.zipCode = zip;
    }

//=============================================================================
//Getters
//=============================================================================

    //get firstName
    public String getFirst(){
        return this.firstName;
    }

    //get lastName
    public String getLast(){
        return this.lastName;
    }

    //get street
    public String getStreet(){
        return this.street;
    }

    //get state
    public String getState(){
        return this.state;
    }

    //get city
    public String getCity(){
        return this.city;
    }

    //Get zipCode
    public String getZip(){
        return this.zipCode;
    }


} //End class
