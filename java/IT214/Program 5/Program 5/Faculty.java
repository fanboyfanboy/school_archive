/**
*@author Dalton Cothron
*@version 1.0
*@since 2015-03-15
*/

public class Faculty extends Employee {


//=============================================================================
//Properties
//=============================================================================

    private Double salary;

//=============================================================================
//Constructors
//=============================================================================

    //Empty Constructor
    public Faculty(){
    }

    //Constructor with private data
    public Faculty(String first, String last, String street, String city, String state, String zipCode, String social, String department, Double salary){

        super(first, last, street, city, state, zipCode, social, department);
        this.salary = salary;
    }

//=============================================================================
//Setters
//=============================================================================

    //Set salary
    public void setSalary(Double salary){
        this.salary = salary;
    }

//=============================================================================
//Getters
//=============================================================================

    //get Salary
    public Double getSalary(){
        return this.salary;
    }

}//end Class
