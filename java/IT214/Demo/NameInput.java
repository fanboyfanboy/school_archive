import java.util.Scanner;
import java.util.regex.*;

public class NameInput
{

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Please enter your name: ");
        String nameInput = input.nextLine();

        double namePercentage = percentAlpha(nameInput);

        boolean done = false;

        while(!done)
        {
            //Below 80%. Force re-entry of name.
            //Had to lower to below %80 due to 90% causing to mainy fails in testing.

            if(namePercentage <=0.799999999)
            {
                System.out.print("Please re-input your name: ");
                nameInput = input.nextLine();
                namePercentage = percentAlpha(nameInput);
            }

            //If between 90 and 99, ask if the input was correct
            else if(namePercentage >0.79999999 && namePercentage <= 0.99)
            {
                System.out.print("Are you sure you entered your name correctly?");
                System.out.println("Enter 'Y' for yes, or 'N' for no. ");
                String correct = input.nextLine();

                boolean correctInput = false;
                    //Check for correct input
                while(!correctInput) //without loop it will ask twice if you incorrectly put anything other than a capitol N.
                {
                    if(correct.equals("Y")){ correctInput = true; done = true; }

                    else if(correct.equals("N")) //Make them re-enter name.
                    {
                        System.out.print("Please re-enter your name: ");
                        nameInput = input.nextLine();
                        namePercentage = percentAlpha(nameInput);
                        correctInput = true;
                    }

                    else //Neither Y or N. Get correct input.
                    {
                        System.out.print("Incorrect Input. Please enter 'Y' or 'N': ");
                        correct = input.nextLine();
                    }
                }
            }
            //Above 99.01 name is correct
            else if (namePercentage >= 0.990000001){ done = true; }
        }
    }

    //Check percentage for amount of characters that are alpha
    public static double percentAlpha(String s){
        return (double)countMatches(s,"\\p{L}")/s.length();
    }

    //Check against regex
    public static int countMatches(String s, String regex){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        int count = 0;
        while (matcher.find()){
            count++;
        }
        return count;
    }
}
