/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class NumberException extends Exception {
    
    /**
     * Constructs a new NumberException
     * 
     * @param e the error message to report
     */
    public NumberException(String e) {
        super(e);
    }
}
