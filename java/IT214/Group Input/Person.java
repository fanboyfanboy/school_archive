/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class Person {
    private Address address = new Address();
    private Phone phone = new Phone();
    private String name = "";
    
    public Person(String name, Address address, Phone phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }
    public Person() {
        
    }
    
    /*
    how to close on submit or cancel
    how to throw exception from cancel
    how to throw exception on close
    */
    
    public void requestUserInfo() {
        RequestDialog request = new RequestDialog();
        
        this.setPhone(request.getPhone());
        this.setAddress(request.getAddress());
        this.setName(request.getName());
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public Address getAddress() {
        return this.address;
    }
    
    public void setPhone(Phone phone) {
        this.phone = phone;
    }
    public Phone getPhone() {
        return this.phone;
    }
    
}
