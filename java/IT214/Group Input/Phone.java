/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class Phone {

    public static final int HOME = 1;
    public static final int WORK = 2;
    public static final int FAX = 3;
    public static final int MOBILE = 4;
    public static final int DEFAULT = 0;

    private String number;
    private int type = DEFAULT;

    /**
     *
     * @param type HOME, WORK, FAX, MOBILE, DEFAULT
     * @param number the phone number
     * @throws InvalidNumberException phone number was bad
     * @throws InvalidNumberTypeException type was wrong
     */
    public Phone(int type, String number) throws InvalidNumberException, InvalidNumberTypeException {
        if (!isValidNumber(number)) {
            throw new InvalidNumberException("Invalid phone number.");
        }
        this.number = number;
        this.type = type;
    }

    /**
     * 
     * @param number the phone number
     * @throws InvalidNumberException the number was invalid
     */
    public Phone(String number) throws InvalidNumberException {
        if (!isValidNumber(number)) {
            throw new InvalidNumberException("Invalid phone number.");
        }
        this.number = number;
        this.type = DEFAULT;
    }

    public Phone() {
        this.number = "";
        this.type = DEFAULT;
    }

    /**
     *
     * @param type The type of phone number: WORK, HOME, PHONE, FAX, MOBILE,
     * DEFAULT
     * @throws InvalidNumberTypeException
     */
    public void setType(int type) throws InvalidNumberTypeException {
        this.type = type;
    }

    /**
     *
     * @return the type of phone number: WORK, HOME, PHONE, FAX, MOBILE, DEFAULT
     */
    public int getType() {
        return this.type;
    }

    /**
     * Sets the phone number
     *
     * @param number a valid phone number
     * @throws InvalidNumberException
     */
    public void setNumber(String number) throws InvalidNumberException {
        if (!isValidNumber(number)) {
            throw new InvalidNumberException("The number given was not valid.");
        } else {
            this.number = number;
        }
    }

    /**
     *
     * @return The phone number
     */
    public String getNumber() {

        return this.number;

    }

    /**
     *
     * @return the phone number with formatting, e.g. (555) 555-0142 or
     * 1-555-555-0142
     */
    public String getNumberFormatted() {

        String num = this.getNumber();

        /* Rough explanation for my group members...
         http://www.regular-expressions.info/tutorial.html
        
         ^ is an anchor meaning match at the start of the string
         \d means a digit [0-9]
         {5} specifies repetition for the preceding digit, 5 times
         $ is an anchor meaning match at the end of the string
         In the second expression, 2 and 3 digits are inside a capturing group: ()
         The string is replaced with the first and second backreference, $1 and $2 (with a dash between)
        
         */
        if (num.matches("^\\d{5}$")) {
            return num.replaceFirst("^(\\d{2})(\\d{3})$", "$1-$2");
        } //same as above, but for 10 digits
        else if (num.matches("^\\d{10}$")) {
            return num.replaceFirst("^(\\d{3})(\\d{3})+(\\d{4})$", "($1) $2-$3");
        } /*
        
         (?<!^) is negative lookbehind, it backtracks (looks left of the current position)
         and will fail the match if it matches ^ (start of string)
         (this is to prevent a dash from getting added at the start of a number)
         (\d{3}) this is the actual match surrounded by capturing brackets. 3 digits.
         (?=) specifies positive lookahead, in other words it checks ahead and will fail
         if it can't find the expression it's looking for
         (?:\d{3}) 3 digits inside a non-capturing group
         * the preceding group repeats 0 or more times
         (so it only matches if there are 3, 6, 9, etc digits ahead)
         $ match the end of a string
        
         Before the replacement happens, the last 4 digits are removed and appended later
        
         */ else if (num.matches("\\d{10,}")) {
            String suffix = "-" + num.substring(num.length() - 4);
            num = num.substring(0, num.length() - 4);
            return num.replaceAll("(?<!^)(\\d{3})(?=(?:\\d{3})*$)", "-$1") + suffix;
        }
         else return num;
    }

    /**
     * Extracts a phone number from a string by removing anything not a digit
     *
     * @param number the string to extract a phone number from
     * @return the string with anything not a digit removed
     * @throws NumberNotFoundException if no valid number could be found
     */
    public static String extractNumber(String number) throws NumberNotFoundException {
        /*
         \D is equivalent to [^0-9] (anything not a digit)
         so any character that's not a digit is replaced with an empty string
         */
        number = number.replaceAll("\\D", "");
        if (!isValidNumber(number)) {
            throw new NumberNotFoundException("Phone number should be 10 or 11 digits.");
        } else {
            return number;
        }
    }

    /**
     * Checks if a string is a valid phone number
     *
     * @param number string to check
     * @return true/false
     */
    public static boolean isValidNumber(String number) {

        //we'll pretend life is simple and international number formats don't exist...
        return number.matches("^(?:\\d{10,11})$");

    }

}
