/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class NumberNotFoundException extends NumberException {
    
    /**
     * Constructs a new NumberNotFoundException
     * 
     * 
     * @param e the error message to report 
     */
    public NumberNotFoundException(String e){
        super(e);
    }
}
