/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class InvalidAddressException extends Exception {
    
    /**
     * Constructs a new InvalidAddressException
     * 
     * @param e the error message to report 
     */
    public InvalidAddressException(String e) {
        super(e);
    }
    
}
