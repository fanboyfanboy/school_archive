/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.regex.*;

/**
 *
 * @author Travis
 */
public class Valid {

    public static final int NAME_INVALID = 2;
    public static final int NAME_VALID = 0;
    public static final int NAME_CONFIRMCORRECT = 1;

    /**
     * Checks if a string is likely a name
     *
     * @param name String to check
     * @return NAME_INVALID, NAME_VALID, NAME_CONFIRMCORRECT
     */
    public static int name(String name) {
        name = name.trim();

        
        if (name.length() < 2) {
            return NAME_INVALID;
        }
        //if there is a dash it shouldn't be at the front or end of the string
        if ((name.charAt(0) == '-') || (name.charAt(name.length() - 1) == '-')) {
            return NAME_INVALID;
        } 

        //contains any characters not -, ., letter
        if (name.matches("[^-\\p{L}.]")) {
            return NAME_CONFIRMCORRECT;
        }

        //contains ONLY characters not letters
        if (name.matches("^[^\\p{L}]+$")) {
            return NAME_INVALID;
        }

        //0-1 characters
        return NAME_VALID;
    }

    /**
     * Checks the percent of letters in a string Letters are anything in the
     * "letter" category of Unicode
     *
     * @param s String to check
     * @return double between 0 and 1 (percent of characters that are letters)
     */
    private static double percentAlpha(String s) {
        //number of letters divided by number of non-whitespace characters
        return (double) countMatches(s, "\\p{L}") / countMatches(s, "\\S");

    }

    /**
     * Counts the number of occurrences of an expression in a string
     *
     * @param s The string to check
     * @param regex The expression to look for
     * @return The number of matches found
     */
    private static int countMatches(String s, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        int count = 0;
        while (matcher.find()) { //loop until can't find more matches
            count++;
        }

        return count;
    }

}
