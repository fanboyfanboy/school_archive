/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class InvalidNumberException extends NumberException {
    /**
     * Constructs a new InvalidNumberException
     * 
     * @param e the error message to report
     */
    public InvalidNumberException(String e) {
        super(e);
    }
}
