/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class InvalidNumberTypeException extends NumberException {
    /**
     * Constructs a new InvalidNumberTypeException
     * 
     * @param e the error message to report
     */
    public InvalidNumberTypeException(String e) {
        super(e);
    }
}
