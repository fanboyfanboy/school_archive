/*
 Group 1 Input project

 Travis Britz
 Dalton Cothron
 Tyler Phillips
 Supreme Shrestha
 Ranendra Lakha
 */

/**
 *
 * @author Travis, Dalton, Supreme, Ranendra, Tyler (Group 1)
 */
public class GroupInputProjectMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new RequestDialog();
    }

}
