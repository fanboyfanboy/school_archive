/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class Address {

    public static final int VALID = 0;
    public static final int INVALID = 1;
    public static final int CONFIRM = 2;
    private String line1 = "";
    private String line2 = "";
    private String zipcode = "";
    private String state = "";
    private String city = "";

    public Address(String line1, String line2, String city, String state, String zipcode) throws InvalidAddressException, InvalidZipcodeException, InvalidCityException {
        this.setAddress(line1, line2);
        this.setCity(city);
        this.setState(state);
        this.setZip(zipcode);

    }

    public Address() {

    }

    /**
     * Sets the address.
     *
     * @param line1 the first address line. can be empty string
     * @param line2 the second address line. can be empty string
     * @throws InvalidAddressException
     */
    public void setAddress(String line1, String line2) throws InvalidAddressException {
        if (isValidAddress(line1, line2) == INVALID) {
            throw new InvalidAddressException("Invalid address.");
        } else {
            this.line1 = line1;
            this.line2 = line2;
        }
    }

    /**
     * Checks both address lines and returns an int for VALID, INVALID, or
     * CONFIRM
     *
     * @param line1 the first address line. can be empty
     * @param line2 the second address line. can be empty
     * @return VALID, INVALID, CONFIRM
     */
    public static int isValidAddress(String line1, String line2) {
        line1 = line1.trim();
        line2 = line2.trim();

        String words1[] = line1.split(" +");
        String words2[] = line2.split(" +");

        //line 1 was empty
        if (words1.length == 1) {

            //line 2 was too short (and 1 was empty)
            if (words2.length < 2) {
                return INVALID;
            }

            //the first word didn't start with a number
            if (!words2[0].matches("^\\d")) {
                return CONFIRM;
            }
        }

        //line 2 was empty
        if (words2.length == 1) {

            //line 1 was too short (and 2 was empty)
            if (words1.length < 2) {
                return INVALID;
            }

            //the first word didn't start with a number
            if (!words1[0].matches("^\\d")) {
                return CONFIRM;
            }
        }

        //neither line contained any letters
        if (line1.matches("^[^\\p{L}]$") && line2.matches("^[^\\p{L}]$")) {
            return INVALID;
        }

        return VALID;
    }

    /**
     * Checks the city name and returns an int for VALID, INVALID, or CONFIRM
     *
     * @param city the city name to check
     * @return VALID, INVALID, CONFIRM
     */
    public static int isValidCity(String city) {

        city = city.trim();

        //too short
        if (city.length() < 2) {
            return INVALID;
        }
        //if there is a dash it shouldn't be at the front or end of the string
        if ((city.charAt(0) == '-') || (city.charAt(city.length() - 1) == '-')) {
            return INVALID;
        }

        //contains any characters not -, ., letter
        if (city.matches("[^-\\p{L}.]")) {
            return CONFIRM;
        }

        //contains ONLY characters not letters
        if (city.matches("^[^\\p{L}]+$")) {
            return INVALID;
        }

        //0-1 characters
        return VALID;
    }

    public void setCity(String city) throws InvalidCityException {
        if (isValidCity(city) == INVALID) {
            throw new InvalidCityException("The city name was invalid.");
        }
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setZip(String zipcode) throws InvalidZipcodeException {
        if (!isValidZipcode(zipcode)) {
            throw new InvalidZipcodeException("Invalid zipcode.");
        } else {
            this.zipcode = zipcode;
        }
    }

    public String getZip() {
        return this.zipcode;
    }

    public boolean isValidZipcode(String zipcode) {
        return zipcode.matches("^\\d{5}$");
    }

    private String realGetLine(int number) {

        String[] line = {"", "", ""};
        String line3 = this.city + ", " + this.state + ", " + this.zipcode;

        int i = 0;
        if (!this.line1.equals("")) {
            line[i++] = this.line1;
        }
        if (!this.line2.equals("")) {
            line[i++] = this.line2;
        }
        line[i] = line3;

        return line[number];
    }

    /**
     * Gets the first address line
     *
     * @return address line 1. can be empty string.
     */
    public String getLineOne() {
        return realGetLine(0);
    }

    /**
     * Gets the second address line
     *
     * @return address line 2. can be empty string
     */
    public String getLineTwo() {
        return realGetLine(1);
    }

    /**
     * Gets the third address line
     *
     * @return address line 3. can and often will be empty string
     */
    public String getLineThree() {
        return realGetLine(2);
    }
}
