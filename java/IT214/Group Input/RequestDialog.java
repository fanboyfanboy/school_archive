
import javax.swing.*;
import java.awt.event.*;

 //we should probably have used JDialog but we ran out of time to figure out how
public class RequestDialog extends JFrame implements ActionListener {
//Run the program

    //Set JFrame size
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 200;

    private JButton submit, clear, cancel; //Option buttons for GUI

    //All the lables needed for the GUI
    private JLabel fName, lName, addressLabel1, addressLabel2, cityLabel,
            stateLabel, zipLabel, phoneLabel;

    //All the textfields needed for the GUI
    private JTextField fNameText, lNameText, addressText1, addressText2,
            cityText, zipText, phoneText;

    //Combo box (dropdown menu) for 50 states
    private JComboBox stateBox;

    //Strings for textField entries
    String entryCity, entryState, entryAddress1, entryAddress2, entryZipcode,
            entryFirstname, entryLastname, entryPhone;

    private Phone phone = new Phone();
    private Address address = new Address();

//Set up the JFrame
    public RequestDialog() {
        /*Call methods to set up Panel and JFrame objects
         createPanel(); must be entryLastname or it will cause an exception by
         trying to add objects to the Panel after already being created
         */
        createLabels();
        createButton();
        createTextField();
        createComboBox();
        createPanel();

        //Customize the JFrame
        setTitle("Input");
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    } //End constructor

//Method for creating all necessary Text fields
    private void createTextField() {
        //Create text fields
        final int FIELD_WIDTH = 13;
        fNameText = new JTextField(FIELD_WIDTH);
        lNameText = new JTextField(FIELD_WIDTH);
        addressText1 = new JTextField(FIELD_WIDTH);
        addressText2 = new JTextField(FIELD_WIDTH);
        cityText = new JTextField(FIELD_WIDTH);
        zipText = new JTextField(FIELD_WIDTH);
        phoneText = new JTextField(FIELD_WIDTH);

    } //end text field and label method

//Method for creating all necessary labels
    private void createLabels() {
        //Create labels
        fName = new JLabel("First Name");
        lName = new JLabel("Last Name");
        addressLabel1 = new JLabel("Street Address");
        addressLabel2 = new JLabel("Address line 2");
        cityLabel = new JLabel("City");
        stateLabel = new JLabel("State");
        zipLabel = new JLabel("Zip Code");
        phoneLabel = new JLabel("Phone Number");
    } //End createLabels method

//Method to create buttons
    private void createButton() {
        submit = new JButton("Submit");
        clear = new JButton("Clear");
        cancel = new JButton("Cancel");
        submit.addActionListener(this);
        clear.addActionListener(this);
        cancel.addActionListener(this);

    }//end createButton method

    //Method to create the combo box with States
    private void createComboBox() {
        //Declare and initialize array with all 50 state abbreviations
        String[] stateList = {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL",
            "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN",
            "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR",
            "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};

        //Create combo box, set to list of states
        stateBox = new JComboBox(stateList);
        stateBox.addActionListener(this);

    }// end combo box method

    //method for when buttons are pressed
    public void actionPerformed(ActionEvent event) {
        //Actions to perform if submit it selected
        if (event.getSource() == submit) {

            entryFirstname = stripWhiteSpace(fNameText.getText());
            if ((Valid.name(entryFirstname) == Valid.NAME_CONFIRMCORRECT)
                    && (!getYesNo("Is your first name correct?"))) {
                return;
            } else if (Valid.name(entryFirstname) == Valid.NAME_INVALID) {
                getOk("First name is invaild.");
                return;
            }

            entryLastname = stripWhiteSpace(lNameText.getText());
            if ((Valid.name(entryLastname) == Valid.NAME_CONFIRMCORRECT)
                    && (!getYesNo("Is your last name correct?"))) {
                return;
            } else if (Valid.name(entryLastname) == Valid.NAME_INVALID) {
                getOk("Last name is invaild.");
                return;
            }

            entryAddress1 = stripWhiteSpace(addressText1.getText());
            entryAddress2 = stripWhiteSpace(addressText2.getText());

            if ((Address.isValidAddress(entryAddress1, entryAddress2) == Address.CONFIRM)
                    && (!getYesNo("Are you sure this address is correct?"))) {
                return;
            }

            entryCity = stripWhiteSpace(cityText.getText());
            if ((Address.isValidCity(entryCity) == Address.CONFIRM)
                    && (!getYesNo("Are you sure this city is correct?"))) {
                return;
            }

            entryState = (String) stateBox.getSelectedItem();

            entryZipcode = stripWhiteSpace(zipText.getText());
            entryPhone = stripWhiteSpace(phoneText.getText());

            try {
                address.setAddress(entryAddress1, entryAddress2);
                address.setCity(entryCity);
                address.setState(entryState);
                address.setZip(entryZipcode);
                phone.setNumber(Phone.extractNumber(entryPhone));

            } catch (InvalidNumberException | NumberNotFoundException //
                    | InvalidZipcodeException | InvalidAddressException | InvalidCityException e) {
                getOk(e.getMessage());
                return;
            }

            //if we reach here everything SHOULD be ok...
            System.out.println(entryFirstname + " " + entryLastname);
            System.out.println(address.getLineOne());
            if (!address.getLineTwo().equals("")) {
                System.out.println(address.getLineTwo());
            }
            if (!address.getLineThree().equals("")) {
                System.out.println(address.getLineThree());
            }
            System.out.println(phone.getNumberFormatted());
        }

        //Actions to perform if clear is selected
        if (event.getSource() == clear) {

            fNameText.setText("");
            lNameText.setText("");
            addressText1.setText("");
            addressText2.setText("");
            cityText.setText("");
            zipText.setText("");
            phoneText.setText("");

        }

        //Actions to perform if cancel is selected
        //Still unsure what exactly we plan to do with this button?
        if (event.getSource() == cancel) {
            System.exit(0);
        }

        //Gets the choice from the combo box
//        if (event.getSource() == stateBox) {
//            entryState = (String) stateBox.getSelectedItem();
//        }
    }  //End actions Performed method

//Method to create the panel to lay on top of the JFrame
    private void createPanel() {
        JPanel panel = new JPanel();
        //Add to panel
        panel.add(fName);
        panel.add(fNameText);
        panel.add(lName);
        panel.add(lNameText);
        panel.add(addressLabel1);
        panel.add(addressText1);
        panel.add(addressLabel2);
        panel.add(addressText2);
        panel.add(cityLabel);
        panel.add(cityText);
        panel.add(stateLabel);
        panel.add(stateBox);
        panel.add(zipLabel);
        panel.add(zipText);
        panel.add(phoneLabel);
        panel.add(phoneText);
        panel.add(submit);
        panel.add(clear);
        panel.add(cancel);
        add(panel);

    } //End createPanel method

    //Returns address object
    public Address getAddress() {
        return this.address;
    }

    //return a concatonated string with the entryFirstname and entryLastname name
    public String getName() {
        String fullName = entryFirstname + " " + entryLastname;
        return fullName;
    }

    //returns phone object with parameter string
    public Phone getPhone() {
        return this.phone;
    }

    private static String stripWhiteSpace(String string) {
        //removes leading and trailing whitespaces
        //consecutive whitespaces are turned into a single space
        return string.trim().replaceAll("\\s+", " ");
    }

    /**
     * Asks a yes/no question
     *
     * @param inputQuestion Question to ask (empty string for default)
     * @return true/false for yes/no
     * @throws InputCancelException
     */
    public static boolean getYesNo(String inputQuestion) {
        if (inputQuestion.equals("")) {
            inputQuestion = "Continue?";
        }
        int response = JOptionPane.showConfirmDialog(null, inputQuestion, "Confirm",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.NO_OPTION) {
            return false;
        } else if (response == JOptionPane.YES_OPTION) {
            return true;
        } else if (response == JOptionPane.CLOSED_OPTION) {
            return false;
        } //should never reach here?
        else {
            return false;
        }
    }

    private static void getOk(String inputQuestion) {
        if (inputQuestion.equals("")) {
            inputQuestion = "Click OK";
        }

        JOptionPane.showMessageDialog(null, inputQuestion);
    }

} //End class
