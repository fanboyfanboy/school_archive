/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class InvalidZipcodeException extends Exception {
    /**
     * Constructs a new InvalidZipcodeException
     * 
     * @param e the error message to report
     */
    public InvalidZipcodeException(String e) {
        super(e);
    }
}
