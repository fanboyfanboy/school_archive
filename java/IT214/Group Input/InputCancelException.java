/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Travis
 */
public class InputCancelException extends Exception {

    /**
     * Constructs a new InputCancelException
     *
     * @param e The error message to report
     */
    public InputCancelException(String e) {
        super(e);
    }
}
