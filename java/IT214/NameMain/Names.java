public class Names
{
	private String firstName;
	private String lastName;

	Names() {
		firstName="first";
		lastName="last";
	}

	Names(String first, String last) {
		firstName=first;
		lastName=last;
	}

	public void printName() {
		System.out.println(firstName+" "+lastName);
	}

	public String comboFirsts(Names varName) {
		return this.firstName+varName.firstName;
	}

	public String getFullName()
	{
		return this.firstName + " " + this.lastName;
	}
}
