import java.io.FileNotFoundException;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/*
Name Array

Dalton Cothron
Travis Britz
Tyler Phillips
Ranendra Lakha
Supreme Shrestha

*/


public class NameMain
{
	public static void main(String[] args) throws FileNotFoundException
	{

	File file = new File("Names.csv");

	Scanner inFile = new Scanner(file);

	String COMMA = ",";


	ArrayList<Names> namesList = new ArrayList<>();

	while(inFile.hasNextLine())

		{
			String fileLine = inFile.nextLine();

			String [] line = fileLine.split(COMMA);

			namesList.add(new Names(line[0],line[1]));


		}

		for(Names name : namesList)
		{
			System.out.println(name.getFullName());
		}

	}  // end main
} // end class
