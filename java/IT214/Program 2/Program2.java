//Dalton Cothron
//Program 2

import java.util.Scanner;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Program2
{

    public static void main(String[] args) throws FileNotFoundException
    {

        File inFile = new File("tax.csv"); //csv input file

        Scanner csvFile = new Scanner(inFile); //read lines from file

        PrintWriter outFile = new PrintWriter("vf4049nv.csv"); //csv output file

        final int EXEMPTION = 3950; //personal exception
        final int SDEDUCT = 6200; //standard deduction

        String COMMA = ","; //set deliminator
        outFile.println("Gross Income"+COMMA+"Status"+COMMA+"Taxes Owed");

        while(csvFile.hasNextLine())
        { //go until there are no more lines

            String fileLine = csvFile.nextLine(); //read next line
            String [] line = fileLine.split(COMMA); //split line objects

            String grossIncome = line[0];
            String status = line[1];

            double income = Double.parseDouble(grossIncome); //get income
            double taxOwed; //declare owed tax total
            double taxIncome;

            //check if they are single
            if (status.equals("S"));
            {
                double phaseOut = income - SDEDUCT;

                if (phaseOut <= 376700){ //test for phase out
                    taxIncome = phaseOut - EXEMPTION;
                }
                else{taxIncome = phaseOut;}

                //see where all the owed taxes lie
                if (taxIncome < 0){
                    taxOwed = 0;
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if (taxIncome <= 9075 && taxIncome >= 0){
                    taxOwed = taxIncome * .1;
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 9075 && taxIncome < 36900){
                    taxOwed = 907.5 + (.15 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 36900 && taxIncome < 89350){
                    taxOwed = 5081.25 + (.25 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 89350 && taxIncome < 186350){
                    taxOwed = 18193.75 + (.28 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 186350 && taxIncome < 405100){
                    taxOwed = 45353.75 + (.33 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 405100 && taxIncome < 406750){
                    taxOwed = 117541.25 + (.35 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 406750){
                    taxOwed = 118118.75 + (.396 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
            }
            if (status.equals("J"));
            {

                double phaseOut = income - (SDEDUCT * 2);
                if(phaseOut <= 427550){
                    taxIncome = phaseOut - (EXEMPTION * 2);
                }
                else{taxIncome = phaseOut;}
                if(taxIncome < 18150){
                    taxOwed = .1 * taxIncome;
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 18150 && taxIncome < 73800){
                    taxOwed = 1815 + (.15 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 73800 && taxIncome < 148850){
                    taxOwed = 10162.50 + (.25 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 148850 && taxIncome < 226850){
                    taxOwed = 28925 + (.28 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 226850 && taxIncome < 405100){
                    taxOwed = 50765 + (.33 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 405100 && taxIncome < 457600){
                    taxOwed = 109587.50 + (.35 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
                else if(taxIncome > 457600){
                    taxOwed = 127962.50 + (.396 * taxIncome);
                    outFile.println(income+COMMA+status+COMMA+taxOwed);
                }
            }
        }
        outFile.close();
        csvFile.close(); //close both files
    }
}
