# Abstract Data Type : Doubly Linked List
# A Doubly Linked List DLL supports
# DLL.is_empty()
# len(DLL)
# DLL._insert_between(e, predecessor, successor)
# DLL._delete_node(e)

class _DoublyLinkedList:
    """Base class for a doubly linked list with header and trailer sentinels"""
    """Non-circular Linked List with head, tail"""

    # ------------- nested _Node class ---------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_previous', '_next'  # memory efficiency, google it

        def __init__(self, element, previous, next):
            self._element = element
            self._previous = previous
            self._next = next

    # ------------------------------------------------
    # ------------- nested Error class ---------------
    class Empty(Exception):
        pass
    # ------------------------------------------------

    def __init__(self):
        self._header = self._Node(None, None, None)
        self._trailer = self._Node(None, None, None)
        self._header._next = self._trailer
        self._trailer._previous = self._header
        self._size = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def _insert_between(self, e, predecessor, successor):
        new_node = self._Node(e, predecessor, successor)
        predecessor._next = new_node
        successor._previous = new_node
        self._size += 1
        return new_node

    def _delete_node(self, anode):
        """Should not delete sentinel nodes."""
        prev_ = anode._previous
        next_ = anode._next
        prev_._next = next_
        next_._previous = prev_
        self._size -= 1
        temp = anode._element
        anode._previous = anode._next = anode._element = None
        return temp

if __name__ == "__main__":
    dll = _DoublyLinkedList()

    def fun():
        length = len(dll)
        temp = dll._header
        print(length)
        for i in range(length + 2):  # include header and trailer sentinels
            print("ELEMENT: " + str(temp._element))
            print("ADDRESS: " + str(temp))
            print("NEXT_ADDRESS: " + str(temp._next))
            print("-"*10)
            temp = temp._next
        print()

    fun()
    a = dll._insert_between("A", dll._header, dll._trailer)
    fun()
    b = dll._insert_between("B", a, a._next)
    fun()
    element = dll._delete_node(a)
    fun()
    print("Item removed from the doubly linked list is ", element)

















