# Abstract Data Type : Singly Linked List
# A Singly Linked List SLL supports:
# SLL.head(): returns private pointer to head of list
# SLL.tail(): returns private pointer to tail of list
# len(SLL): returns number of elements
# SLL.is_empty(): returns True if len(SLL) is zero
# SLL.insert_head(e): insert new node e at the head of list, update head to e
# SLL.insert_tail(e): insert new node e at the tail of list, update tail to e
# SLL.remove_head(): updates head of list to e.next reference, returns head of list
# SLL.remove_tail(): update tail of list to second-to-last element of list
# (requires traversing the list), set second-to-last element link to None

class SLinkedList:
    """Non-circular Linked List with head, tail"""
    # ------------- nested _Node class ---------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_link'   # memory efficiency, google it

        def __init__(self, element, link):
            self._element = element
            self._link = link

        def __repr__(self):
            return '[{0},{1}]'.format(self._element, self._link)
    # ------------------------------------------------
    # ------------- nested Error class ---------------
    class Empty(Exception):
        pass
    # ------------------------------------------------

    def __init__(self):
        self._head = None           # A node
        self._tail = self._head
        self._size = 0

    def __len__(self):
        return self._size

    def head(self):
        if self._size == 0:
            return None
        return self._head._element

    def tail(self):
        if self._head is not None:
            return self._tail._element
        return None

    def is_empty(self):
        return len(self) == 0

    def insert_head(self, e):
        new_head = self._Node(e, self._head)
        self._head = new_head
        if self._size == 0:
            self._tail = new_head
        self._size += 1

    def insert_tail(self, e):
        new_tail = self._Node(e, None)
        if self._size == 0:
            self._head = new_tail
        else:
            self._tail._link = new_tail
        self._tail = new_tail
        self._size += 1

    def remove_head(self):   # :(
        if self.is_empty():
            raise self.Empty("Linked List is empty")
        temp = self._head
        self._head = self._head._link
        self._size -= 1
        element = temp._element
        temp._element = temp._link = None    # help garbage collection
        return element

if __name__ == "__main__":
    # Tesing SLinkedList
    SLL = SLinkedList()
    print(SLL.head())
    print(SLL.tail())
    print(SLL)
    print()

    #SLL.insert_head("A")
    SLL.insert_tail("A")
    #print(SLL.head()._element, SLL.head()._link)
    print(SLL.head())
    #print(SLL.tail()._element, SLL.tail()._link)
    print(SLL.tail())
    print()

    #SLL.insert_head("B")
    SLL.insert_tail("B")
    #print(SLL.head()._element, SLL.head()._link)
    print(SLL.head())
    print(SLL.tail())
    #print(SLL.tail()._element, SLL.tail()._link)
    print()

    SLL.insert_tail("C")
    print(SLL.head())
    #print(SLL.head()._link)
    print(SLL.tail())
    print(len(SLL))
    print()

    print(SLL.remove_head())
    print(len(SLL))
    print(SLL.head())
    print(SLL.tail())




























