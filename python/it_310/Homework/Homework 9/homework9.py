# Dalton Cothron
# Homework 9
# It310 - Spring 2017


##############################################################################################
# Imports
from time import time
from PositionalLists import PositionalList
from random import shuffle



##############################################################################################
# Problem R-9.3
# **NOTE TO PROFESSOR** PROBLEM 9.3 ANSWERS ARE IN SCREENSHOT INCLUDED IN homework9.ZIP FILE


##############################################################################################
# Problem R-9.7
# Selection Sort function (execution in tests section below)

def selectionSort(aList):
	for i in range(len(aList)-1, 0, -1):
		posOfMax = 0;
		for j in range(1, i+1):
			if aList[j]>aList[posOfMax]:
				posOfMax=j;
			temp = aList[i];
			aList[i] = aList[posOfMax];
			aList[posOfMax] = j;


##############################################################################################
# Problem R-9.9
# Insertion Sort Function (execution in tests section below) 

def insertionSort(aList):
	for x in range(len(aList)):
		for y in range(x):
			if aList[y]>aList[x]:
				aList.insert(y, aList.pop(x));
				break;


##############################################################################################
# Problem R-9.19


##############################################################################################
# Problem C-9.26


##############################################################################################
# Problem C-9.27


##############################################################################################
# Problem C-9.38


##############################################################################################
# Problem C-9.52


##############################################################################################
# Tests

if __name__ == '__main__':

	# R-9.7
	print("\n**Begin R-9.7 Tests**");
	seq = [22, 15, 36, 44, 10, 3, 9, 13, 29 ,25];
	selectionSort(seq);
	print(seq);
	print("\n**End R-9.7 Tests**");


	print('\n**Begin R-9.7 Tests**');
	# R-9.9
	listOfLists=[];
	for x in range(100):
		a=list(range(1000));
	shuffle(a);
	listOfLists.append(a);

	start=time();
	for i in listOfLists:
		insertionSort(i[:]);
		insertionTime=time()-start;

	print("regular:",insertionTime);
	print("\n**End R-9.9 Tests**");