# Numeric sequences

class Sequence(object):
    def __init__(self, start=0):
        self._current = start

    # needs to be overridden in subclasses
    def _advance(self):
        self._current += 1

    def __next__(self):  # Python iterators are objects with built-in funcion next(i)
        if self._current is None:   # by convention to end a sequence
            raise StopIteration()

        else:
            answer = self._current  # record the current value to return
            self._advance()
            return answer

    def __iter__(self):    # by convention an iterator must return itself as an iterator
        return self

    def print_sequence(self, n):
        print(" ".join(str(next(self)) for j in range(n)))

    # INHERITANCE: inherited attributes: _current
        # inherited methods: __next__, __iter__, print_sequence(), _advance()

class ArithmeticSquence(Sequence):
    def __init__(self, increment=1, start=0):
        super().__init__(start)
        self._increment = increment

    def _advance(self):
        self._current += self._increment


class GeometricSequence(Sequence):
    def __init__(self, base=2, start=1):
        super().__init__(start)
        self._base = base

    def _advance(self):
        self._current *= self._base

# SAMPLE USE OF CLASSES:

# Example 1: Arithmetic sequence defined by a1 = -5, an = an-1 + 2
ari_5_2 = ArithmeticSquence(2, -5)

# Find and print the first 10 elements of this sequence
for i in range(10):
    print(next(ari_5_2), end=" ")

# Example 2: Geometric sequence defined by an = 20 * (1/2) ^ n-1
geo_half_20 = GeometricSequence(.5, 20)

print()

# Find and print the first 10 elements of this sequence
for i in range(10):
    print(next(geo_half_20), end=" ")

































