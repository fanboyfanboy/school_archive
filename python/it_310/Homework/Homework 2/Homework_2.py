# R-2.4 

class Flower(object):

	def __init__(self, name="", numPetals=0, price=0.0):
		self._name = name
		self._numPetals= numPetals
		self._price = price

	def get_name(self):
		return self._name
	def set_name(self, new):
		self._name = new
	def set_num_petals(self, new):
		self._numPetals = new
	def get_num_petals(self):
		return self._numPetals
	def set_price(self, new):
		self._price = new
	def get_price(self):
		return self._price


# R - 2.13

class Vector(object):
	def __init__(self, d):
		self._coords = [0] * d
	def __len__(self):
		return len(self._coords)
	def __getitem__(self, j):
		try:
			return self._coords[j]
		except IndexError:
			print("Index @ position: {0} exceeds vector dimensions." .format(j, len(self)))

	def __mul__(self, other):
		if(len(self) != len(other)):
			raise ValueError("dimensions must match.")
		result = Vector(len(self))
		for j in range(len(self)):
			result[j] = self[j] * 3
		return result

	def __rmul__(self, other):
		if(len(self) != len(other)):
			raise ValueError("dimensions must match.")
		result = Vector(len(self))
		for j in range(len(self)):
			result[j] = 3 * self[j]
		return result

# R - 2.14
	def __str_rep__(self):
		return "<" + str(self._coords)[1:-1] + ">"



# R - 2.16

# By choosing 0 as the argument value in the range, it 
# allows the key default argument to be positive or negative.  
# When calculating the default, the step does not matter 
# whether it is positive or negative because it is using floor 
# division (//) leaving a whole integer. The max() function
# calls list.sort() when performing it's operation which
# compares the two arguments and returns the largest of the
# two.  With the end result being the total length of the given 
# range. (since range does not start @ 0)

# R - 2.19
# 1,256,000 times


# C - 2.28
class PreadatoryCreditCard(CreditCard):
	def __init__(self, customer, bank, acnt, limit, apr):

		super().__init__(customer, bank, acnt, limit)
		self._apr = apr
		self._charges=0
	def charge(self, price):
		self._charges++
		success = super().charge(price)
		if not success:
			self.balance += 5
		return success
	def process_month(self):
		if self._balance > 0:
			monthly_factor = pow(1 + self._apr, 1/12)
			self._balance *=monthly_factor
		self._charges=0

# C - 2.31
class Progression:

	def __init__(self, start=0):
		self._current = start
	def _advance(self):
		self._current += 1
	def __next__(self):
		if self._current is None:
			raise StopIteration()
		else:
			answer = self._current
			self._advance()
			return answer
	def __iter__(self):
		return self
	def print_progressoin(self, n):
		print(' '.join(str(next(self)) for j in range(n)))

class FibonacciProgression(Progression):
	def __init__(self, first=2, second=200):

		super().__init__(first)
			self._prev = second - first
	def _advance(self):
		self._prev, self._current = self._current, self._prev + self._current
# C - 2.32
import math

class sqrtProgression(Progression):
	def __init__(self, first=65536.0):
		super().__init__(first):
	def _advance(self):
		self._prev = math.sqrt(self._current)

# C - 2.33
class Polynomial(Poly):
	def __init__(self, p):
		self._poly = [p[i] * i for i in range(1, len(poly))]
	def get_poly(self, i):
		return self._poly[i]
p_nom = Polynomial(5)
print(Polynomial.get_poly(p_nom))

# C - 2.39

class Polygon(gon):
	def __init__(self):
	def _area(self):
	def _perimeter():

class Triangle(tri):
	super().__init(tri)__(self):
	def _area(self):
	def _perimeter(self):
class Pentagon(pent):
	def super().__init(pent)__(self):
	def _area(self):
	def _perimiter(self):
class Hexagon(hex):
	def super().__init(hex)__(self):
	def _area(self):
	def _perimiter(self):
class Octagon(oct):
	def super().__init(oct)__(self):
	def _area(self):
	def _area(self):
class IsoscelesTriangle(ise):
	def super().__init(ise)__(self):
	def _area(self):
	def _perimiter(self):
class EquilateralTriange(ete):
	def super().__init(ete)__(self):
	def _area(self):
	def _perimeter(self):
class Rectangle(re):
	def super().__init(re)__(self):
	def _area(self):
	def _perimeter(self):
class Square(se):
	def super().__init(se)__(self):
	def _area(self):
	def _perimeter(self):