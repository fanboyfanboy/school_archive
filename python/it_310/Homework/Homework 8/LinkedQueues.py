class LinkedQueue:
    """FIFO queue implementation using a singly linked list for storage"""

    # ----------- nested _Node class --------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_next'  # memory efficiency

        def __init__(self, element, next):
            self._element = element
            self._next = next

        def __repr__(self):
            return '[{0},{1}]'.format(self._element, self._next)

    # ---------------------------------------------

    # ----------- nested Error class --------------
    class Empty(Exception):
        pass
    # ---------------------------------------------

    def __init__(self):
        self._head = None
        self._tail = None
        self._size = 0

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def first(self):
        if self.is_empty():
            raise self.Empty("Queue is empty")
        return self._head._element

    def dequeue(self):
        if self.is_empty():
            raise self.Empty("Queue is empty")
        answer = self._head._element
        self._head = self._head._next
        self._size -= 1
        if self.is_empty():
            self._tail = None
        return answer

    def enqueue(self):
        newest = self._Node(e, None)
        if self.is_empty():
            self._head = newest
        else:
            self._tail._next = newest
        self._tail = newest
        self._size += 1
