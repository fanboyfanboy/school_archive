# R-1.4: Write a short Python function that takes a positive integer n and returns the sum of the squares of all the positive integers smaller than n. 
def sumSquareRoots(n):
	if not isinstance(n, int):
		raise TypeError("n must be an integer! Not: " + type(n))
	elif n<= 0:
		raise ValueError("n must be a positive integer! Not: " + str(n))
	else:
		return sum([x**2 for x in range(n)])
n_value = -1
while n_value<0:
	try:
		n_value = int(input("Please enter a value for n: "))
	except (SyntaxError, EOFError, NameError):
		print("Caught invalid input. Please input a valid integer.")
print(sumSquareRoots(n_value))

# R-1.11: Demonstrate how to use Python's list comprehension syntax to produce the list [1, 2, 4, 8, 16, 32, 64, 128, 256]
print([2**i for i in range(9)])


# R-1.12: Python's random module includes a function choice(data) that returns a random element from a non-empty sequence.  The random  
# module includes a more basic fucntion randrange, with parameterization similar to range.  Using only the randrange function, implement your 
# own version of the choice function
import random

def getRandomChoice(sequence):
	if not sequence:
		print("Must provide a non-empty sequence")
	elif len(sequence) == 1:
		print("Cannot pick random choice with one item in sequence.")
		return sequence[0]
	else:
		return sequence[random.randrange(0,len(sequence)-1)]

sequence_values = []; done  = False
while not done:
	try:
		in_val = input("Please enter sequence value ('wq:' to finish inputs):  ")
		if in_val == 'wq:':
			done = True
		else:
			sequence_values.append(in_val)
	except (EOFError):
		print("Caught invalid input. Please input a valid integer.")

print(getRandomChoice(tuple(sequence_values)))


# C-1.14: Write a short Python function that takes a sequence of integer values and determines if there is a distinct pair of numbers in the
# sequence whose product is odd.
from itertools import combinations

def findDistinctPairOddProducts(sequence_to_check):
	if not sequence:
		print("You must provide a non-empty sequence.")
	elif len(sequence) <=  1:
		print("You cannot have a pair with less than 0 or 1 items in the sequence!")
	else:
		count = 0
		for k in range(0, len(sequence_to_check) - 1):
			if k%2 != 0:
				count +=1
		if count>=2:
			return True
		else:
			return False

distinct_sequence = []; done  = False
while not done:
	try:
		in_val = input("Please enter an integer to add to sequence ('wq:' to finish input): ")
		if in_val == 'wq:':
			 done = True
			 break
		else:
			distinct_sequence.append(int(in_val))
	except (SyntaxError, EOFError, NameError):
		print("Caught invalid input. Please input a valid integer.")
print(getRandomChoice(tuple(distinct_sequence)))

# C- 1.19: Demonstrate how to use Python's list comprehension syntax to produce the list ['a' 'b', 'c', ......, 'z'] 
print([chr(i) for i in range(97, 123)])


# C - 1.21: Write a Python program that repeatedly reads lines from standard input until an eoferror is raised, and then outputs those lines in 
# reverse order (a user can indicate end of input by tapping ctrl-D). 
# README TO PROFESSOR: I have been using 'wq:' for my terminator keyset - I am going to stick with this implementation so it is uniform
done = False
lines = []
while not done:
	try:
		in_val = input("Please enter a value (or 'wq:' to finish input): ")
		if in_val == 'wq:':
			done = True; break
		else:
			lines.append(in_val)
	except(EOFError):
		print("EOFError caught. breaking."); break
print([i for i in reversed(lines)])


#C-1.22: Write a short Python program that takes two arrays a and b of length n storing int values, and returns the dot product of a and b.  That is, 
# it returns an array c of length n such that c[i] = a[i] * b[i], for i = 0, ...., n-1

def dotProduct(first_array, second_array):
	if len(first_array) != len(second_array):
		raise ValueError("Array's are not of same length.  Please make arrays same length in values.")
	elif len(first_array) <1 or len(second_array) <1:
		raise ValueError("Arrays must be at least length of one!")
	else:
		third_array = []
		for i in range(len(first_array)):
			third_array.append(first_array[i] * second_array[i])
		return third_array

seqOne = [1, 2, 3, 4]; seqTwo = [4, 3, 2 ,1]
print(dotProduct(seqOne, seqTwo))


# C-1.23: Give an example of a python code fragment that attempts to write an element to a list based on an index that may be out of bounds.  
# If that index is out of bounds, the program should catch the exception that results, and print the following error message: "Don't try buffer 
# overflow attacks in Python!"
test_list = [1, 2, 3, 4, 5]
try:
	test_list[5]=6
except IndexError:
	print("Index OOB Error caught! ");


# C - 1.27: In Section 1.7, we prvided three different implementations of a generator that computes factors of a given integer.   The third of those 
# implementations, from page 41, was the most efficient, but we noted that it did not yield the factors in increasing order.  Modify the generator so 
# that it reports factors in increasing order, while maintaining its general performance advantages
def factors(n):
	return [i for i in range(1, n+1) if not n%i]
print(factors(12))


# C - 1.28: The p-norm of a vector v = (v1, v2, ....,Vn) in n-dimensional space is defined as ||v|| = P(sqrt(VP1 + VP2 + .... + VPn))  For the special 
# case of p = 2, this results in the traditional Euclidean norm, which represents the length of the vector.  For example, the Euclidean norm of a
# two-dimensional vector with coordinates (4, 3) has a Euclidean norm of sqrt(4^2 + ^2) = sqrt(16 + 9) = sqrt(25) = 5.  Give an implementation of a
# function named norm such that norm(v, p) returns the p-norm value of v and norm(v) returns the Euclidean norm of v.  You may assume that
# v is a list of number 

def norm(n, p=2):
	temp = sum(i**p for i in n)
	return temp ** (1/p)


# P - 1.36: Write a Python program that inputs a list of words, separated by whitespace, and outputs how many times each word appears in the
# list.  You need not worry about efficiency at this point, however, as this topic is something that will be addressed later in this book.
input_file=open("earthquakes.txt","r")
wordcount={}
for word in input_file.read().split():
    if word not in wordcount:
        wordcount[word] = 1
    else:
        wordcount[word] += 1
input_file.close()
out_file = open("earthquakesOut.txt","w") # w modifier erases all current file content
for k,v in wordcount.items():
    out_file.write(" " +k+ " " )
    out_file.write(str(v))
out_file.close()