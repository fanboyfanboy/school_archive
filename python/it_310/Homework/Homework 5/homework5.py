# Dalton Cothron
# Homework 5


# R-5.2
import sys

data = []
idx = 0
for k in range(30):
	a = len(data)
	b = sys.getsizeof(data)
	if b > idx and k > 0:
		idx = b
		print("Exhausted at: {0}".format(a-1))
	data.append(None)




import ctypes

class DynamicArray(object):

	def __init__(self):
		self._n = 0
		self._capacity = 1
		self._A = self._make_array(self._capacity)

	def _make_array(self, c):
		return (c * ctypes.py_object)()
		# creates an "array" of pointers
		# http://docs.python.org/3/c-api/structures.html#c.PyObject

	def __len__(self):
		return self._n

# ################### UPDATED METOD: R-5.4 ######################
	def __getitem__(self, item):
		if  item < 0:
			item += self._n
		if not 0 <= item < self._n:
			raise IndexError('invalid index')
		return self._A[item]

	# GROW THE ARRAY DYNAMICALLY
	def _resize(self, c):    # c should be larger that the original array
		B = self._make_array(c)
		for k in range(self._n):
			B[k] = self._A[k]
			self._A = B
			self._capacity = c

	def append(self, obj):
		if self._n == self._capacity:    # list is full
			self._resize(2 * self._capacity)   # grow the "list" by twice its size
			self._A[self._n] = obj
			self._n += 1

	# Insert... see mutating
	# assume 0 <= k < n
######################## UPDATED METHOD: R-5.6 ###################
	def insert(self, k, value):
		if self._n == self._capacity:
			dblCapacity = self._make_array(2 * self._capacity)
			for i in range(self._n, k, -1):
				dblCapacity[i] = self._A[i-1]

			dblCapacity[k] = value
			for j in range(0, k):
				dblCapacity[j] = self._A[j]
			self._A  = dblCapacity
			self._n +=1
			self._capacity = 2 * self._capacity
		else:
			for x in range(self._n, k, 1):
				self._A[x] = self._A[x-1]

			self._A[k] = value
			self._n +=1

	# Pop the last element of the array
######################## UPDATED METHOD: C-5.16 ####################
	def pop(self):
		self._n -=1
		if self._n <= 0:
			raise IndexError("Found index error!")
		if self._n < self._capacity / 4:
			self._capacity = self._capacity // 2
		return self._A[self._n-1]


# R - 5.12
from pprint import pprint
data = [[i for i in range(15)] for j in range(15)]
pprint(data)

print("Sum: ", sum(sum(x) for x in data))