# An iterator manages iteration. The built-in function next(i)
# produces the subsequent element from the underlying series.

# An iterable is an object tha produces an iterator via the
# syntax iter(obj)

# Examples
x = [4, 10, 14, 8, -3]  # is an iterable
i = iter(x)             # is an iterator

# It is legal to do the following:
for j in range(len(x)):
    print(next(i))

# Generator syntax is used to create iterators.
# It relies on function syntax and a yield statemnt instead of
# a return statement
# CANNOT COMBINE YIELD AND RETURN
# Example:  Factors of n
def factors_(n):
    results = []
    for i in range(1, n+1):
        if not n % i:
            results.append(i)

    return results

# Generator version
def factors(n):
    for i in range(1, n+1):
        if not n%i:
            yield i

facts = factors_(10)
print(facts)   # a list of factors

factsG = factors(10)
print(factsG)  # a generator object
print(next(factsG))
print(next(factsG))

# For loops take iterable. A generator is converted to an iterable by the intepreter:
for i in factsG:
    print("From Generator: " + str(i))

# Generatos as comprehensions

# first 100 even numbers
x = (x for x in range(1, 201) if not x % 2)
print(x)
for i in x:
    print(i, end=" ")  # prints everything in single line

print()

# Three digit numbers divisible by 13
x = (x for x in range(100, 1000) if not x % 13)
for i in x:
    print(i, end=" ")

# We can improve efficiency of factorization as follows:
# It suffices to check factors up to sqrt(n)
print()

def factors(n):
    k = 1
    while k * k < n:    # checking sqrt(n)
        if not n % k:
            yield k     # generate the factor k
            yield n//k  # since k is a factor of n, so is n//k
                        # this factor is larger than sqrt(n)
        k += 1
    if k * k == n:      # case where n is a perfect square
        yield k

facts = factors(124)
print(next(facts))
print(facts.__next__()) # same call as before but explicitly
print(next(facts))
print(list(factors(124)))












