class Vector(object):
    """Represent a vector in a multidimensional space"""

    def __init__(self, d):
        """Create d-dimensional vector of zeroes."""
        self._coords = [0] * d

    def __len__(self):
        """Return the dimension of the vector."""
        return len(self._coords)

    def __getitem__(self, j):
        """Return jth coordinate of vector."""
        try:
            return self._coords[j]
        except IndexError:
            print("The index value {0} exceeds the vector's dimension {1}" \
                  .format(j, len(self)))

    def __setitem__(self, j, value):
        """Set the jth coordinate of the vector to a given value."""
        try:
            self._coords[j] = value
        except IndexError:
            print("The index value {0} exceeds the vector's dimension {1}" \
                  .format(j, len(self)))

    # __repr__ goal is to be unanbiguous -- formal string representation
    # __str__ goal is to be readable -- informal string represtation
    # if __repr__ is defined, and __str__ is not, the object will behave as though
    #        __str__ is the same as __repr__
    # __str__ is optional... called the "pretty print" functionality

    def __repr__(self):
        """Produce the string representation of vector."""
        return '<' + str(self._coords)[1:-1] + '>'

    def __add__(self, v):
        """Return the sum of two vectors"""
        if len(self) != len(v):
            raise ValueError('Dimensions must be equal. Currently {0}!={1}'\
                             .format(len(self), len(v)))
        result = Vector(len(self))
        for i in range(len(self)):
            result[i] = float('{0:.1f}'.format(self[i] + v[i]))

        return result

# TESTING
if __name__ == '__main__':   # will execute when Python is invoked on the module, not when imported
    # instantiate a vector
    from random import random
    n = 4
    vector1 = Vector(n)
    for i in range(n):
        vector1[i] = float('{0:.1f}'.format(random()))

    print(vector1)

    # force an error
    vector1[5] = 3

    # test vector addition
    vector2 = Vector(n)
    for i in range(n):
        vector2[i] = float('{0:.1f}'.format(random()))

    print(vector1 + vector2)

    # force an error with vectors of distinct dimension
    vector3 = Vector(2*n)
    print(vector3)
    vector3._coords = vector1._coords * 3
    print(vector3)
    print(vector1 + vector3)




