from time import time
import matplotlib.pyplot as plt

def factors1(n):
    return [i for i in range(1, n+1) if not n % i]

def factors2(n):
    factors = []
    k = 1
    while k * k < n:
        if not n % k:
            factors.append(k)
            factors.append(n//k)
        k += 1
    if k * k == n:
        factors.append(k)
    return factors

# Experiment: Run 100 integers through each function and plot the
# running time results
x = [i for i in range(100, 600, 5)]

times1 = []
times2 = []
for j in x:
    # Testing factors1
    start_time = time()
    factors1(j)
    end_time = time()
    time1 = end_time - start_time

    # Testing factors2
    start_time = time()
    factors2(j)
    end_time = time()
    time2 = end_time - start_time

    # Add running times to the appropriate lists
    times1.append(time1)
    times2.append(time2)

print(times1)
print(times2)


# Plot times vs input size
plt.scatter(x, times1, color="red")
plt.scatter(x, times2, color="blue")
plt.ylabel("Running Time" + r"$(ms)$")
plt.xlabel("Input Size" + r"$(n) \tau$")
plt.ylim(-.0001, .0001)
plt.show()













