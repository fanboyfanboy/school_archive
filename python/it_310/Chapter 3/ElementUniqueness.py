# Element Uniqueness
# Determine if all elements in a collection are distinct

def unique1(S):
    for i in range(len(S)):
        for j in range(i+1, len(S)):
            if S[i] == S[j]:
                return False
    return True


# Analysis
# Looping through all distinct pairs of elements of the collection
# and comparing each element takes O(n**2)
# Each iteration of the outer loop causes n-1 iteration of the inner loop
# Thus, n-2 + n-3 + ... + 1 which is O(n**2)

def unique2(S):
    temp = sorted(S)
    for i in range(1, len(temp)):
        if temp[i-1] == temp[i]:
            return False
    return True

# Analysis
# By sorting in the first step we reduce the time to check consecutive
# pairs of elements rather than all possible pairs
# The complexity of sorted() is O(nlogn)
# The for loop runs in O(n) times
# Thus the time complexity is O(nlogn)

















