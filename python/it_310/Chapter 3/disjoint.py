# Three-way Set Disjointness
# Determine if A intersection B intersection C is empty


def disjoint1(A, B, C):
	for i in A:
		for j in B:
			for z in C:
				if i == j == z:
					return False
	return True

# Analysis
# If each set has n elements, each for loop executes n times.
# Since loops are nested, the second loop contributes 1, 2, ...., n 
# steps at each n step of the first loop
# Thus O(n**2)
# The third loop contributes 1, 2, ...., n steps for each of n**2 steps before
# Thus O(n**3)



# Observation, IF a in A and b in B and a!=b, why on earth would I 
# bother to check if a or b are in C?

def disjoint2(A, B, C):
	for a in A:
		for b in B:
			if a == b:
				for c in C:
					if a == c: 
						return False
	return True


# Analysis
# If A and B are different, then the if statement will prevent
# the execution of the inner most loop
# In the worst-case scenario, consider A and B having the same elements
# Thus, there are at most O(n) paris where a == b
# The inner loop for C will then eexecute at most n times.
# Thus, the total execution time will be bound by O(n**2) + O(n)
# Thus, O(n**2)


