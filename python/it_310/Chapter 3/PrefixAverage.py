# Prefix Average
# for S = 1,2,3,...,n  (A[j] = S[0] + ... + S[j]) / j + 1

def prefix_average1(S):
    n = len(S)          # get size of S
    A = [0] * n         # create a new list as large as S
    for j in range(n):
        total = 0
        for i in range(j + 1):
            total += S[i]    # accumulates the total prefix
        A[j] = total / (j + 1)  # calculates average

    return A

# Analysis:
# Line 5: O(1)
# Line 6: O(n) since list grows one step at a time...
# think of elements being appended every time for each n
# Lines 7-9: Nested loops contribute O(n**2)
# since inner loop adds an operation every step as in
# 1 + 2 + ... + n = n(n+1) / 2
# Thus O(n**2)


def prefix_average2(S):
    n = len(S)
    A = [0] * n
    for j in range(n):
        A[j] = sum(S[:j+1]) / (j+1)
    return A

# Analysis:
# Replaced inner loop with sum(S[:j+1])
# The evaluation of the sum() takes O(j+1) steps for each j
# thus we still get 1 + 2 + 3 +... + n steps added for each j
# Still O(n**2)

def prefix_average3(S):
    n = len(S)
    A = [0] * n
    total = 0
    for j in range(n):
        total += S[j]
        A[j] = total / (j+1)
    return A

# Analysis:
# For loop executes n times
# Each of lines 43 and 44 is executed once per iteration
# thus O(1) each of n times
# Thus O(n) is the total running time






















