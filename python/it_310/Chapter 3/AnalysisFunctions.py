# Plot all functions to describe time complexity:
# 1, logn, n, nlogn, n**2, n**3, 2**n
import numpy as np
import matplotlib.pyplot as plt

n = 100
n_ = [i for i in range(2, n)]

# Constant function
x = [1 for i in range(2, n)]
plt.plot(n_, x)

# Log base 2
log2 = [np.log2(i) for i in range(2, n)]
plt.plot(n_, log2)

# linear
linear = [i for i in range(2, n)]
plt.plot(n_, linear)

# n-log-n
nlogn = [i * np.log2(i) for i in range(2,n)]
plt.plot(n_, nlogn)

# quadratic
nsquare = [i**2 for i in range(2, n)]
plt.plot(n_, nsquare)

# cubic
ncube = [i**3 for i in range(2, n)]
plt.plot(n_, ncube)

# exponential base 2
exponential = [2**i for i in range(2, n)]
plt.plot(n_, exponential)

plt.legend(['y=1', 'y=log(n)', 'y=x', \
            'y=nlog(n)', 'y=' + r'$n^{2}$', \
            'y=' + r'$n^{3}$', 'y=' + r'$2^{n}$'], \
           loc='upper left')

plt.yscale('log')
plt.xscale('log')

#plt.show()


# Ceiling of log n base b

def log_ceiling(n, b):
    n_ = n
    counter = 0
    while True:
        x = n_/b
        counter += 1
        if x <= 1:
            break
        else:
            n_ = x
    return counter

print(log_ceiling(81, 6))
print(log_ceiling(126, 7))
print(log_ceiling(12, 2))
print(log_ceiling(27, 3))

























