# Abstract Data Type : Double-Ended Queue - pronounced "deck"
# Deque D Supports:
# D.add_first(e): add element e to the front of D
# D.add_last(e): add element e to the back of D
# D.delete_first(e): remove and return a reference to the first element of D
# D.delete_last(e): remove and return a reference to the last element of D
# D.first(): return a reference to the first element of D
# D.last(): return a reference to the last element of D
# D.is_empty(): return true if D has no elements
# len(D): number of elements in D

class Empty(Exception):
    pass


