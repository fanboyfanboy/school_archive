# Homework 6
# Dalton Cothron




####################################################
# Imports 
####################################################

from collections import deque
from Stacks import ArrayStack, Empty
from Queues import ArrayQueue




####################################################
# R - 6.2
####################################################




####################################################
# R - 6.3
####################################################

# Params
def transfer(S, T):

	# create Stack S
	while(len(S)>0):
		# Append Stack T onto Stack S
		T.push(S.pop());


if __name__ == '__main__':

	# Create stacks
	S = ArrayStack();
	T = ArrayStack();

	# Create test length
	for i in range(10):
		S.push(i);
	
	# Test
	transfer(S, T);

	# Append
	for i in range(10):
		print(T.pop());


####################################################
# R -6.5
####################################################


def reverse(L):

	# Create stack S
	S = ArrayStack();

	#Add Each element in L to Stack S
	for i in range(len(L)):
		S.push(L[i])

	# Add each element in L to bottom of stack S
	for i in range(len(L)):
		L[i] = S.pop()

if __name__ == '__main__':
	

	# Create L length
	L = [i for i in range(10)]
	
	# print
	print("\n");
	print(L);
	print("\n");
	
	# reverse L
	reverse(L)
	

	# Print
	print("\n");
	print('reversed L:\n', L)
	print("\n");

####################################################
# R-6.13
####################################################
if __name__ == '__main__':
	

	# Create D, Q  Stack length ([1,...,8])
	D = ArrayQueue(9);
	Q = ArrayQueue(9);
	
	#initial D
	for i in range(1, 9):
		D.enqueue(i);

	D.enqueue(D.dequeue());
	D.enqueue(D.dequeue());
	D.enqueue(D.dequeue());  # D = [4,5,6,7,8,1,2,3]

	Q.enqueue(D.dequeue());  # Q = [4], D = [5,6,7,8,1,2,3]
	D.enqueue(D.dequeue());  # Q = [4], D = [6,7,8,1,2,3,5]
	D.enqueue(Q.dequeue());  # Q = [], D = [6,7,8,1,2,3,5,4]

	D.enqueue(D.dequeue());
	D.enqueue(D.dequeue());
	D.enqueue(D.dequeue());  # D = [1,2,3,5,4,6,7,8]

	# Print D
	for i in range(8):
		print(D.dequeue(), end=',');
	print("\n");



####################################################
# R-6.14
####################################################

if __name__ == '__main__':

	# Create D & S stacks
	D = deque();
	S = deque();
	# extend D 
	D.extend(i for i in range(1, 9));
	print(D);

	# Appand Left D Element to bottom of S Stack
	S.append(D.popleft());
	S.append(D.popleft());
	S.append(D.popleft());
	S.append(D.popleft());
	D.append(D.popleft());
	D.appendleft(S.pop());
	D.appendleft(D.pop());
	D.appendleft(S.pop());
	D.appendleft(S.pop());
	D.appendleft(S.pop());

	print(D);


####################################################
# C-6.15
####################################################

# X last element in S
#	X = S.pop()

# X greater than last lement in S
#	X > S.pop() ? x : S.pop()


####################################################
# C-6.18
####################################################




####################################################
# C-6.29
####################################################