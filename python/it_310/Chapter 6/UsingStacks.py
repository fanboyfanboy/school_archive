from Stacks import ArrayStack

##############################
# Reversing Data using a Stack

def reverse_file(filename, extension):
    S = ArrayStack()
    in_file = open(filename + "." + extension, "r")
    for line in in_file:
        S.push(line.rstrip('\n'))       # copy of the string with trailing characters removed
    in_file.close()

    out_file = open(filename + "_reverse." + extension, "w")

    while not S.is_empty():
        out_file.write(S.pop() + "\n")  # re-inserting the newline character

    out_file.close()


#reverse_file("poem", "txt")

##############################
# Matching delimiters
# Parentheses ()
# Braces {}
# Brackets []

def is_matched(expression):
    """Checks delimiter matching on arithmetic expressions"""
    opening = "([{"
    closing = ")]}"
    S = ArrayStack()
    for e in expression:
        if e in opening:
            S.push(e)
            #print(S.top())
        elif e in closing:
            if S.is_empty():
                return False
            if closing.index(e) != opening.index(S.pop()):
                return False
    return S.is_empty()

#exp = "((2*3)*(5/(3+5))-7)+8))"
#exp = "()"
#print(is_matched(exp))

##############################
# Matching Tags in Markup Language: HTML, XML, etc.

def html_tag_match(filename, extension="html"):
    file = open(filename + "." + extension)
    raw = "".join(file.readlines())
    file.close()

    S = ArrayStack()
    j = raw.find("<")        # see http://docs.python.org/3/library/stdtypes.html
    while j != -1:
        k = raw.find(">", j+1)  # return lowest index of string raw[j+1:]
        if k == -1:
            return False
        tag = raw[j+1:k]     # removes <>
        if not tag.startswith("/"):
            S.push(tag)
        else:
            if S.is_empty():
                return False
            if tag[1:] != S.pop():
                return False
        j = raw.find("<", k+1)
    return S.is_empty()


#print(html_tag_match("sample"))
print(html_tag_match("sample2"))






















