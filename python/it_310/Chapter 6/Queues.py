# Abstract Data Type : Queue
# Queue Q supports
# Q.enqueue(e): add element e to the back of the queue Q
# Q.dequeue(e): remove and return the first element of the queue Q
# Q.first(): return a reference to the first element of the queue Q
# Q.is_empty(): return true if Q does not contain any elements
# len(Q): return number of elements in Q

# We implement circular lists using mod(len(Q)) -> using % operator

class Empty(Exception):
    pass

class ArrayQueue:
    """Circular FIFO Queue"""
    def __init__(self, capacity):
        self._data = [None] * capacity
        self._size = 0                 # number of elements (NOT size of list)
        self._front = 0                # index of the first element in queue

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def first(self):
        if self.is_empty():
            raise Empty("Queue is empty")
        return self._data[self._front]

    def dequeue(self):
        if self.is_empty():
            raise Empty("Queue is empty")
        element_to_dequeue = self._data[self._front]
        self._data[self._front] = None
        self._front = (self._front + 1) % len(self._data)  # implementing circular behavior
        self._size -= 1
        # if size of queue is less that 1/4 capacity reduce the queue size to half
        if 0 < self._size < len(self._data) // 4:
            self._resize(len(self._data)//2)
        return element_to_dequeue

    def _resize(self, capacity):
        temp = self._data
        self._data = [None] * capacity
        step = self._front
        for k in range(self._size):
            self._data[k] = temp[step]
            step = (step + 1) % len(temp)
        self._front = 0

    def enqueue(self, e):
        if self._size == len(self._data):
            self._resize(2*len(self._data))
        idx_to_enqueue = (self._front + self._size) % len(self._data)
        self._data[idx_to_enqueue] = e
        self._size += 1

# Analysis
# enqueue(), dequeue() -> O(1) amortized
# first(), is_empty(), len() -> O(1)

Q = ArrayQueue(10)
Q.enqueue(6)
Q.enqueue(7)
Q.enqueue(8)
Q.enqueue(9)
Q.enqueue(10)
Q.dequeue()
Q.dequeue()
print(Q.first())
Q.enqueue(11)
Q.enqueue(12)
Q.enqueue(13)
Q.enqueue(14)
Q.enqueue(15)
# next element should be added to the beginning of the underlying list
Q.enqueue(16)
print(len(Q._data))
print(Q._data.index(16))
Q.enqueue(17)
# the next element should force the queue to double its capacity
Q.enqueue(18)
print(len(Q._data))   # should be 20
print(Q._data[10])    # should be 18
print(Q._data[Q._front])

