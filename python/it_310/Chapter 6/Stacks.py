# Abstract Data Type : Stack
# Stack S supports
# S.push(e): add element e to the top of the stack S
# S.pop(e): remove an element from the top of the stack S and return it
# S.top(): returns a reference to the top element of the stack S
# S.is_empty(): returns true if S does not contain any elements
# len(S): number of elements in stack S

# We use Python list to implement a stack
# since lists implement append(), pop() and are mutable

# The Adapter Design Pattern: create a new class repackaging existing
# functionality from another class

# Stack -> List
# push() -> append()
# pop() -> pop()
# top() -> L[-1]
# is_empty() -> len(L) == 0 or L == []
# len(S) -> len(L)

class Empty(Exception):
    """Simple Exception extension"""
    pass

class ArrayStack:
    """LIFO Stack"""
    def __init__(self):
        self._data = []            # meant as a non-public instance

    def __len__(self):             # allows the natural use of len()
        return len(self._data)

    def is_empty(self):
        return self._data == []

    def push(self, e):
        self._data.append(e)       # push/"append" to top of the stack

    def top(self):
        if self.is_empty():
            raise Empty("Stack is empty")

        return self._data[-1]

    def pop(self):
        if self.is_empty():
            raise Empty("Stack is empty")

        return self._data.pop()


# Analysis: based on running times of list methods (see table 5.4)
# push() and pop() -> O(1) amortized (mutable)
# top(), is_empty(), len() -> O(1)     (immutable)

if __name__ == "__main__":
    # Testing ArrayStack methods
    S = ArrayStack()
    S.push("dog")
    S.push(3)
    print(len(S))
    print(S.pop())
    print(S.is_empty())
    print(S.pop())
    print(S.is_empty())
    #S.pop()
    S.push("Hello World")
    print(S.top())





















