# Comprehenison syntax

# Lists: [expression for value in iterable if condition]

# squares of numbers from 1 to n
n = 100
squares = [x**2 for x in range(n)]
print(squares)

# first 100 even numbers
evens_100 = [x for x in range(1, 201) if not x%2]
print(evens_100)


# random 3x3 matrix of integers less than 20
import random
matrix_3_3 = [[random.randrange(20) for i in range(3)] for j in range(3)]
print(matrix_3_3)

# factors of n
n = 12
factors = [i for i in range(1, n+1) if not n%i]
print(factors)

# Similarly set and dictionary comprehensions can be instantiated

# dictionary of integers:square_root for numbers from 1 to n (2 decimal points)
n = 25
square_roots = {x:float("{0:.2f}".format(x**(1/2))) for x in range(n+1)}
print(square_roots)

