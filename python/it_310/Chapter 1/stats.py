__author__ = 'ua4225pe'

#RANGE
def Range(list_):
    return max(list_) - min(list_)

#MEAN
def mean(list_):
    return sum(list_)/len(list_)


#MEDIAN
#if odd, take middle value
#if even, take the average of the two middel values

def median(list_):
    list_.sort()   # sorts the list in place

    if len(list_) % 2:   #odd case
        median_ = list_[len(list_)//2]
    else:  #even case
        median_ = mean([list_[len(list_)//2-1], list_[len(list_)//2]])

    return median_

#COMPUTING THE MODE USING DICTIONARIES

def mode(dataList):
    #create a dictionary to hold the data as key and the counts as values
    dictionaryWithCounts = dict()

    for item in dataList:
        if item not in dictionaryWithCounts: # key does not exist
            dictionaryWithCounts[item] = 1
        else:  #key exists so add 1 to the counter
            dictionaryWithCounts[item] += 1

    #find the max count in the dictionary
    maxValue = max(dictionaryWithCounts.values())  #the maximum counter

    #create a list to store all the modes found
    modeList = []

    #iterate through the dictionary to find the modes and add them to modeList
    for item in dictionaryWithCounts:
        if dictionaryWithCounts[item] == maxValue:
            modeList.append(item)

    return modeList


#Standard Deviation
def stDev(list_):
    #Calculate the mean of the list
    Mean = mean(list_)

    #Setup an accumulator fo the terms of the series
    sumOfValues = 0

    #NUMERATOR
    #For each data value in the collection (list_)
    #Substract the mean, square the result and add it to the sumOfValues

    for value in list_:
        sumOfValues += (value - Mean)**2

    #Divide the sumOfValues by n-1, where n is the number of data values (ie, len(list_) )
    # then take the square root
    # return the result
    n = len(list_)
    return (sumOfValues/(n-1))**(1/2)