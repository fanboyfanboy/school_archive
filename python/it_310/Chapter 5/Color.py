class Color(object):
    """Defines a color object as a triple of numbers from 0-1"""
    def __init__(self, r=0, g=0, b=0):
        self.red = r
        self.blue = b
        self.green = g
        self.check_colors()

    def __repr__(self):
        return "(Red:%.2f; Blue:%.2f; Green:%.2f)" % (self.red, self.blue, self.green)

    def check_colors(self):
        self.red = 1 if self.red > 1 else self.red
        self.blue = 1 if self.blue > 1 else self.blue
        self.green = 1 if self.green > 1 else self.green


if __name__ == "__main__":
    color0 = Color()
    print(color0.red)
    print(color0)

    color1 = Color(3.1, 1.9, 0.4)
    print(color1)

    color2 = Color(0.3, 0.4, 0.9)
    print(color2)



