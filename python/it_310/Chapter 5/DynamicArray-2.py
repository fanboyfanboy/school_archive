import ctypes


class DynamicArray(object):

    def __init__(self):
        self._n = 0
        self._capacity = 1
        self._A = self._make_array(self._capacity)

    def _make_array(self, c):
        return (c * ctypes.py_object)()
        # creates an "array" of pointers
        # http://docs.python.org/3/c-api/structures.html#c.PyObject

    def __len__(self):
        return self._n

    def __getitem__(self, item):
        if not 0 <= item < self._n:
            raise IndexError('invalid index')
        return self._A[item]

    # GROW THE ARRAY DYNAMICALLY
    def _resize(self, c):    # c should be larger that the original array
        B = self._make_array(c)
        for k in range(self._n):
            B[k] = self._A[k]
        self._A = B
        self._capacity = c

    def append(self, obj):
        if self._n == self._capacity:    # list is full
            self._resize(2 * self._capacity)   # grow the "list" by twice its size
        self._A[self._n] = obj
        self._n += 1

    # Insert... see mutating
    # assume 0 <= k < n
    def insert(self, k, value):
        if self._n == self._capacity:       # out of empty cell, must make room
            self._resize(2*self._capacity)  # double capacity
        for j in range(self._n, k, -1):     # shift rightmost first; -1 iterates through the range backwards
            self._A[j] = self._A[j-1]
        self._A[k] = value
        self._n += 1


    # Pop the last element of the array
    def pop(self):
        # do not shrink the array; only allows removal of last element
        if self._n > 1:
            self._A[self._n - 1] = None  # help garbage collection
            self._n -= 1  # one less element
            return















