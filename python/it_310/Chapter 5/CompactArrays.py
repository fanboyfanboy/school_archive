# Compact Arrays
# Store bits that represent the primary data; not references to the data

# For example Strings are compact arrays

# We can use compact arrays to store (integers) data as elements rather
# than as references
# Cannot create compact arrays from user defined data


# from array import array
#
# multiples_10 = [i for i in range(1, 100) if not i % 10]
# print(type(multiples_10))
#
#
# multiples_10 = array("i", multiples_10)   # using signed int representation of the number
#
# print(type(multiples_10))



# Lists are Dynamic Arrays
# they are initialized with more space than necessary
# lists grow as necessary by adding "chunks" of memory
# to add a "chunk" of memory, a new area in memory with the
# desired allocation is selected
# and a copy of the array is then stored in the new location
# the reference also needs to be updated


# let's explore how lists grow dynamically
import sys

data = []
n = 50
for k in range(n):
    a = len(data)
    b = sys.getsizeof(data)
    print("Length: {0:3d}; Size in bytes: {1:4d}".format(a, b))
    data.append(None)
































