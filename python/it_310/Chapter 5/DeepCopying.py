import Color


# List of colors
orange = Color.Color(249/255, 124/255, 43/255)
lime = Color.Color(87/255, 249/255, 43/255)
blue = Color.Color(43/255, 46/255, 249/255)
warmtones = [orange, lime, blue]

print("warmtones" + str(warmtones))


# Create a "copy"
#palette = warmtones


# Try removing an element from palette
#palette.pop(1)
#print(warmtones)
# warmtones is affected
# This is because both warmtones and palette are references to the same object
# in memory


# # Try to create a new list a different way
# palette = list(warmtones)    # similar to palette = warmtones[:]
# print("palette" + str(palette))
# palette.pop(1)
# print("warmtones" + str(warmtones))
# print("palette" + str(palette))
# palette[0].red = 0
# print("warmtones" + str(warmtones))
# Weird behavior since we just have copies to references in memory
# not copies of actual "data"
# this is called a shallow copy.




# Let us create a real copy of warmtones
# Deep copy
import copy


palette = copy.deepcopy(warmtones)
palette[0].red = 0
print("warmtones" + str(warmtones))
print("palette" + str(palette))
























