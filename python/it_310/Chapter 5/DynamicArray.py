import ctypes

class dynamicArray(object):

	def __init__(self):
		self._n = 0
		self._capacity = 1
		self._A = self._mak_array(self._capacity)

	def makeArray(self, c):
		return (c* ctypes.py_object)()	# returns a function - Docs: https://docs.python.org/3/c-api/structures.html#c.PyObject
						# Creates an Array of Pointers
