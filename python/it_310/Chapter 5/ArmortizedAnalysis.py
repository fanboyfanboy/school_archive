#testing efficiency of __contains__
from time import time

data = list(range(10000000))
start = time()
5 in data
print ("An element in a left most position: " + str(time() - start))
start = time()
999995 in data
print("An element in  a right most positiion: " + str(time() - start))


# Test slicing

start = time()
data[6000000:600008]
print("A small slice: " + str(time() - start))

start = time()
data[600000:9999999]
print("A large slice: " + str(time() - start))


multiples_10 = [10xi for i in range(10, 100)]

for i in multiples_10:
	start = time()
	data[:i]
	print(str(time() - start))
