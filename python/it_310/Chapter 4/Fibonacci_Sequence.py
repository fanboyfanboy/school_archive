# Fibonacci sequence
# F0 = 1
# F1 = 1
# Fn = Fn-1 + Fn-2

def fibonacci1(n):
	if n <= 1:
		return 1
	else:
		return fibonacci1(n-1) + fibonacci1(n-2)

# Analysis
# This is the most natural way of defining this recursion
# (See the execution tree)
# There is a problem of claling the function to do tasks that have already been done
# By the claim (see your notes) there are more than 2**n steps
# That is fibonacci1(n) > 2**(n/2)
# The memory consumption is also inefficient 




# Better Fibonacci sequence implementation
def fibonacci2(n):
	if n<= 1:
		return n, 0	# With the understanding that the sequence is indexed starting at 1

	else:
		a, b = fibonacci2(n-1)
		return a+b, a

# print(fibonacci2(11))
# print(fibonacci1(11))

# Analysis
# each recursive call reduces n by 1. thus n recursive calls are made.
# each recursive call uses constant time (the only operations are an assignment and a sum)
# Thus there are n times a constant number of operations 
# That is, O(n)




# Python's recursive depth
import sys
# sys.setrecursionlimit(10000)

def function(n, level=0):
	if n < -1250:
		return 0
	else:
		print("Level " + str(level))
		level +=1
		return function(n-1, level)

function(0)