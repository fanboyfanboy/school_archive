# Element Uniqueness
# Determine if all elements of a collection are distinct 

def unique(S, start, stop):
	if stop - start <= 1:
		return True
	elif not unique(S, start, stop -1):
		return False
	elif not unique(S, start+1, stop):
		return False
	else:
		return S[start] != S[stop-1]	# Notice start and stop are indexes

s_list = [2, 4, 7, 8, 5, 4, 3, 5, 7, 8, 9, 0, 8, 7, 6, 4]
print(unique(s_list, 0, len(s_list)-1))

# Analysis
# The problem is that for a list of S of size n, there are two recursive calls of size n-1
# If n = 1, we make a single call.
# For n>1, the first call makes 2 calls, then each call makes 2 calls that is 4 calls
# then each call makes 2 calls, that is 8 calls.
# The pattern is 1+2+4+8+....+2**(n-1)
# this is O(2**n) ... Bad! 