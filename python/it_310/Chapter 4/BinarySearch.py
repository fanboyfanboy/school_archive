# Binary Search
# Find an element in a sorted list

def binary_search(data, y, low, high):
	if low > high:
		return False
	else:
		mid = (low + high) // 2 		# Middle index uses
		if v = data[mid]: 		# found the value in the middle
			return True
		elif v < data[mid]:		# value is possibly in the lower half
			return binary_search(data, v, low, mid-1)
		else:
			return binary_search(data, v, mid+1, high)

# Analysis 
# O(logn) ... see the binary search tree

