# File System
# Determine the cumulative disk space usage of a path in the os directory 

from os import path, listdir

def disk_usage(p):
	"""""Return number of bytes used by a directory and its sub-nodes"""""
	total = path.getsize(p) 		# immediate disk usage of p
	print('{0:<7} -- I'.format(total), p)

	if path.isdir(p):
		for sub_node in listdir(p):
			sub_path = path.join(p, sub_node)
			total += disk_usage(sub_path)
	print('{0:<7} -- C'.format(total), p)	 # explicit output of cumulative size of the current sub_node

	return total

# UNIT_TESTING
if __name__ == '__main__':
	_path = '/Users/Dalton/Documents/School/Junior Semester 2/IT310/Documents/Class'

	t = disk_usage(_path)
	print(t)

# Analysis:
# Each sub_node will be called once at each iteration of the for loop
# then we make exactly n recursive calls... provided there are n sub_nodes 
# but, how many operations does each recursive call contribute overall?
# The results of traversing a tree structure in chapter 8
# It turns out that the complexity is O(n).
# Each node is O(1)
# It turns out that the complexity is O(n).