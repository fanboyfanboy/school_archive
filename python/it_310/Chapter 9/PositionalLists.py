# Abstract Data Type : Positional List
# Underlying Data Structure is a Doubly Linked List
# A Positional List PL supports
# PL.is_empty()
# len(PL)

# Accessor methods
# methods to get information from the positional list
# iter(PL) : Return a forward iterator (use yield)
# PL.first(), PL.last() : Return position of first (last) element, or None if empty
# PL.before(p), PL.after(p) : Return the position immediately before (after) p, or None if p is first (last)

# Mutator methods
# methods to change information in the positional list
# PL.add_first(e), PL.add_last(e) : Insert new element in front (back) and return position
# PL.add_before(p, e), PL.add_after(p, e) : Insert new element e just before (after) p, return position
# L.delete(p) : Remove and return element at position p.  Invalidates position.
# L.replace(p, e) : Replace and return element replaced


from DoublyLinkedList import _DoublyLinkedBase as dlb

class PositionalList(dlb):
    # ----------- nested Position class --------------
    class Position:

        def __init__(self, container, node):
            self._container = container             # reference to linked list that contains node at this position
            self._node = node                       # reference to node pointed to by this position

        def element(self):
            return self._node._element

        def __eq__(self, other):
            """Return True if other is a Position representing the same location"""
            return type(other) is type(self) and other._node is self._node

        def __ne__(self, other):
            """Return True if other does not represent the same location"""
            return not (self == other)

    # ----- private position Vaditation method -------
    # possible errors: p is not a Position, p is not in the current list, p is no longer valid
    # purpose of method: return the node pointed to by p if p is a valid position
    # self in this scope represents a doubly linked list... why?
    def _validate(self, p):
        """Return position's node, or raise exception"""
        if not isinstance(p, self.Position):
            raise TypeError("p must be of type Position")
        # If the list that contains p is not in the current list raise ValueError
        if p._container is not self:                # self is a doubly linked list and p is a Position
            raise ValueError("p does not belong to this container")
        if p._node._next is None:                   # underlying Node has been invalidated
            raise ValueError("p is no longer valid")
        return p._node

    # ----- private method to create a position ------
    # self represents a doubly linked list
    def _make_position(self, node):
        """Return Position instance for given node, None if sentinel"""
        if node is self._header or node is self._trailer:
            return None
        else:
            return self.Position(self, node)
    # ------------------------------------------------

    # ---------------- accessors ---------------------
    # methods to get information from the positional list

    def first(self):
        """Return position of first element"""
        return self._make_position(self._header._next)

    def last(self):
        """Return position of last element"""
        return self._make_position(self._trailer._prev)

    def before(self, p):
        """Return the position immediately before p"""
        node = self._validate(p)
        return self._make_position(node._prev)

    def after(self, p):
        """"Return the position immediately after p"""
        node = self._validate(p)
        return self._make_position(node._next)

    def __iter__(self):
        """Forward iterator of list elements."""
        #  Allows the use of next().
        #  Allows embed in for loops.
        pointer = self.first()
        while pointer is not None:
            yield pointer.element()         # return element stored at this position
            pointer = self.after(pointer)   # update the pointer

    # ---------------- mutators ----------------------
    # methods to change information in the positional list

    # Private method to return position rather than node
    # Overrides _insert_between() from parent _DoublyLinkedList class
    def _insert_between(self, e, predecessor, successor):
        node = super()._insert_between(e, predecessor, successor)
        return self._make_position(node)

    def add_first(self, e):
        """Insert new element in front and return position"""
        return self._insert_between(e, self._header, self._header._next)  # returns position thanks to overridden method

    def add_last(self, e):
        """Insert new element in back and return position"""
        return self._insert_between(e, self._trailer._prev, self._trailer)

    def add_before(self, p, e):
        """Insert new element e just before p, return position"""
        valid_position = self._validate(p)   # recall _validate returns the node to which p points
        return self._insert_between(e, valid_position._prev, valid_position)

    def add_after(self, p, e):
        """Insert new element e just after p, return position"""
        valid_position = self._validate(p)
        return self._insert_between(e, valid_position, valid_position._next)

    def delete(self, p):
        """Remove and return element at position p.  Invalidates position."""
        # Invalidation takes place due to the inherited method setting underlying node to None
        valid_position = self._validate(p)
        return self._delete_node(valid_position)

    def replace(self, p, e):
        """Replace and return element replaced"""
        valid_position = self._validate(p)
        element_to_return = valid_position._element
        valid_position._element = e
        return element_to_return


# Insertion Sort using Positional List
# Uses m marker for right most element of sorted list
# p marker for next element to sort
# w marker to traverse list from right to left

# def insertion_sort(PL):
#     """Sort Positional List of comparable elements in a non-decreasing order"""
#     if len(PL) > 1:             # at least two elements
#         m = PL.first()
#         w = m
#         while m != PL.last():
#             p = PL.after(m)
#             if p.element() >= m.element():
#                 w = m = p
#                 continue
#
#             # move w to the left
#             w = PL.before(w)
#             if w is None:    # special case when the first element is larger
#                 # delete p ... store element temporarily
#                 temp = PL.delete(p)
#                 # place p at beginnig
#                 PL.add_first(temp)
#                 # reset w
#                 w = m
#             elif p.element() > w.element():
#                 # delete p ... stohre element temporarily
#                 temp = PL.delete(p)
#                 # place p after w
#                 PL.add_after(w, temp)
#                 # reset w
#                 w = m


def insertion_sort(PL):
    """Sort Positional List of comparable elements in a non-decreasing order"""
    if len(PL) > 1:             # at least two elements
        m = PL.first()
        w = m
        while m != PL.last():
            p = PL.after(m)
            # CASE: element is correctly sorted
            if p.element() >= m.element():
                w = m = p
                continue

            # CASE: w reached the beginning of list
            if w == PL.first():
                # delete p and place p at beginning
                PL.add_first(PL.delete(p))
                continue

            w = PL.before(w)                # move w to the left
            if p.element() > w.element():
                # delete p and place p after w
                PL.add_after(w, PL.delete(p))
                # reset w
                w = m

if __name__ == '__main__':
    pl = PositionalList()

    # Add A, B, C
    pA = pl.add_first("A")
    pC = pl.add_after(pA, "C")
    pB = pl.add_before(pC, "B")

    # iterate through list
    for e in pl:
        print(e)

    print("\nFIRST")
    # check first(), last()
    pFirst = pl.first()
    print(pFirst == pA)
    print(pA.element())

    print("\nLAST")
    pLast = pl.last()
    print(pLast == pC)
    print(pC.element())
    print()

    # check delete and replace
    pD = pl.add_last("F")
    for e in pl:
        print(e)

    print("\nREPLACE")
    element = pl.replace(pD, "D")
    print("REPLACED", element)
    for e in pl:
        print(e)

    print("\nDELETE")
    element = pl.delete(pA)
    print("DELETED", element)
    for e in pl:
        print(e)


    # Testing Insertion Sort
    pl2 = PositionalList()
    pl2.add_first(1)
    pl2.add_first(2)
    pl2.add_first(3)
    pl2.add_first(5)
    pl2.add_first(5)
    pl2.add_first(7)
    pl2.add_first(10)

    print()
    for e in pl2:
        print(e)

    print("\nSORTED")
    insertion_sort2(pl2)  # mutates list
    for e in pl2:
        print(e)















