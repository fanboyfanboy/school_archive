# IT310 Bubble Sort Function
# Dalton Cothron

def bubbleSort(aList):
	for i in range(len(aList)-1, 0, -1):
		for j in range(i):
			if aList[j]>aList[j+1]:
				temp = aList[j]
				aList[j] = aList[j+1]
				aList[j+1] = temp

listOne = [12, 2, 1, 2, 10, 9, 15, 1];
bubbleSort(listOne);
print(listOne);