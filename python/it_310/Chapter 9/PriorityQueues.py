# Abstract Data Type : Priority Queue
# A Priority Queue PQ supports

# PQ.add(k, v) : Insert an item with key k and value v
# into the priority queue PQ

# PQ.min() : Return a tuple (k, v), representing the
# key and value of an item from PQ with minimum key;
# return error if PQ is empty

# PQ.remove_min() : Remove item with minimum key and return
# tuple (k, v); error if PQ is empty

# len(PQ) : Return number of items in PQ

from PositionalLists import PositionalList

class Empty(Exception):
    pass

class PriorityQueueBase:
    # --- Nested class to store priority queue items
    # --- in a compositional way
    class _Item:
        __slots__ = "_key", "_value"

        def __init__(self, key, value):
            self._key = key
            self._value = value

        def __lt__(self, other):
            return self._key < other._key

    def is_empty(self):
        return len(self) == 0


# Implementation of Priority Queue with Unsorted List
# Idea: Store key-value pair _Item instances in a PositionalList
# Since PositionalList will be implemented with a doubly-linked list
# all operations on doubly-linked list cost O(1)
# To remove a minimum element (or to find the minimum element), we must
# traverse the underlying list until we find the position of the item with the
# minimum key
# Thus, in the worst case scenario, we must check all elements witch means
# min() and remove_min() run O(n)

class UnsortedPriorityQueue(PriorityQueueBase):
    def _find_min(self):
        if self.is_empty():
            raise Empty('Priority queue is empty')

        small = self._data.first()
        walk = self._data.after(small)
        while walk is not None:
            if walk.element() < small.element():
                small = walk
            walk = self._data.after(walk)
        return small

    def __init__(self):
        self._data = PositionalList()

    def __len__(self):
        return len(self._data)

    def add(self, key, value):
        self._data.add_last(self._Item(key, value))

    def min(self):
        if self.is_empty():
            raise Empty('Priority queue is empty.')
        p = self._find_min()
        item = p.element()
        return (item._key, item._value)

    def remove_min(self):
        if self.is_empty():
            raise Empty('Priority queue is empty')
        p = self._find_min()
        item = self._data.delete(p)
        return (item._key, item._value)


# Implementation of Priority Queue with Sorted List
# Idea: Use Positional List, make sure you add elements
# so that the relative positions before and after are
# smaller and larger respectively
# Thus, the the remove_min() and min() operations will be
# O(1), but adding elements will require O(n) in the
# worst case

class SortedPriorityQueue(PriorityQueueBase):
    def __init__(self):
        self._data = PositionalList()

    def __len__(self):
        return len(self._data)

    def min(self):
        if self.is_empty():
            raise Empty('Priority queue is empty')
        p = self._data.first()
        item = p.element()
        return (item._key, item._value)

    def remove_min(self):
        if self.is_empty():
            raise Empty('Priority queue is empty')
        item = self._data.delete(self._data.first())
        return (item._key, item._value)

    def add(self, key, value):
        newest = self._Item(key, value)
        walk = self._data.last()   # walking backwards
        while walk is not None and newest < walk.element():
            walk = self._data.before(walk)
        if walk is None:
            self._data.add_first(newest)
        else:
            self._data.add_after(walk, newest)

    




















