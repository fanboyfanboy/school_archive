# Abstract Data Type : Binary Trees
# Abstract class, not providing complete specifications
# A Binary Tree B inherits from Tree and supports
# B.left(p) : Return the position of the left child of p, or None
# B.right(p) : Return the position of the right child of p, or None
# B.sibling(p) : Return the position that represents sibling of p, or None

from Trees import Tree

class BinaryTree(Tree):
    def left(self, p):
        raise NotImplementedError("must be implemented by subclass")

    def right(self, p):
        raise NotImplementedError("must be implemented by subclass")

    def sibling(self, p):
        parent = self.parent(p)
        if parent is None:
            return None
        else:
            if p == self.left(parent):
                return self.right(parent)
            else:
                return self.left(parent)

    def children(self, p):
        if self.left(p) is not None:
            yield self.left(p)
        if self.right(p) is not None:
            yield self.right(p)

    # Inorder tree traversal
    def inorder(self):
        if not self.is_empty():
            for p in self._subtree_inorder(self.root()):
                yield p

    def _subtree_inorder(self, p):
        if self.left(p) is not None:
            for other in self._subtree_inorder(self.left(p)):
                yield other
        if self.right(p) is not None:
            for other in self._subtree_inorder(self.right(p)):
                yield other

    # Override positions method
    def positions(self):
        return self.inorder()


