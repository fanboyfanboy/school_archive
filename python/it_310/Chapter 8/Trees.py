# Abstract Data Type : Tree
# Tree uses position abstraction to determine node in tree
# Positions will contain elements, positions are comparable and must satisfy parent-child relationship
# Tree will be an abstract class with no mutator methods.  These will be implemented by specific applications via
# concrete classes
#
# Tree T supports (and checks positions are valid)
# p.element() : Return element at p
# T.root() : Return position of root
# T.is_root(p) : Return True if p is root
# T.parent(p) : Return position of parent of p; None if p is root
# T.num_children(p) : Return the number of children of p
# T.children(p) : Generate an iteration of the children of p; empty iteration if p is leaf
# T.is_leaf(p): Return True if p has no children
# len(T) : Return number of positions of T
# T.is_empty(): Return True if T has no positions
# T.positions() : Generate an iteration of all positions of T; empty if T is empty
# iter(T) : Generate iteration of all elements stored in T; empty if T is empty

from LinkedQueues import LinkedQueue

class Tree:
    """Abstract base class for a tree structure"""

    # ----------- Position class ------------------
    class Position:
        def element(self):
            raise NotImplementedError('must be implemented by subclass')

        def __eq__(self, other):
            raise NotImplementedError('must be implemented by subclass')

        def __ne__(self, other):
            return not (self == other)

    # ---------------------------------------------

    def root(self):
        raise NotImplementedError('must be implemented by subclass')

    def parent(self):
        raise NotImplementedError('must be implemented by subclass')

    def num_children(self, p):
        raise NotImplementedError('must be implemented by subclass')

    def children(self, p):
        raise NotImplementedError('must be implemented by subclass')

    def __len__(self):
        raise NotImplementedError('must be implemented by subclass')

    # ---------------------------------------------
    def is_root(self, p):
        return self.root() == p

    def is_leaf(self, p):
        return self.num_children(p) == 0

    def is_empty(self):
        return len(self) == 0

    # Depth function
    # Recursive definition
    # Number of ancestors of p excluding p itself
    def depth(self, p):
        if self.is_root():
            return 0
        else:
            return 1 + self.depth(self.parent())

    # Analysis: The time per recursive step is constant
    # for each ancestor of p.  If there are d ancestors, then
    # the running time is O(d + 1)
    # In the worst-case scenario, if the tree is a single branch,
    # running time is O(n).
    # Notice that d is much smaller that n in general, since we do not
    # care about all the nodes in a tree.

    # By Proposition 8.4: Height of a non-empty tree T is the max
    # depth of all leaves
    # Idea: translate height into depth an use the depth function

    def _height1(self):
        return max(self.depth(p) for p in self.positions() if self.is_leaf())

    # Analysis:
    # We will see an implementation of positions() that runs in O(n) times.
    # _height1 calls depth() for each leaf, and depth is O(n). Thus running time
    # is O(n + sum(d + 1)) where d is the number of ancestors of a leaf.
    # In the worst-case scenario, (think of the chicken foot looking tree)
    # we get O(n^2) time complexity

    # Instead, let's use the recursive definition of height
    def _height2(self, p):
        if self.is_leaf(p):
            return 0
        else:
            return 1 + max(self._height2(c) for c in self.children(p))


    # Analysis:
    # Recursive call on _height2 calls all children at least once.
    # Assume children(p) runs in O(cp + 1), where cp is the number of
    # children of p,
    # then the overall running time of _height2 is O(sum(cp + 1)) =
    # O(n + sum(cp)), see that there are n - 1 children of the root,
    # that is sum(cp) = n - 1, thus O(n + n) = O(n)


    # Wrapper for the _height2 method to allow users to compute height
    # of all sub-trees as well as the entire tree
    def height(self, p=None):
        if p is None:
            p = self.root()
        return self._height2(p)


    # Tree Traversal algorithms are used to generate iterators
    # choose between preorder, postorder, breadthfirst
    
    def __iter__(self):
        for p in self.positions():
            yield p.element()
        
    # Preorder
    def preorder(self):
        if not self.is_empty():
            for p in self._subtree_preorder(self.root()):
                yield p
            
    def _subtree_preorder(self, p):
        yield p
        for c in self.children(p):
            for other in self._subtree_preorder(c):
                yield other

    # Preorder

    def postorder(self):
        if not self.is_empty():
            for p in self._subtree_postorder(self.root()):
                yield p

    def _subtree_postorder(self, p):
        for c in self.children(p):
            for other in self._subtree_postorder(c):
                yield other
        yield p

    # Breadthfirst
    def breadthfirst(self):
        if not self.is_empty():
            Q = LinkedQueue()
            Q.enqueue(self.root())
            while not Q.is_empty():
                p = Q.dequeue()
                yield p
                for c in self.children(p):
                    Q.enqueue(c)

    # Return iterator for positions
    def positions(self, mode="breadthfirst"):
        if mode == "preorder":
            return self.preorder()
        if mode == "postorder":
            return self.postorder()
        if mode == "breadthfirst":
            return self.breadthfirst()


    
    











