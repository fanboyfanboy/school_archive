from time import time
import matplotlib.pyplot as plt


def disjoint1(A, B, C):
	for i in A:
		for j in B:
			for z in C:
				if i == j == z:
					return False	
	return True


def disjoint2(A, B, C):
	for a in A:
		for b in B:
			if a == b:
				for c in C:
					if a == c:
						return False
	return True


A_1 = [1,2,3,4]
B_1 = [5,6,3,8]
C_1 = [5,6,7,3]

A_2 = [4,3,2,1]
B_2 = [8,3,6,5]
C_2 = [3,7,6,5]

A_3 = [9,8,6,6]
B_3 = [23,4,5]
C_3 = [1,2,3,4]

disjoint1_times = []
disjoint2_times = []

x = [i for i in range(100, 600, 5)]
for j in x:
	start_time = time()
	disjoint1(A_1, B_1, C_1)
	end_time = time()
	disjoint_time1 = start_time - end_time
	start_time = time()
	disjoint2(A_1, B_1, C_1)
	end_time = time()
	disjoint_time2 = start_time - end_time
	disjoint1_times.append(disjoint_time1)
	disjoint2_times.append(disjoint_time2)


print(disjoint1_times)
print(disjoint2_times)

# Plot times vs input size
plt.scatter(x, disjoint1_times, color="red")
plt.scatter(x, disjoint2_times, color="blue")
plt.ylabel("Running Time" + r"$(ms)$")
plt.xlabel("Input Size" + r"$(n) \tau$")
plt.ylim(-.0001, .0001)
plt.show()