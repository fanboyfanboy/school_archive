# Exam 2
# Dalton Cothron
# IT310

####################################################################
# Imports 
####################################################################

from DLLDeque import LinkedDeque
from time import time

####################################################################
# Problem 1
####################################################################

# see attached picture in file for problem 1


####################################################################
# Problem 2
####################################################################

a_seq = [2, 4, 7, 8, 5, 4, 3, 5, 7, 8, 9, 0, 8, 7, 6, 4, 5];
b_seq = [];
length = len(a_seq) %2
start_time = time()
for i in range(length -1):
    b_seq[i] = (a_seq[i] *2) + (a_seq[i]*2+1);
    print("i= " + str(i));
if len(b_seq)== 1:
    print("0 index: " + str(b_seq[0]));
else:
    a_seq = b_seq
end_time = time();
print("total time taken: " + str(end_time - start_time));


# The algorithm is running in (O)n^2 time


####################################################################
# Problem 3
####################################################################



####################################################################
# Problem 4
####################################################################




####################################################################
# Bonus 1
####################################################################
if __name__ == "__main__":
    
    ld = LinkedDeque();


    a = ld.insert_first("A");
    b = ld.insert_first("B");

    # insert_first() test
    if "B" != ld.first():
        print("B should be first, found: " + str(ld.first()) + ", should be: " + str(b));
    else:
        print("B is first, found: " + str(ld.first()));
    print();

    # insert_last() test
    c = ld.insert_last("C");
    if "C" != ld.last():
        print("C not inserted last, found: " + str(ld.last()) + ", should be: " + str(c));
    else:
        print("C inserted last, found:  " + str(ld.first));
    print();


    # delete_first() test
    d = ld.delete_first();
    if "B" != d:
        print("d var should be: " + str(b) + ", but value was: " + str(d));
    else:
       print("Correct node deleted, found:  " + str(d));
    print();

    # delete_last() test
    e = ld.delete_last();
    if "C" != e: 
       print("e var should be : " + str(c) + ", but value was: " +str(e));
    else:
      print("Correct node deleted, found:  " + str(e));



####################################################################
# Bonus 2
####################################################################

