# Dalton Cothron
# Exam 3
# IT310 Spring 2017
# Tests will be at the end of the program


##############################################################################################
# Imports
##############################################################################################
from CircularlyLinkedLists import CLinkedList
from PositionalLists import PositionalList
from time import time

##############################################################################################
# Question 1 (25xp): 
# Implement a function that counts the number of nodes in a cirularly linked list by re-implementing
#   the len() method in a way that does not use the precalculated size member.

# Code (if necessary)
# **NOTE TO PROFESSOR***:
# My implementation of the len() method is inside the attached CircularlyLinkedLists.py class


##############################################################################################
# Question 2 (25xp):
# Implement a function that accepts a PositionalList *L* of *n* integers sored in a nondescreasing order, and another
#   value *V*, and determines in O(n) time if there are two elements of *L* that sum precisely to V.  That function should
#   return a pair of positions of such elements, if found, or None otherwise.

def find_sum_of(L, V):
	for x in range(len(L)):
		num1 = L[x];
		num2 = L[len(L)-x];
		if num1+num2 == V:
			return [num1, num2]
	return None


##############################################################################################
# Question 3 (25xp):
# Prove that the number of leaves in a binary tree of height *H* is at most 2^(H). [HINT: Use Induction]
# Analysis: 
#  At most, a each single leaf can have 2 children.  So if the leaf of the tree is not the final leaf, it will continue to 
# Quantify at a power^2/leaf. Giving us a Tree((child)^2) as a maximum number of leaves



##############################################################################################
# Question 4 (25xp):
# Prove that a non-empty binary tree of height *H* has at most 2^(H+1)-1 nodes. [HINT: Use Induction]

# By Proposition 8.4: Height of a non-empty tree T is the max
# depth of all leaves
# Idea: translate height into depth an use the depth function
def _height1(self):
	return max(self.depth(p) for p in self.positions() if self.is_leaf())


##############################################################################################
# Tests

if __name__ == "__main__":
	# Question 1
	print('**Start Question 1 tests.**');
	CLL = CLinkedList();
	for x in range(10):
		CLL.insert(str(x));
		print('Length= ' +str(len(CLL))+'\n');
		print('Current= ' +CLL.current()+'\n');
	print('\n**End Question 1 tests.**');
	
	# Question 2
	print("\n**Start Question 2 tests.**");
#	pl = PositionalList();
#	pl.add(15);
#	for x in range(10):
#		pl.add_last(x);
#		print(str(x));
#	start_time = time();
#	find_sum_of(pl, 10);
#	end_time = time()
#	print("Time taken: " + str(start_time-end_time));
	print("******** COULD NOT PROUDCE QUESTION 2 TESTS. FAILURE IN PositionalLists.py****************");
	print("\nDaltons-MacBook-Pro-2:Exam3 Dalton$ python3 positionallists.py ");
	print("\nTraceback (most recent call last): File positionallists.py, line 203, in <module> pB = pl.add_before(pC, B)");
	print("\nFile positionallists.py, line 117, in add_before return self._insert_between(e, valid_position._prev, valid_position");
	print("\nAttributeError: '_Node' object has no attribute _prev");
	print("\n**End Question 2 tests.**");
