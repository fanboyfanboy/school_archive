def quick_sort(S):
    """Return a sorted version of list S"""
    n = len(S)
    if n < 2:
        return

    # DIVIDE
    p = S[-1]     # last element as pivot p
    L = []        # will hold elements < p
    E = []        # will hold elements = p
    G = []        # will hold elements > p
    i = 0         # index to traverse list

    while S != []:
        if S[i] < p:
            L.append(S.pop(i))   # S.pop() its expensive
        elif S[i] == p:
            E.append(S.pop(i))
        else:
            G.append(S.pop(i))

    # CONQUER
    quick_sort(L)
    quick_sort(G)

    # COMBINE
    while L != []:
        S.append(L.pop(i))
    while E != []:
        S.append(E.pop(i))
    while G != []:
        S.append(G.pop(i))

myList = [15, 3, 2, 1, 9, 5, 7, 8, 6]
#quick_sort(myList)
print(myList)


# Altenative 1: Choose pivot at the beginning of list and
# pop from the end. Still recall that O(1) is ammortized.

# Better if we avoid dynamic arrays
from LinkedQueues import LinkedQueue

LQ = LinkedQueue()
for i in myList:
    LQ.enqueue(i)

def print_queue(Q):
    """Print out queue elements"""
    for i in range(len(Q)):
        temp = Q.dequeue()
        print(temp)
        Q.enqueue(temp)

print_queue(LQ)

def quick_sort2(S):
    """Return a sorted version of queue S"""
    n = len(S)
    if n < 2:
        return

    # DIVIDE
    p = S.first()     # last element as pivot p
    L = LinkedQueue()        # will hold elements < p
    E = LinkedQueue()        # will hold elements = p
    G = LinkedQueue()        # will hold elements > p

    while not S.is_empty():
        if S.first() < p:
            L.enqueue(S.dequeue())
        elif S.first() == p:
            E.enqueue(S.dequeue())
        else:
            G.enqueue(S.dequeue())

    # CONQUER
    quick_sort2(L)
    quick_sort2(G)

    # COMBINE
    while not L.is_empty():
        S.enqueue(L.dequeue())
    while not E.is_empty():
        S.enqueue(E.dequeue())
    while not G.is_empty():
        S.enqueue(G.dequeue())

# TESTING QUICK SORT WITH QUEUE
#quick_sort2(LQ)
print()
#print_queue(LQ)

# IMPLEMENT RANDOMIZATION TO AVOID O(n^2) complexity of quick-sort
from random import randint

def random_shift_queue(Q):
    """Randomly shift elements in queue"""
    shift = randint(1, len(Q))
    for i in range(shift):
        Q.enqueue(Q.dequeue())

def randomized_quick_sort(S):
    """Return a sorted version of queue S"""
    n = len(S)
    if n < 2:
        return
    # "RANDOMIZE" QUEUE
    random_shift_queue(S)

    # DIVIDE
    p = S.first()     # last element as pivot p
    L = LinkedQueue()        # will hold elements < p
    E = LinkedQueue()        # will hold elements = p
    G = LinkedQueue()        # will hold elements > p

    while not S.is_empty():
        if S.first() < p:
            L.enqueue(S.dequeue())
        elif S.first() == p:
            E.enqueue(S.dequeue())
        else:
            G.enqueue(S.dequeue())

    # CONQUER
    randomized_quick_sort(L)
    randomized_quick_sort(G)

    # COMBINE
    while not L.is_empty():
        S.enqueue(L.dequeue())
    while not E.is_empty():
        S.enqueue(E.dequeue())
    while not G.is_empty():
        S.enqueue(G.dequeue())

# TESTING RANDOMIZED QUICK SORT
#randomized_quick_sort(LQ)
#print_queue(LQ)


def inplace_quick_sort(S, start, end):
    """Return a sorted version of S in place"""
    if start >= end:
        return
    pivot = S[end]    # USING PYTHON LISTS
    first = current = start

    while current < end:
        if S[current] >= pivot:
            # update index of current
            current += 1
        elif S[current] < pivot:
            # swap current with first
            S[first], S[current] = S[current], S[first]
            # update index of current
            current += 1
            # update index of first
            first += 1

    # Put pivot in final(correct) place
    S[first], S[end] = S[end], S[first]

    # Make recursive calls
    inplace_quick_sort(S, start, first - 1)
    inplace_quick_sort(S, first + 1, end)


myList = [3,1,2,5,7,6,6,6,7,10,2,3,4,4,2,1,1,3,2,11]
inplace_quick_sort(myList, 0, len(myList) - 1)
print(myList)




























