# Exam 4 - Final 
# IT310
#
# @Author: Dalton Cothron

# imports
from random import randint
from time import time
import matplotlib.pyplot as plt

# Problem 1) 

def bubble_sort(aList):
	for x in range(len(aList)-1, 0, -1):
		for y in range(x):
			if aList[y]>aList[y+1]:
				temp = aList[y];
				aList[y] = aList[y + 1];
				aList[y+1] = temp


def merge(S1, S2, S):
	i = j = 0;
	while i + j < len(S):
		if j == len(S2) or (i < len(S1) and S1[i] < S2[j]):
			S[i + j] = S1[i];
			i += 1;
		else:
			S[i + j] = S2[j];
			j += 1;


def merge_sort(aList):
	length = len(aList);
	if length < 2:
		return;
	
	mid = length//2;
	L1 = aList[:mid];
	L2 = aList[mid:];

	merge_sort(L1);
	merge_sort(L2);

	merge(L1, L2, aList);


def quick_sort(aList):
	length = len(aList);
	if length < 2:
		return;

	p = aList[-1];
	L = [];
	E = [];
	G = [];
	i = 0;

	while aList != []:
		if aList[i] < p:
			L.append(aList.pop(i));
		elif aList[i] == p:
			E.append(aList.pop(i));
		else:
			G.append(aList.pop(i));

	quick_sort(L);
	quick_sort(G);

	while L != []:
		aList.append(L.pop(i));
	while E != []:
		aList.append(E.pop(i));
	while G != []:
		aList.append(G.pop(i));


if __name__ == '__main__':

	List_S = [8, 8 ,7 , 5 ,4 ,0 , 56, 89, 11];
	List_S1 = [8, 8 ,7 , 5 ,4 ,0 , 56, 89, 11];
	List_S2 = [8, 8 ,7 , 5 ,4 ,0 , 56, 89, 11];

	# Quick unit test methods are working
	print('begin smoketest test.');
	print('\n**List values**');
	print(List_S);

	print('\n**merge sort**');
	merge_sort(List_S);
	print(List_S);

	print('\n**quick sort**');
	quick_sort(List_S1);
	print(List_S1);

	print('\n**bubble sort**');
	bubble_sort(List_S2);
	print(List_S2);

	print('\nEnd smoketest');


	# ****** NOTE TO PROFESSOR ******
	# Using three lists of a random size between 1->999 to get average
	#
	#   Values are random between 1->999 but the same for each list# 

	# Time holders 
	bubble_sort_times = [];
	merge_sort_times = [];
	quick_sort_times = [];


	## List 1 testing values 
	list_one_control = [];
	list_oneM = [];
	list_oneB = [];
	list_oneQ = [];
	## List 2 testing values 
	list_two_control = [];
	list_twoM = [];
	list_twoB = [];
	list_twoQ = [];
	## List 3 testing values 
	list_three_control = [];
	list_threeM = [];
	list_threeB = [];
	list_threeQ = [];

# produce list 1 testing values
	x = [i for i in range(100, 600, 5)];
	for j in x:
		temp = randint(0, 999)
		list_one_control.append(temp);
		list_oneM.append(temp);
		list_oneQ.append(temp);
		list_oneB.append(temp);
	
	# bubble sort -- list1
	start_time = time();
	bubble_sort(list_oneB);
	end_time = time();
	bubble_sort_times.append(start_time-end_time);
	# merge sort -- list1
	start_time = time();
	merge_sort(list_oneM);
	end_time = time();
	merge_sort_times.append(start_time-end_time);
	# quick sort -- list1
	start_time = time();
	quick_sort(list_oneQ);
	end_time = time();
	quick_sort_times.append(start_time-end_time);


	print('**\nBegin Analysis**');

	print('Original Values of List1: \n');
	print(list_one_control);
	print('\nMerge Sort Time: ' + str(merge_sort_times[0]));
	print('\nQuick Sort Time: ' + str(quick_sort_times[0]));
	print('\nBubble Sort Time:  ' + str(bubble_sort_times[0]));
	print('\nMerge Sort Values: ' + str(list_oneM));
	print('\nBubble Sort Values: ' + str(list_oneB));
	print('\nQuick Sort Values: ' + str(list_oneQ));

	# plot it 
	plt.scatter(x, list_oneB, color="red");
	plt.scatter(x, list_oneQ, color="blue");
	plt.scatter(x,list_oneM, color="green");
	plt.ylabel("Running Time" + r"$(ms)$");
	plt.xlabel("Input Size" + r"$(n) \tau$");
	plt.ylim(0, 1000);
	plt.show();
