def merge(S1, S2, S):
    """Given sorted lists S1 and S2, merge them into a new sorted list."""
    i = j = 0

    while i + j < len(S):
        if j == len(S2) or (i < len(S1) and S1[i] < S2[j]):
            S[i + j] = S1[i]
            i += 1
        else:
            S[i + j] = S2[j]
            j += 1

def merge_sort(S):
    n = len(S)
    if n < 2:
        return
    # DIVIDE
    mid = n//2
    S1 = S[:mid]
    S2 = S[mid:]

    # CONQUER
    merge_sort(S1)
    merge_sort(S2)

    # COMBINE
    merge(S1, S2, S)


List_S = [8, 8 ,7 , 5 ,4 ,0 , 56, 89, 11]
merge_sort(List_S)
print(List_S)













