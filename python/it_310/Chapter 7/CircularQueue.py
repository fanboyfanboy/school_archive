class CircularQueue:
    # ----------- nested _Node class --------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_next'  # memory efficiency

        def __init__(self, element, next):
            self._element = element
            self._next = next

        def __repr__(self):
            return '[{0},and a link]'.format(self._element)

    # ---------------------------------------------

    # ----------- nested Error class --------------
    class Empty(Exception):
        pass
    # ---------------------------------------------

    def __init__(self):
        self._tail = None
        self._size = 0

    def __len(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def first(self):
        if self.is_empty():
            raise self.Empty("Queue is empty")
        head = self._tail._next
        return head._element

    def dequeue(self):
        if self.is_empty():
            raise self.Empty("Queue is empty")
        oldhead = self._tail._next
        if self._size == 1:
            self._tail = None
        else:
            self._tail._next = oldhead._next
        self._size -= 1
        return oldhead._element

    def enqueue(self, e):
        newest = self._Node(e, None)
        #if self.is_empty():
        #    raise self.Empty("Queue is empty")
        if not self.is_empty():
            newest._next = self._tail._next
            self._tail._next = newest
        self._tail = newest
        self._size += 1

    def rotate(self):
        if self._size > 0:
            self._tail = self._tail._next
