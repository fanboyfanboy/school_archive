# Abstract Data Type : Circularly Linked List
# A circularly linked list CLL supports
# All methods of linked list
# tail._next = head

class CLinkedList:
    # ----------- nested _Node class --------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_next'  # memory efficiency

        def __init__(self, element, next):
            self._element = element;
            self._next = next;

        def getElement(self):
            return self._element;

        def getNext(self):
            return self._next;

    # ---------------------------------------------
    # ----------- nested Error class --------------
    class Empty(Exception):
        pass;
    # ---------------------------------------------

    def __init__(self):
        self._current = None;
        self._size = 0;


#    def __len__(self):
#        return self._size
     def __len__(self):
         size = 0;
         if self._current is not None:
         	size++;
         	while self.getNext() is not None:
         		size++;
         	return size;
         	return 0;

    def current(self):
        if self._current is not None:
            return self._current.getElement();
        else:
            return self._current;

    def next(self):
        """Returns element after current and updates current"""
        if self.is_empty():
            raise self.Empty("Linked list is empty");
        if self._size > 1:      # at least two element
            self._current = self._current.getNext();
        return self.current();

    def is_empty(self):
        return len(self) == 0;

    def insert(self, e):
        """Insert e after current element and update current"""
        if self._current is not None:
            new_node = self._Node(e, self._current.getNext());
            self._current = new_node;
        else:
            new_node = self._Node(e, self._current);
            self._current = new_node;
            self._current._next = new_node;
        self._size += 1;

    def remove(self):
        """Removes and returns the element after current, or current if
        its the only element"""
        if self.is_empty():
            raise self.Empty("Linked List is empty");
        elif self._size == 1:
            temp = self._current;
            self._current = None;
        elif self._size == 2:
            temp = self._current._next;
            self._current._next = self._current;
        else:
            temp = self._current._next;
            self._current._next = self._current._next._next;
        self._size -= 1;
        return temp.getElement();

if __name__ == "__main__":
    CLL = CLinkedList();
    print(CLL.current());

    CLL.insert("A");
    print(CLL.current());

    CLL.insert("B");
    print(CLL.current());
    print(CLL._current._next._element);

    print(CLL.remove());
    print(CLL.remove());
    print(CLL.remove());