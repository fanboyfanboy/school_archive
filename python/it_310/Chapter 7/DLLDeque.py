# Abstract Data Type : Double Ended Queue
# Underlying structure is a Doubly Linked List
# implemented as an Extension of _DoublyLinkedList

# Recall
# Deque D supports:
# D.insert_first(e)
# D.insert_last(e)
# D.delete_first()
# D.delete_last()
# D.first()
# D.last()
# D.is_empty()
# len(D)

from DoublyLinkedList import _DoublyLinkedList

class LinkedDeque(_DoublyLinkedList):

    def first(self):
        if self.is_empty():
            raise self.Empty("Deque is empty");
        return self._header._next._element

    def last(self):
        if self.is_empty():
            raise self.Empty("Deque is empty");
        return self._trailer._previous._element

    def insert_first(self, e):
        self._insert_between(e, self._header, self._header._next);

    def delete_first(self):
        if self.is_empty():
            raise self.Empty("Deque is empty");
        return self._delete_node(self._header._next);

    def insert_last(self, e):
        self._insert_between(e, self._trailer._previous, self._trailer);

    def delete_last(self):
        if self.is_empty():
            raise self.Empty("Deque is empty");
        return self._delete_node(self._trailer._previous);

####################################################################
# Bonus 1
####################################################################
if __name__ == "__main__":
    
    ld = LinkedDeque();


    a = ld.insert_first("A");
    b = ld.insert_first("B");

    # insert_first() test
    if "B" != ld.first():
        print("B should be first, found: " + str(ld.first()) + ", should be: " + str(b));
    else:
        print("B is first, found: " + str(ld.first()));
    print();

    # insert_last() test
    c = ld.insert_last("C");
    if "C" != ld.last():
        print("C not inserted last, found: " + str(ld.last()) + ", should be: " + str(c));
    else:
        print("C inserted last, found:  " + str(ld.first));
    print();


    # delete_first() test
    d = ld.delete_first();
    if "B" != d:
        print("d var should be: " + str(b) + ", but value was: " + str(d));
    else:
       print("Correct node deleted, found:  " + str(d));
    print();

    # delete_last() test
    e = ld.delete_last();
    if "C" != e: 
       print("e var should be : " + str(c) + ", but value was: " +str(e));
    else:
      print("Correct node deleted, found:  " + str(e));