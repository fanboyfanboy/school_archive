# Abstract Data Type : Stack
# Underlying data structure is a linked list

class LinkedStack:
    """LIFO Stack"""
    # ----------- nested _Node class --------------
    class _Node:
        """Lightweight private class for storing a linked node"""
        __slots__ = '_element', '_next'  # memory efficiency

        def __init__(self, element, next):
            self._element = element
            self._next = next

    # ---------------------------------------------
    # ----------- nested Error class --------------
    class Empty(Exception):
        pass
    # ---------------------------------------------

    def __init__(self):
        self._head = None        # reference to head node... starts empty stack
        self._size = 0           # number of stack elements

    def __len__(self):
        return self._size

    def is_empty(self):
        return self._size == 0

    def push(self, e):
        self._head = self._Node(e, self._head)
        self._size += 1

    def top(self):
        if self.is_empty():
            raise self.Empty("Stack is empty")
        return self._head_element

    def pop(self):
        if self.is_empty():
            raise self.Empty("Stack is empty")
        answer = self._head._element
        self._head = self._head._next
        self._size -= 1
        return answer
