# Abstract Data Type : Stack
# Underlying data structure is a linked list using Adapter Design Pattern

from SinglyLinkedList import SLinkedList as sll

# Stack -> sll
# push() -> insert_head()
# pop() -> remove_head()
# top() -> head()
# is_empty()
# len()

class SLLStack:
    def __init__(self):
        self._data = sll()

    def __len__(self):
        return len(self._data)

    def is_empty(self):
        return self._data.is_empty()

    def push(self, e):
        self._data.insert_head(e)

    def pop(self):
        if self._data.is_empty():
            raise self._data.Empty("Stack is empty")
        return self._data.remove_head()

    def top(self):
        if self._data.is_empty():
            raise self._data.Empty("Stack is empty")
        return self._data.head()

if __name__ == "__main__":
    # Testing SLLStack methods
    S = SLLStack()
    S.push("dog")
    S.push(3)
    print(len(S))
    print(S.pop())
    print(S.is_empty())
    print(S.pop())
    print(S.is_empty())
    #S.pop()
    S.push("Hello World")
    print(S.top())