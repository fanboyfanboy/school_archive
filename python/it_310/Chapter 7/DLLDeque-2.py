# Abstract Data Type : Double Ended Queue
# Underlying structure is a Doubly Linked List
# implemented as an Extension of _DoublyLinkedList

# Recall
# Deque D supports:
# D.insert_first(e)
# D.insert_last(e)
# D.delete_first()
# D.delete_last()
# D.first()
# D.last()
# D.is_empty()
# len(D)

from DoublyLinkedList import _DoublyLinkedList

class LinkedDeque(_DoublyLinkedList):

    def first(self):
        if self.is_empty():
            raise self.Empty("Deque is empty")
        return self._header._next._element

    def last(self):
        if self.is_empty():
            raise self.Empty("Deque is empty")
        return self._trailer._previous._element

    def insert_first(self, e):
        self._insert_between(e, self._header, self._header._next)

    def delete_first(self):
        if self.is_empty():
            raise self.Empty("Deque is empty")
        return self._delete_node(self._header._next)

    def insert_last(self, e):
        self._insert_between(e, self._trailer._previous, self._trailer)

    def delete_last(self):
        if self.is_empty():
            raise self.Empty("Deque is empty")
        return self._delete_node(self._trailer._previous)


















