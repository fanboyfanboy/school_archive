#Dalton Cothron
#PA2

#Import statement
from PA2Data1 import array

#Set rows and columns
ROWS = len(array)
COLUMNS = len(array[0])

#Main function
def main():
    printRow(array)

#Min value function, could not format to get to print
def minValue(prompt):
    minNumber = min(prompt)
    return minNumber

#Max value function, could not format to get to print
def maxValue(prompt):
    maxNumber = max(prompt)
    return maxNumber

#Print arrays
def printRow(arrayInp):
    print("+" + ("-"*5) * COLUMNS +"+","%5s %5s %8s %8s" % ("Min", "Max", "Mean", "S.D.")) #Print first line
    for i in range(ROWS):
        for j in range(COLUMNS):
            print("%5d" % array[i][j],end="") #Print array data
        print()
    print("+" + ("-"*5) * COLUMNS +"+")
    print("Column Sums")
    for j in range(COLUMNS):
        total = 0
        
        for i in range(ROWS): #Print column total
            total = total + array[i][j]
        print("%5d" % total, end="")
    print()        

#Start the program        
main()
    
