#Dalton Cothron
# P6.7
#

def main():
    values = [-9,1,-2,2,3,4,-123213,1235,1934,-34,-314,5]
    print(removeMin(values))

#declare function removeMin
# @param prompt takes list
# @return returns the list with smallest value removed
#
def removeMin(prompt):
    
    #Set the smallest value and index position
    index = 0
    smallest = prompt[0]

    #Loop to find the smallest value and position in list
    for i in range(1,len(prompt)):
        if prompt[i] < smallest:
            smallest = prompt[i]
            index = i
    prompt.pop(index)
    return prompt

main()
