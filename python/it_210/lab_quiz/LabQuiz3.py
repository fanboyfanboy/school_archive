#Lab quiz 3
#Dalton Cothron

#import randint
from random import randint

#runs rollDice, gets values and total sum
def main():
    values = rollDice()
    totalSum = sum(values)
    print(values)
    while totalSum > 11 and totalSum < 24: #loop to check if within range
        values = rollDice()
        totalSum = sum(values)
        print(values)


#rolls a 6 sided die 5 times
#returns the tuple
def rollDice():
    dice = []
    for i in range(5): #loop to run 5 times
        dice.append(randint(1,6))
    dice.sort()
    dieOne = dice[0]
    dieTwo = dice[1]
    dieThree = dice[2]
    dieFour = dice[3]
    dieFive = dice[4]
    return dieOne,dieTwo,dieThree,dieFour,dieFive

#starts the program
main()

