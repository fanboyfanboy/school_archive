# Dalton Cothron
#Lab Quiz 4a

class Dog :
    #Construct dog object
    def __init__(self, name, breed):
        self._name = name
        self._breed = breed
        self._age = 0
        self._weight = 0

     #Set age   
    def setAge(self, age):
        self._age = age

    #Set weight      
    def setWeight(self, weight):
        self._weight = weight

    #Set age      
    def getAge(self):
        return self._age

    #Get weight       
    def getWeight(self):
        return self._weight

    #Get name        
    def getName(self):
        return self._name

    #Get breed
    def getBreed(self):
        return self._breed
    
    #Return final string
    def __repr__(self):
        return "%d year-old %s named %s weighing %.1f pounds." %(self._age,self._name,self._breed,self._weight)
        
        
if __name__ == "__main__":
    dogs = []
    # Construct dogs.
    dogs.append(Dog("Fido", "Black Labrador"))
    dogs.append(Dog("Snoopy", "Beagle"))
    dogs.append(Dog("Spot", "Dalmatian"))
    
    ages = [3,6,4]
    weights = [55.5, 23.85, 40.25]
    
    for i in range(3):
        dogs[i].setAge(ages[i])
        dogs[i].setWeight(weights[i])
      
    # Generate the output.  
    for dog in dogs:
        print(dog.getName())
        print(dog.getBreed())
        print(dog.getAge())
        print(dog.getWeight())
        print()
        
    for dog in dogs:
        print(dog)
        
        
