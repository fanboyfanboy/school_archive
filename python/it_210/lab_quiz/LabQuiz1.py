#Dalton Cothron
#Lab Quiz 1

#Get input from user.
stringInp = input("PLease enter a string: ")

#Check for empty string
if stringInp == "":
    print("Empty string.")
else:
    if len(stringInp) % 2 == 0: #Check if string is even
        print("The second character is: ",stringInp[1])
    else:
        i = len(stringInp) // 2 #Get middle character
        if(stringInp[i]).isdigit(): #Check if middle character is digit
            print("The middle character is a digit") 
        else:
            print("The middle character is: ", stringInp[i])
                                                                
#Print done at bottom.
print("Done.")
