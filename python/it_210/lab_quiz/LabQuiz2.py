#Dalton Cothron
#Lab Quiz 2
#

#Set main function, get integers
def main():
    firstInt = input("Please enter the first number: ")
    secondInt = input("Please enter the second number: ")
    computX(firstInt,secondInt)

##Computes the value of x
# @paramX1 is the first integer
# @paramX2 is the second integer
#
def computX(x1,x2):
    intOne = int(x1)
    intTwo = int(x2)
    #Checking for either integer to be less that 1
    if intOne < 1 or intTwo < 1 :
        print("Invalid input.")
        return
    
    #Swaps the values if int1 is less than in2
    if intOne < intTwo:
        intOne,intTwo = intTwo,intOne
        #While loop to compute the remainder
        while intTwo != 0:
            remainder = intOne % intTwo
            intOne = intTwo
            intTwo = remainder
        print("The value is", intOne)
    else:
        while intTwo != 0:
            remainder = intOne % intTwo
            intOne = intTwo
            intTwo = remainder
        print("The value is", intOne)

#starts the program
main()
