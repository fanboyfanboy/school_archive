#Lab10
#Dalton Cothron

#Determines if sequence is increasing
def isIncreasing(seq):

    #Check if length is equal to 0 or 1
    if len(seq) == 0 or len(seq) == 1:
        return True
    else:
        #Determine if it is larger
        if seq[0] <= seq[1]:
            shorter = seq[1:]
            return isIncreasing(shorter)
        else:
            return False

