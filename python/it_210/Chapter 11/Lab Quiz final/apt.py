#Dalton Cothron
#Lab Quiz 5

class Apartment:
    #construct apartment class
    def __init__(self,aptNumber,address,bedrooms,baths):
        brooms = int(bedrooms)
        barooms = float(baths)
        self._aptNumber = aptNumber
        self._address = address
        self._bedrooms = brooms
        self._baths = barooms
        self._securityDep = 330 * brooms
        self._rent = (220 * brooms) + (150 * barooms) + 200
        self._renter = "Vacant"

    #Set a new value for security deposit
    def setSecDep(self,securityDep):
        securityDeposit = float(securityDep)
        self._securityDep = securityDeposit

    #Set a new value for rent
    def setRent(self,rent):
        rentTotal = float(rent)
        self._rent = rentTotal

    #Set a new renter
    def setRenter(self,renter):
        if renter != "":
            self._renter = renter
        else:
            self._renter = "Vacant"

    #Get user data
    def getData(self):
        firstString = "Apt %s located at %s" %(self._aptNumber,self._address)
        secondString = "%d bedrooms and %.2f baths" % (self._bedrooms,self._baths)
        thirdString = "$.2d security deposit and %.2f rent" % (self._securityDep, self._rent)
        fourthString = "Rented to: %s" % (self._renter)
        return firstString + secondString + thirdString + fourthString



    def __repr__(self):
        "Apt %s @ %s %d BR  %.2f  Bath  Dep=$%.2d  Rent=$%.2d  %s" % (self._aptNumber,self._address,self._bedrooms,self._baths,self._securityDep,self._rent,self._renter)
