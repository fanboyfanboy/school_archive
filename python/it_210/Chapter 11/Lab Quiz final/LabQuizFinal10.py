# Your Name
#Dalton Cothron
# Your code goes here
#Dalton Cothron
#Lab Quiz 5

class Apartment:
    #construct apartment class
    def __init__(self,aptNumber,address,bedrooms,baths):
        brooms = int(bedrooms)
        barooms = float(baths)
        self._aptNumber = aptNumber
        self._address = address
        self._bedrooms = brooms
        self._baths = barooms
        self._securityDep = 330 * brooms
        self._rent = (220 * brooms) + (150 * barooms) + 200
        self._renter = "Vacant"

    #Set a new value for security deposit
    def setSecDep(self,securityDep):
        securityDeposit = float(securityDep)
        self._securityDep = securityDeposit

    #Set a new value for rent
    def setRent(self,rent):
        rentTotal = float(rent)
        self._rent = rentTotal

    #Set a new renter
    def setRenter(self,renter):
        if renter != "":
            self._renter = renter
        else:
            self._renter = "Vacant"

    #Get user data
    def getData(self):
        firstString = "Apt %s located at %s" %(self._aptNumber,self._address)
        secondString = "%d bedrooms and %.2f baths" % (self._bedrooms,self._baths)
        thirdString = "$.2d security deposit and %.2f rent" % (self._securityDep, self._rent)
        fourthString = "Rented to: %s" % (self._renter)
        return firstString + secondString + thirdString + fourthString 



    def __repr__(self):
        "Apt %s @ %s %d BR  %.2f  Bath  Dep=$%.2d  Rent=$%.2d  %s" % (self._aptNumber,self._address,self._bedrooms,self._baths,self._securityDep,self._rent,self._renter)


# Test code for the Apartment Class
if __name__ == "__main__":
    apts = []
    apts.append(Apartment("10C", "107 E. Main", 3, 1.5))
    apts.append(Apartment("14B", "109 E. Main", 4, 2))
    apts.append(Apartment("13", "2207 W. Broadway", "5", "2.5"))

    for apt in apts:
        print(apt)
    print()

    apts[0].setRent("1245")
    apts[0].setRenter("Rocky Quartzite")
    apts[1].setSecDep("1000")
    apts[1].setRenter("Millie Milton")

    print(apts[0].getData())
    print()

    for apt in apts:
        if not apt.isVacant():
            print(apt)
    print()

    apts[0].setRenter("")
    print(apts[0])
