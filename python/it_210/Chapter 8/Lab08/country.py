##Dalton Cothron
#Alyssa Lillehaugen
#Lab08

class Country :
    ## Creates a Country object for its Name, population, and area.
    #@param name is the countries name
    #@param pop is the countreis population
    #@param area is the countries area
    #
    def __init__(self, name, pop, area):
        self._name = name
        self._populationDensity = 0
        self._pop = pop
        self._area = area

    #Gets the name of the current country
    #returns the name
    #
    def getName(self):
        return self._name

    #Gets the population of the current country
    #returns the population
    #
    def getPopulation(self):
        return self._pop

    #Gets the area of the current country
    #returns the area
    #
    def getArea(self):
        return self._area

    #Calculates the population density and sets its value
    #returns the populatin density
    def getPopDensity(self):
        self._populationDensity = self._pop / self._area
        return self._populationDensity
        
    
    
