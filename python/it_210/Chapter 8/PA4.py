#!/usr/bin/env python3
#PA4.py
#Dalton Cothron

#import system arguements 
from sys import argv

#Sets up dictionary, gets results of anagrams and prints them
def main():
    anagramDict = {}
    largest = 0
    anagramKey = ""

    inFile = argv[1]
    fileName = open(inFile,'r') #Open the file from the command line
    
    fields = extractLine(fileName)#Extract the first line
    while len(fields) > 0: #Loop to run while not empty string
        if len(fields[0]) == argv[2]: #Checks if fields length is within limit
            addWord(anagramDict,fields[0],fields[1]) #adds word to dictoinary
        fields = extractLine(fileName)
    
    for item in anagramDict:#Checks for the largest set
        if len(anagramDict[item]) > largest:
            largest = len(anagramDict[item])
            anagramKey = item

    #Checks for correct amount of command line arguements
    if argv != 2:
        print("Too few/many command-line arguements supplied.")
        
    else:
        #Chekcs for anagrams in set
        if largest <= 1:
            print("No anagrams.")

        #checks for longest anagram set and prints them
        else:
            print("Words of length: ", argv[2])
            print(anagramDict[anagramKey])
        
    fileName.close()

#Extracts a line from the file
#@param infile takes the file
def extractLine(infile):
    file = []
    line = infile.readline()

    #Read strip and sort lines
    if line != "":
        line = line.rstrip()
        sortedLine = sorted(line)
        return [line, sortedLine]
        
    else: return[]
    
#Adds anagrams to the set
#@param entries is the dictionary
#@param word is the unsorted word
#@param sortedWord is the sorted word in the dictionary
def addWord(entries, word, sortedWord):

    #If in dictionary, add unsorted word to set
    if sortedWord in entries:
        addDict = entries[sortedWord]
        addDict.add(word)

    #If not in the dictionary create a new set
    else:
        addDict = set([sortedWord])
        entries[sortedWord] = addDict

#Run the program
main()
