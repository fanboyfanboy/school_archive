## Play "Heap of Beans" . Use the best strategy to beat the opponent
#1,2, or 3 beans will be removed.  Never more than 3 or less than one.
#Never more than the amount on the table
# @param beans currently on the table
# @return the amount of beans to be returned
#
import random

def player(beans):
    if beans >= 9:
        return random.randrange(1,4)
    if beans == 8 : return 3
    if beans == 7 : return 2
    if beans == 6: return 1
    if beans == 5: return random.randrange(1,4)
    if beans == 4: return 3
    if beans == 3: return 2
    if beans == 2: return 1
    if beans == 1: return 1
    return beans - 1

