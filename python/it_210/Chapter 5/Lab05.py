#Dalton Cothron
#Erica Thompson
#Lab 05

#import statements
from math import sqrt

#Setting variables
a = 440
count = 0

#Loop and print
while count <= 12 :
    total = a * (2 ** (count / 12))
    count = count + 1
    print("%.2f" % total)
