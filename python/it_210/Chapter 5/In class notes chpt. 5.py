# In Class notes

def factorial(n) :
    if n < 2:
        return n
    return n * factorial(n-1) #Returns factorial 


def reverse(s): #P5.15
    if len(s) < 2:
        return s
    return reverse(s[1:]) + s[0] #Returns string backwards
                                  
def isPalindrome(s) : #P5.16
    if len(s) < 2:
        return True
    if s[0] == s[-1]: #Checks to see if first letter and last letter are same
        return isPalindrome(s[1:-1]) #checks middle for same characters
    else:
        return False
