##Takes input and prints the first and last digit, and total entered digits
# @paramA takes the inputs
# @prints the results of the other functions
#
def main():
    a = input("Please enter an integer: ")
    print("The first digit is: ", firstDigit(a))
    print("The last digit is: ", lastDigit(a))
    print("The digits are: ", digits(a))
    
## Looks for first digit in the entered set
# @paramX takes paramA and finds the first digit
# @return the first digit
#
def firstDigit(x):
    i = int(x)
    for i in range(x):
        if i.isdigit():
            result = x[i]
            return result
        else:
            i = i - 1
    return


## Looks for last digit in the entered set
# @paramX takes paramA and finds the last digit
# @return the last digit
#
def lastDigit(x):
    if x.isdigit():
        result = x[-1:]
        return result
    else:
        return

##counts total number of entered digit set
# @paramX takes paramA and finds the length of entered set
# @return total number of digits
#
def digits(x):
    count = 0
    for i in x:
        if i.isdigit():
            count = count + 1
    return count

#starts program    
main()
