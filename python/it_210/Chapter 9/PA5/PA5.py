#Dalton Cothron
#Programming Assignment 5

#Import statement
from math import pi, sqrt

#Define Cone class
class Cone:
    #Constructs a cone and sets the radius and height.
    def __init__(self, radius, height):
        self._radius = radius
        self._height = height

    # Gets the computed volume of the cone.
    def getVolume(self):
        return pi * (self._radius ** 2) * (self._height / 3)

    #Gets the computed surface area of the cone.
    def getSurface(self):
        surfaceArea = self._radius + sqrt((self._height ** 2) + (self._radius ** 2))
        return pi * self._radius * surfaceArea


#Define Sphere class
class Sphere:
    #constructs a sphere and sets the radius and height.
    def __init__(self,radius):
        self._radius = radius

    #Gets the computed volume of a sphere.
    def getVolume(self):
        return (4 / 3) * pi * (self._radius ** 3)

    #Gets the computed surafce area of a sphere.
    def getSurface(self):
        return 4 * pi * (self._radius ** 2)


#Define Cylinder class
class Cylinder:
    def __init__(self,radius,height):
        self._radius = radius
        self._height = height

    #Gets the computed volume of a cylinder.
    def getVolume(self):
        return pi * (self._radius ** 2) * self._height
    
    #Gets the computed surface area of a cylinder.
    def getSurface(self):
        return (2 * pi * self._radius * self._height) + (2 * pi * (self._radius ** 2))


#Test the classes defined above.
def main():
    #Gather input from the user.
    r = float(input("Enter the radius: "))
    h = float(input("Enter the height: "))

    #Create a Cone, Sphere and Cylinder.
    co = Cone(r, h)
    s = Sphere(r)
    cy = Cylinder(r, h)

    #Display the volume and surface area of each shape.
    print("A sphere has volume:", s.getVolume())
    print("A sphere has surface area:", s.getSurface())
    print("A cylinder has volume:", cy.getVolume())
    print("A cylinder has surface area:", cy.getSurface())
    print("A cone has volume:", co.getVolume())
    print("A cone has surface area:", co.getSurface())

#Call the main function.
main()
