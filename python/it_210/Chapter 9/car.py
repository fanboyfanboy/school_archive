#Dalton Cothron
#P9.6

class Car:
    #Constructs a car
    def __init__(self, milesPerGallon):
        self._gas = 0
        self._efficiency = milesPerGallon/1

    #Adds gas to car object
    def addGas(self, addedGas):
        self._gas = self._gas + addedGas

    #reduces gas mileage by driven miles
    def drive(self, drivenMiles):
        self._gas = self._gas - (drivenMiles / self._efficiency)

    #Returns current gas level
    def getGasLevel(self):
        return "There are %.2f gallons of fuel remaining." % self._gas

