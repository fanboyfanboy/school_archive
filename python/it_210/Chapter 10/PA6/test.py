from PA6 import DooDad

doodads = []
doodads.append(DooDad("red",5,10))
doodads.append(DooDad("red",8,9))
doodads.append(DooDad("blue",20,15))
doodads.append(DooDad("green",2,5))
doodads.append(doodads[0] + doodads[1])
doodads.append(doodads[2] + doodads[0])
doodads.append(doodads[3]- doodads[1])
doodads.append(doodads[1] - doodads[3])
doodads.append(doodads[0] * doodads[1])
doodads.append(doodads[0] * doodads[2])
doodads.append(doodads[0] / doodads[3])
doodads.append(doodads[2] % doodads[3])

for doodad in doodads:
    print(doodad)
