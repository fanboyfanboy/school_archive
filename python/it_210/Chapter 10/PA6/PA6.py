#Dalton Cothron
#PA6

#Define DooDad class
class DooDad:
    #set base serial number
    _serialNumber = 1000

    #construct DooDad object
    def __init__(self,color,weight,value):

        #Increase serial number by 1
        DooDad._serialNumber += 1

        #set Instance variables
        self._color = color
        self._weight = weight
        self._value = value
        self._serialNumber= DooDad._serialNumber

    #Returns color
    def getColor(self):
        return self._color

    #returns weight
    def getWeight(self):
        return self._weight

    #Returns value
    def getValue(self):
        return self._value

    #returns serial number
    def getSerialNumber(self):
        return self._serialNumber

    #Creates new DooDad from adding two together
    def __add__(self, secondObj):
        if self._serialNumber - DooDad.getSerialNumber(secondObj) == 1 or DooDad.getSerialNumber(secondObj) - self._serialNumber == 1:
            value = self._value * DooDad.getValue(secondObj)
        else:
            value = self._value + DooDad.getValue(secondObj)
        color = self._color
        weight = max(self._weight, DooDad.getWeight(secondObj))
        return DooDad(color, weight, value)

    #Creates new doodad from subtracting values
    def __sub__(self, secondObj):
        color = self._color
        weight = DooDad.getWeight(secondObj)
        value = min(self._value,DooDad.getValue(secondObj))
        return DooDad(color,weight,value)

    #Creates new object from multiplication operator
    def __mul__(self,secondObj):
        weight = max(self._weight, DooDad.getWeight(secondObj))
        value = (self._value + DooDad.getValue(secondObj)) / 2
        if self._color == DooDad.getColor(secondObj):
            color = self._color
        else:
            if self._color == "red":
                color = "green"
            elif self._color == "green":
                color = "blue"
            elif self._color == "blue":
                color = "red"
        return DooDad(color,weight,value)

    #creates new object from / operator
    def __truediv__(self,secondObj):
        weight = min(self._weight, DooDad.getWeight(secondObj))
        value = (self._value + DooDad.getValue(secondObj)) / 2
        if self._color == DooDad.getColor(secondObj):
            color = self._color
        else:
            if self._color == "red":
                color = "green"
            elif self._color =="green":
                color = "blue"
            elif self._color == "blue":
                color = "red"
        return DooDad(color,weight,value)

    #creates new object from % operator
    def __mod__(self,secondObj):
        color = "red"
        weight = self._weight + DooDad.getWeight(secondObj)
        value = DooDad.getWeight(secondObj)
        return DooDad(color,weight,value)



    #Return string formating
    def __repr__(self):
        return "%s DooDad weighing %.1f grams.  Value = %.1f points.  S/N=%d" % (self._color,self._weight,self._value,self._serialNumber)

