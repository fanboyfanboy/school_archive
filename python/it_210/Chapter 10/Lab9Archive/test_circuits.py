

# The necessary imports should go here
from resistor import Resistor
from circuits import Serial, Parallel, Circuit


# Make no changes after this line!!

# Create (manufacture) resistors.
r1 = Resistor(220,10)
r2 = Resistor(220,10)
r3 = Resistor(220,5)
r4 = Resistor(220,5)
r5 = Resistor(220,5)

# Examine the resistors we have made
print("The five resistors have these values:")
print(r1.getActual(), "Expected: 220")
print(r2.getActual(), "Expected: 220")
print(r3.getActual(), "Expected: 220")
print(r4.getActual(), "Expected: 220")
print(r5.getActual(), "Expected: 220")
print()
print("Series and parallel combinations yield these values:")

# Put two resistors in series.
s = Serial()
s.addCircuit(r1)
s.addCircuit(r2)
print(s, "Expected: 440")

# Put two resistors in parallel.
p = Parallel()
p.addCircuit(r3)
p.addCircuit(r4)
print(p, "Expected: 110")

# Mix it up a little bit more.
c = Serial()
c.addCircuit(r5)
c.addCircuit(s)
c.addCircuit(p)
print(c, "Expected: 770")
print()
