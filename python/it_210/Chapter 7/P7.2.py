#P7.2
#Dalton Cothron

#Prompt user for input and output files
inputFileName = input("Input file: ")
outputFileName = input("Output file: ")

#Open input and output files
inputFile = open(inputFileName, "r")
outputFile = open(outputFileName, "w")

i = 0 
#split lines and write to file
for line in inputFile:
    i = i + 1
    outputFile.write("/* %d */ %s" %(i, line))
    
#close files
inputFile.close()
outputFile.close()
