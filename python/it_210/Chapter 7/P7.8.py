#Dalton Cothron
#P7.8

#get file
inputFileName = input("Enter file name to be reversed: ")

#open file get contents reverse
inputContents = open(inputFileName,"r")
writeContents = inputContents.read()
inputContents.close()

#write and reverse
outputContents = open(inputFileName,'w')
outputContents.write(writeContents[::-1])
outputContents.close()
