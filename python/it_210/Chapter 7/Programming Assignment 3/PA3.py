#Dalton Cothron
#PA3

#get matrix data and run other functions
def main():
   matrixData = readMatrix("PA3matrix1.txt") #file import
   ROWS = len(matrixData)
   COLUMNS = len(matrixData[0])

   for i in range(ROWS): #Row totals
     rowTotal = rowSum(matrixData,i)
     

   for j in range(COLUMNS): #Column totals
     for i in range(ROWS):
       columnSum = colSum(matrixData,i) 

   #print(diagonalSums(matrixData))
   printMatrix(matrixData)

   if columnSum != rowTotal:
      print("This matrix is a magic square.")
   else:
      print("This matrix is not a magic square.")

#parameter of the file name
#reads the file and returns the matrix
def readMatrix(filName):
    matrix = []
    inputFile = open(filName,'r')
    for line in inputFile:
        line = line.strip()
        if len(line) > 0:
            matrix.append(line.split())
    inputFile.close()
    return matrix


#parameters matrix and row number
#returns the sum of the row
def rowSum(matrix, rowNum):
    values = matrix[rowNum]
    totalOfValues = 0

    for i in values:
       number = int(i)
       totalOfValues += number
    return totalOfValues


#parameter matrix and row number
#returns the sum of the column
def colSum(matrix, colNum):
   values = matrix[colNum]
   totalOfValues = 0

   for i in values:
     number = int(i)
     totalOfValues += number
   return totalOfValues


#parameter matrix
#returns the diagonal sums in tuples
def diagonalSums(matrix):
   ROWS = int(len(matrix))
   COLUMNS = int(len(matrix[0]))
   leftTotal = 0
   rightTotal = 0
   
   for i in range(ROWS):
      leftTotal += i - 1

   for i in range(COLUMNS):
       rightTotal += i + i
   return leftTotal,rightTotal

   


#Parameter matrix for the data
#prints Matrix data out
def printMatrix(matrix):
    ROWS = len(matrix)
    COLUMNS = len(matrix[0])

    for i in range(ROWS):
       for j in range(COLUMNS):
          print("%5s" % matrix[i][j],end=" ")
       print()


#start program
main()
