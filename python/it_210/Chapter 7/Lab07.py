#Lab 07
#Dalton Cothron

#import randint
from random import randint

#runs rollDice, gets values and total sum
#Writes results to output file called "diceOut.txt"
#Also prints result in console
def main():
    values = list(rollDice())
    totalSum = sum(values)
    outputFile = open("diceOut.txt",'w') #Open file for writing

    for i in values: #Loop to print values and write to file
        print(i, end=" ")
        outputFile.write("%s " % i)
    outputFile.write("\n")
    print()

    while totalSum > 11 and totalSum < 24: #loop to check if within range
        values = list(rollDice())
        totalSum = sum(values)

        for i in values: #Loops to print values and write to file
            outputFile.write("%s " % i)
            print(i, end=" ")
        outputFile.write("\n")
        print()

    #Close the file
    outputFile.close()


#rolls a 6 sided die 5 times
#returns the tuple
def rollDice():
    dice = []
    for i in range(5): #loop to run 5 times
        dice.append(randint(1,6))
    dice.sort()
    dieOne = dice[0]
    dieTwo = dice[1]
    dieThree = dice[2]
    dieFour = dice[3]
    dieFive = dice[4]
    return dieOne,dieTwo,dieThree,dieFour,dieFive

#starts the program
main()

