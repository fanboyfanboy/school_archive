#4.9
#Get input, set strings
stringInp = input("Please enter a string: ")
reverseWord = ""
stringLength = len(stringInp)

#Set string backwards and print
for char in range(stringLength,0,-1):
    reverseWord = reverseWord + stringInp[char-1]
print(reverseWord)
