#4.3B
#Get input, set strings
stringInp = input("Please enter a string: ")
secondLetter = ""
position = True

#Add second letter
for char in stringInp :
    if char.isalpha():
        if position:
            secondLetter = secondLetter + char
        if char != " ":
            position = not position
    else:
        position = not position

print("Every second letter of the string is: ", secondLetter)
