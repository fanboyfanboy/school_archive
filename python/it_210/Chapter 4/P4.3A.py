#P4.3A

#Get input, set empty string for uppercase letters
stringInp = input("Please enter a sentence: ")
upperCaseLetters = ""

#Find, add, and print uppercase letters
for char in stringInp :
    if char.isupper() :
        upperCaseLetters = upperCaseLetters + char
print("The uppercase letters in the sentence are :", upperCaseLetters)



